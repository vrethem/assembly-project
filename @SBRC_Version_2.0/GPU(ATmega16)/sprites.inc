/////////////////////////  sprites.inc   ////////////////////////////
;
; Containts the pixeldata for the sprites

.cseg

FIRST_SPRITE:
; Sprite $0-9 Numbers (unanimated)
; Nolla
.db	$77, $77, $ff, $ff
.db	$7f, $f7, $ff, $ff
.db	$7f, $f7, $ff, $ff
.db	$7f, $f7, $ff, $ff
.db	$77, $77, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff

; Etta
.db	$ff, $7f, $ff, $ff
.db	$ff, $7f, $ff, $ff
.db	$ff, $7f, $ff, $ff
.db	$ff, $7f, $ff, $ff
.db	$ff, $7f, $ff, $ff 
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff

;Tv�a
.db	$77, $77, $ff, $ff
.db	$ff, $f7, $ff, $ff
.db	$77, $77, $ff, $ff
.db	$7f, $ff, $ff, $ff
.db	$77, $77, $ff, $ff 
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff

;Trea
.db	$77, $77, $ff, $ff
.db	$ff, $f7, $ff, $ff
.db	$f7, $77, $ff, $ff
.db	$ff, $f7, $ff, $ff
.db	$77, $77, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff

; Fyra
.db	$7f, $f7, $ff, $ff
.db	$7f, $f7, $ff, $ff
.db	$77, $77, $ff, $ff
.db	$ff, $f7, $ff, $ff
.db	$ff, $f7, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff

; Femmma
.db	$77, $77, $ff, $ff
.db	$7f, $ff, $ff, $ff
.db	$77, $77, $ff, $ff
.db	$ff, $f7, $ff, $ff
.db	$77, $77, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff

; Sexa
.db	$77, $77, $ff, $ff
.db	$7f, $ff, $ff, $ff
.db	$77, $77, $ff, $ff
.db	$7f, $f7, $ff, $ff
.db	$77, $77, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff

; Sjua
.db	$77, $77, $ff, $ff
.db	$ff, $f7, $ff, $ff
.db	$ff, $77, $7f, $ff
.db	$ff, $f7, $ff, $ff
.db	$ff, $f7, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff

; �tta
.db	$77, $77, $ff, $ff
.db	$7f, $f7, $ff, $ff
.db	$77, $77, $ff, $ff
.db	$7f, $f7, $ff, $ff
.db	$77, $77, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff

; Nia
.db	$77, $77, $ff, $ff
.db	$7f, $f7, $ff, $ff
.db	$77, $77, $ff, $ff
.db	$ff, $f7, $ff, $ff
.db	$ff, $f7, $ff, $ff 
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff

; Sprite $0 Player (unanimated)
.db	$f1, $ee, $e3, $3f
.db	$41, $77, $e3, $3e
.db	$41, $00, $0e, $ee
.db	$f1, $ee, $ee, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff

; Sprite $1 Enemy (unanimated)
.db	$1d, $dd, $cc, $a4
.db	$ff, $dd, $cc, $a1
.db	$ff, $fe, $cc, $a4
.db	$ff, $f6, $cc, $a1
.db	$ff, $f6, $cc, $a4
.db	$ff, $fe, $cc, $a1
.db	$ff, $dd, $cc, $a4
.db	$1d, $dd, $cc, $a1

; Sprite $2 Astroid (unanimated)
.db	$ff, $88, $88, $ff
.db	$f9, $88, $88, $cf
.db	$c8, $99, $99, $cf
.db	$c8, $aa, $aa, $cf
.db	$c8, $aa, $aa, $cf
.db	$aa, $aa, $aa, $9f
.db	$fc, $ca, $a9, $af
.db	$ff, $cc, $99, $ff

; Sprite $3 Bullet (unanimated)
.db	$7F, $FF, $FF, $FF
.db	$FF, $FF, $FF, $FF
.db	$FF, $FF, $FF, $FF
.db	$FF, $FF, $FF, $FF
.db	$FF, $FF, $FF, $FF
.db	$FF, $FF, $FF, $FF
.db	$FF, $FF, $FF, $FF
.db	$FF, $FF, $FF, $FF

; Sprite $4 Heart (unanimated)
.db	$ff, $ff, $ff, $ff
.db	$ff, $1f, $f1, $ff
.db	$f1, $1f, $11, $1f
.db	$f1, $11, $11, $1f
.db	$f1, $11, $11, $1f
.db	$ff, $11, $11, $ff
.db	$ff, $11, $1f, $ff
.db	$ff, $f1, $ff, $ff

; Sprite $5 Dead player (unanimated)
.db	$ff, $1f, $ff, $ff
.db	$f1, $4f, $f4, $4f
.db	$f4, $04, $14, $ff
.db	$f0, $01, $ff, $ff
.db	$f1, $e0, $e4, $34
.db	$41, $17, $e4, $3e
.db	$41, $01, $10, $ee
.db	$f1, $1e, $1e, $ff

; Sprite $6 (unanimated)
.db	$3F, $aa, $bb, $bb
.db	$FF, $aa, $FF, $cc
.db	$64, $FF, $FF, $cc
.db	$aa, $86, $66, $cc
.db	$dd, $aa, $22, $cc
.db	$FF, $bb, $FF, $cc
.db	$cc, $cF, $FF, $cc
.db	$FF, $FF, $FF, $cc

; Sprite $7 (unanimated)
.db	$1d, $dd, $cc, $a4
.db	$ff, $dd, $cc, $a1
.db	$ff, $fe, $cc, $a4
.db	$ff, $f6, $cc, $a1
.db	$ff, $f6, $cc, $a4
.db	$ff, $fe, $cc, $a1
.db	$ff, $dd, $cc, $a4
.db	$1d, $dd, $cc, $a1

; Sprite $8-$b Player animation
.db	$f1, $ee, $e3, $3f
.db	$41, $77, $e3, $3e
.db	$41, $00, $0e, $ee
.db	$f1, $ee, $ee, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff

.db	$41, $ee, $e3, $3f
.db	$41, $77, $e3, $3e
.db	$41, $00, $0e, $ee
.db	$41, $ee, $ee, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff

.db	$41, $ee, $e3, $3f
.db	$11, $77, $e3, $3e
.db	$11, $00, $0e, $ee
.db	$41, $ee, $ee, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff

.db	$11, $ee, $e3, $3f
.db	$f1, $77, $e3, $3e
.db	$f1, $00, $0e, $ee
.db	$11, $ee, $ee, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff

; Sprite $c-$f Enemy animation
.db	$1d, $dd, $c8, $ff
.db	$ff, $dd, $cc, $41
.db	$ff, $fe, $c1, $1f
.db	$ff, $f6, $c0, $14
.db	$ff, $f6, $c1, $4f
.db	$ff, $fe, $c1, $04
.db	$ff, $dd, $cc, $1f
.db	$1d, $dd, $c8, $ff

.db	$ff, $dd, $c8, $ff
.db	$1d, $dd, $cc, $4f
.db	$ff, $fe, $c4, $11
.db	$ff, $f6, $c4, $1f
.db	$ff, $f6, $c1, $14
.db	$ff, $fe, $c1, $1f
.db	$1d, $dd, $cc, $41
.db	$ff, $dd, $c8, $ff

.db	$1d, $dd, $c8, $ff
.db	$ff, $dd, $cc, $14
.db	$ff, $fe, $c4, $0f
.db	$ff, $f6, $c4, $41
.db	$ff, $f6, $c4, $1f
.db	$ff, $fe, $c0, $11
.db	$ff, $dd, $cc, $4f
.db	$1d, $dd, $c8, $ff

.db	$ff, $dd, $c8, $ff
.db	$1d, $dd, $cc, $11
.db	$ff, $fe, $c1, $4f
.db	$ff, $f6, $c0, $44
.db	$ff, $f6, $c4, $4f
.db	$ff, $fe, $c4, $41
.db	$1d, $dd, $cc, $0f
.db	$ff, $dd, $c8, $ff

; Sprite $10-$13 Astroid animation
.db	$ff, $88, $88, $ff
.db	$f9, $88, $88, $cf
.db	$c8, $99, $99, $cc
.db	$c8, $aa, $aa, $c9
.db	$c8, $aa, $aa, $cf
.db	$aa, $aa, $aa, $9f
.db	$fc, $ca, $a9, $af
.db	$ff, $cc, $99, $ff

.db	$ff, $c9, $ff, $ff
.db	$fc, $cc, $c9, $af
.db	$88, $9a, $aa, $99
.db	$88, $9a, $aa, $a9
.db	$88, $9a, $aa, $ac
.db	$88, $9a, $aa, $cc
.db	$f9, $88, $8a, $cf
.db	$ff, $cc, $ca, $ff

.db	$ff, $99, $cc, $ff
.db	$fa, $9a, $ac, $cf
.db	$f9, $aa, $aa, $aa
.db	$fc, $aa, $aa, $8c
.db	$cc, $99, $99, $c8
.db	$fc, $88, $88, $9f
.db	$fc, $88, $88, $ff
.db	$ff, $88, $88, $ff

.db	$ff, $ac, $cc, $ff
.db	$fc, $a8, $88, $9f
.db	$cc, $aa, $a9, $88
.db	$ca, $aa, $a9, $88
.db	$9a, $aa, $a9, $88
.db	$99, $aa, $a9, $88
.db	$fa, $9c, $cc, $cf
.db	$ff, $ff, $9c, $ff

; Sprite $14-$15 Heart animation
.db	$ff, $ff, $ff, $ff
.db	$ff, $1f, $f1, $ff
.db	$f1, $1f, $11, $1f
.db	$f1, $11, $11, $1f
.db	$f1, $11, $11, $1f
.db	$ff, $11, $11, $ff
.db	$ff, $11, $1f, $ff
.db	$ff, $f1, $ff, $ff

.db	$ff, $ff, $ff, $ff
.db	$f1, $ff, $1f, $ff
.db	$11, $f1, $11, $ff
.db	$11, $11, $11, $ff
.db	$11, $11, $11, $ff
.db	$f1, $11, $1f, $ff
.db	$f1, $11, $ff, $ff
.db	$ff, $f1, $ff, $ff

; Sprite  Dead animation
.db	$ff, $1f, $ff, $ff
.db	$f1, $4f, $f4, $4f
.db	$f4, $04, $14, $ff
.db	$f0, $01, $ff, $ff
.db	$f1, $e0, $e4, $34
.db	$41, $17, $e4, $3e
.db	$41, $01, $10, $ee
.db	$f1, $1e, $1e, $ff

.db	$f1, $4f, $f4, $4f
.db	$f4, $04, $14, $ff
.db	$f0, $01, $ff, $ff
.db	$ff, $1f, $ff, $ff
.db	$f1, $e0, $e4, $34
.db	$41, $17, $e4, $3e
.db	$41, $01, $10, $ee
.db	$f1, $1e, $1e, $ff

.db	$f4, $04, $14, $ff
.db	$f0, $01, $ff, $ff
.db	$ff, $1f, $ff, $ff
.db	$f1, $4f, $f4, $4f
.db	$f1, $e0, $e4, $34
.db	$41, $17, $e4, $3e
.db	$41, $01, $10, $ee
.db	$f1, $1e, $1e, $ff

.db	$f0, $01, $ff, $ff
.db	$ff, $1f, $ff, $ff
.db	$f1, $4f, $f4, $4f
.db	$f4, $04, $14, $ff
.db	$f1, $e0, $e4, $34
.db	$41, $17, $e4, $3e
.db	$41, $01, $10, $ee
.db	$f1, $1e, $1e, $ff

; Sprite 
.db	$3F, $FF, $55, $33
.db	$FF, $FF, $44, $27
.db	$FF, $ee, $ee, $77
.db	$FF, $ee, $ee, $aa
.db	$FF, $ee, $ee, $aa
.db	$dd, $ee, $ee, $ee
.db	$dd, $dd, $dd, $dd
.db	$dd, $dd, $dd, $dd

; Sprite  Flagga
.db	$aa, $aa, $aa, $aa
.db	$aa, $bb, $bb, $aa
.db	$aa, $bb, $bb, $aa
.db	$aa, $bb, $bb, $aa
.db	$aa, $bb, $bb, $aa
.db	$aa, $bb, $bb, $aa
.db	$aa, $bb, $bb, $aa
.db	$aa, $aa, $aa, $aa

; Sprite Gubben
.db	$f1, $fe, $ee, $ff
.db	$f1, $f5, $5f, $ff
.db	$ff, $18, $81, $1f
.db	$ff, $f8, $8f, $f1
.db	$ff, $f8, $8f, $ff
.db	$ff, $ef, $fe, $ff
.db	$ff, $ef, $fe, $ff
.db	$f9, $9f, $f9, $9f

; Sprite testbild
.db	$e1, $23, $45, $67
.db	$89, $ab, $cd, $ef
.db	$00, $11, $22, $33
.db	$00, $11, $22, $33
.db	$44, $55, $66, $77
.db	$44, $55, $66, $77
.db	$88, $99, $aa, $bb
.db	$88, $99, $aa, $bb

; OLD Sprite $c-$f Enemy animation
.db	$1d, $dd, $cc, $8f
.db	$ff, $dd, $cc, $44
.db	$ff, $fe, $cc, $11
.db	$ff, $f6, $cc, $11
.db	$ff, $f6, $cc, $14
.db	$ff, $fe, $cc, $14
.db	$ff, $dd, $cc, $41
.db	$1d, $dd, $cc, $8f

.db	$ff, $dd, $cc, $8f
.db	$1d, $dd, $cc, $14
.db	$ff, $fe, $cc, $41
.db	$ff, $f6, $cc, $41
.db	$ff, $f6, $cc, $11
.db	$ff, $fe, $cc, $11
.db	$1d, $dd, $cc, $44
.db	$ff, $dd, $cc, $8f

.db	$1d, $dd, $cc, $8f
.db	$ff, $dd, $cc, $11
.db	$ff, $fe, $cc, $44
.db	$ff, $f6, $cc, $44
.db	$ff, $f6, $cc, $41
.db	$ff, $fe, $cc, $41
.db	$ff, $dd, $cc, $14
.db	$1d, $dd, $cc, $8f

.db	$ff, $dd, $cc, $8f
.db	$1d, $dd, $cc, $41
.db	$ff, $fe, $cc, $14
.db	$ff, $f6, $cc, $14
.db	$ff, $f6, $cc, $44
.db	$ff, $fe, $cc, $44
.db	$1d, $dd, $cc, $11
.db	$ff, $dd, $cc, $8f
