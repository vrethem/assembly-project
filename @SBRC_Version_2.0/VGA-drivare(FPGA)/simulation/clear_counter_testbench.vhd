library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity clear_counter_testbench is
end clear_counter_testbench;

architecture clear_counter_testbench_beh of clear_counter_testbench is

	component clear_counter
		port( clk   : in std_logic;
				reset : in std_logic;
				adr   : buffer std_logic_vector(14 downto 0);
				wr    : out std_logic;
				color	: out std_logic_vector(3 downto 0));
	end component;
	
	signal clk : std_logic;
	signal reset : std_logic;
	signal adr : std_logic_vector(14 downto 0);
	signal wr  : std_logic;
	signal color  : std_logic_vector(3 downto 0);
	
	constant clk_period : time := 1 ns;
begin

	uut: clear_counter port map(
		clk => clk,
		reset => reset,
		adr => adr,
		wr => wr,
		color => color
	);
	
	test_process : process
	begin
		clk <= '0';
		reset <= '1';
		wait for clk_period;
		reset <= '0';
		wait for clk_period;
		
		for i in 0 to 160*120 loop
			clk <= '1';
			wait for clk_period;
			clk <= '0';
			wait for clk_period;
		end loop;	
		
		
		wait for clk_period;
	end process;
	
end clear_counter_testbench_beh;