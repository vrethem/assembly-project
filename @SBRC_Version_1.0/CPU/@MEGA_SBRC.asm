 /*
 * _MEGA_SBRC.asm
 *
 *  Created: 2/3/2017 10:55:38 AM
 */ 
 .include "settings.inc"
 .include "macros.inc"
 .include "music_tones.inc"
 .include "Fur_Elise.inc"

.org $00
	jmp HW_INIT
.org $026								; Timer 0  Sound effects 
	jmp SHOOT_SOUND						
.org OC1Aaddr							; Timer 1 Game tics
	jmp INTERUPPT_TIC_COUNT				
;.org OC2addr							; Timer 2 Music - song	Not in use
;	jmp MUSIC_INTERUPT					

.org	PROGRAM_START
HW_INIT:
	ldi		r16, HIGH(RAMEND)			; set Stack-pointer
	out		SPH, r16
	ldi		r16, LOW(RAMEND)
	out		SPL, r16

;; Timer 0 - Setup sound effects
	ldi		r16,3						
	out		TCCR0,r16					; S�tter timer 0 prescaler till clk/64
	
	clr		r16
	sts		COUNTER_SHOOT,r16			; Sound effects byte in SRAM
	sts		COUNTER_ENEMY,r16			; Sound effects byte in SRAM

; Timer 1 - 16-bit Timer, setup interupt for 60,096 Hz
	ldi		r16,(1<<CS12)|(0<<CS11)|(1<<CS10)|(1 << WGM12)	; Prescaler CLK/1024
	out		TCCR1B,r16
	ldi		r16,$00	
	out		OCR1AH,r16						; Set match registerA
	ldi		r16,$9C	
	out		OCR1AL,r16

	ldi		r16,(1<<OCIE1A)					; Enable Game_tics interupts
	out		TIMSK,r16

;; Timer 2 -  Enable song interupt ( Not in use )
	ldi		r16,(1<<CS12)|(0<<CS11)|(1<<CS10)
	out		TCCR2, r16
	ldi		r16, $FF
	out		OCR2, r16

;; Setup SPEAKER
	ldi		r16, $80		; Weak pullup PORTA 0-4
	out		DDRA, r16		; Pin7 Audio output pin
	ldi		r16,  $1F
	out		PORTA, r16

UART_INIT:
; Set baud rate 
	ldi		r17, $00
	ldi		r16, $00
	out		UBRRH, r17
	out		UBRRL, r16
; Enable Transmit
	ldi		r16, (1 << TXEN)
	out		UCSRB, r16
; Set frame format 8-bit data, 1 stop bit
	ldi		r16, (1<<URSEL) | (1<<UCSZ1) | (1<<UCSZ0)
	out		UCSRC, r16
;; Setup sprites and new game
	call	INIT_SPRITE_SETTINGS
	
	sei		; !!! ENABLE INTERUPPTS GLOBALLY !!!
	call	NEW_GAME
	rjmp	MAIN






;;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;;=====================					MAIN LOOP					=========================================
;;---Uses  Z-ptr, Y-ptr
MAIN:
	call	READ_JOYSTICK				; Read joystick input
	call	GAME_LEVEL					; Insert new ASTROID or ENEMY
	call	GAME_UPDATE					; Checks if game should update
	call	RANDOM_SEED					; Inc SEED
rjmp MAIN

;;------------------------------------
;;			READ_JOYSTICK
;; PORTA input is negative inverted
;;---Uses  Z-ptr
READ_JOYSTICK:
	in		r16, PINA						
	com		r16									; Invert input
	andi	r16, $1F							; Player input on pin 0-4
	LOAD_Z	JOY_BUFFER
	ld		r17, Z		
	or		r17, r16							; OR -> Don't miss any input 
	st		Z, r17
ret

;;------------------------------------
;;			GAME_UPDATE
;; SEND_DATA after collision check -> Always send data inside boundries	
;;---Uses  Z-ptr
GAME_UPDATE:
	lds		r16, TICS							; Get TICS
	cpi		r16, ( GAME_SPEED )
	brlo	NO_UPDATE
	;				OBS						brne	NO_UPDATE  ; Borde vara brlo 
UPDATE:	
	call	GAME_TIC							; Update game
	call	COLLISION_BOX						; Check collisions
	call	SEND_DATA							; Send objects SPRITE_ID / X_ID / Y_ID
	clr		r16									
	sts		TICS, r16							; Reset TICS
NO_UPDATE:
	nop
ret

;;------------------------------------
;;				SEND_DATA
;;---	Send 3 bytes via UART, SPRITE_ID, X_ID, Y_ID
SEND_DATA:
	ldi		r16, END_FRAME						; Send END_FRAME two times at begining 
	call	UART_TRANSMIT						; of new transfer
	ldi		r16, END_FRAME
	call	UART_TRANSMIT
	LOAD_Y	PLAYER								; Always send player 
	call	SEND								
	FOR_VIS	Y, ENEMY, (OBJECTS -1), SEND		; Send all VISIBLE objects in SRAM, start at ENEMY
	ldi		r16, END_FRAME
	call	UART_TRANSMIT						; Send END_FRAME
ret

;; ---- SEND one object ----
SEND:
	ldd		r16, Y + SPRITE_ID
	call	UART_TRANSMIT
	ldd		r16, Y + X_ID
	call	UART_TRANSMIT
	ldd		r16, Y + Y_ID
	call	UART_TRANSMIT
ret

;; ---- UART_Transmit ----
UART_TRANSMIT:
	sbis	UCSRA, UDRE		; Wait for empty Transmit buffer
	rjmp	UART_Transmit
	
	out		UDR, r16		; Put data(r16) into buffer, sends the data
ret

;;------------------------------------
;;			Random-Seed
;;---Uses  Z-ptr
;;---
RANDOM_SEED:
	push	r16			; push
	INCSRAM	SEED
	lds		r16, SEED
	cpi		r16, FRAME_MAX_Y - SPRITE_SIZE
	brne	NO_SEED_RESET
	ldi		r16, FRAME_MIN_Y
	sts		SEED, r16
NO_SEED_RESET:
	pop		r16			; pop
ret
;////////////////////////////////////////////////////////////////////////////////////////////////////////////






;;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;;=====================				GAME_LEVEL						=========================================
;;---Uses  Z-ptr, Y-ptr
GAME_LEVEL:					
	lds		r16, ASTROID_TIC			; Maybe spawn new Astroid				
	cpi		r16, ASTROID_SPAWN_TIMER	; ASTROID_TIC + LEVEL > ASTROID_SPAWN_TIMER
	brlo	NO_SPWN_A	
		lds		r16, LEVEL		
		sts		ASTROID_TIC,r16			; set ASTROID_TIC to current LEVEL
		call	NEW_ASTROID

NO_SPWN_A:
	lds		r16, ENEMY_TIC				; Maybe spawn new Enemy				
	cpi		r16, ENEMY_SPAWN_TIMER		; ENEMY_TIC + LEVEL > ENEMY_SPAWN_TIMER
	brlo	NO_SPWN_E			
		lds		r16, LEVEL
		sts		ENEMY_TIC, r16			; set ENEMY_TIC to current LEVEL
		call	NEW_ENEMY
NO_SPWN_E:
	nop
ret

;;------------------------------------
;;			NEW_ASTROID
;; Spawn new astroid-object, inc game difficulty
;;---Uses  Y-ptr
NEW_ASTROID:
	FOR_INV		Y, ASTROID, ASTROIDS, INSERT
	// Y-ptr is set to newly inserted astroid-object
	// Add code for Astroid flags
	INCSRAM	 LEVEL_TIC
	lds		r16, LEVEL_TIC
	cpi		r16, LEVEL_UP_COUNTER		; if LEVEL_TIC == LEVEL_UP_COUNTER 
	brne	Skip_6
		call	LEVEL_UP					; LEVEL++
Skip_6:							
ret
;;------------------------------------
;;			NEW_ENEMY
;; Spawn new enemy-object, inc game difficulty
;;---Uses  Y-ptr
NEW_ENEMY:
	FOR_INV		Y, ENEMY, ENEMIES, INSERT
	// Y-ptr is set to newly inserted enemy-object
	ldd		r16, Y + OBJECT_ID
	cbr		r16, $1F					; Clear Enemy movment bits 0-4
	std		Y + OBJECT_ID, r16

	INCSRAM	 LEVEL_TIC
	lds		r16, LEVEL_TIC
	cpi		r16, LEVEL_UP_COUNTER		; if LEVEL_TIC == LEVEL_UP_COUNTER 
	brne	Skip_7	
		call	LEVEL_UP				; LEVEL++
Skip_7:						
ret
;;------------------------------------
;;		LEVEL_UP
;; Inc LEVEL and check its not higher than max_level
LEVEL_UP:
	lds		r16, LEVEL
	cpi		r16, MAX_LEVEL
	breq	SKIP_5
	INCSRAM LEVEL				; if not max_level inc LEVEL
	clr		r16
	sts		LEVEL_TIC, r16		; Reset LEVEL_TIC
SKIP_5:
ret

;;------------------------------------
;;			INSERT
;;---Uses  Z-ptr, req Y-ptr
;; Gets call from FOR_INV 
;; Sets T-flag in SREG -> Lets calling macro know object was inserted
INSERT:
	push	r16
	ldd		r16, Y + OBJECT_ID
	sbr		r16, (1 << VIS_FLAG) 			; Set VIS_FLAG true
	std		Y + OBJECT_ID, r16	
	LOAD_Z	SEED							; Get SEED for Y-pos
	ld		r16, Z							
	std		Y + Y_ID, r16

	com		r16
	andi	r16, $7F						; Invert SEED
	sts		SEED, r16 

	ldi		r16, FRAME_MAX_X - SPRITE_SIZE	; set X-pos
	std		Y + X_ID, r16
	;call	PREPARE_SPAWN_SOUND
	set										; Set T-flag (to exit loop)
	pop		r16
ret
;////////////////////////////////////////////////////////////////////////////////////////////////////////////






;;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;;=====================					GAME TIC					=========================================
;;---Uses  Z-ptr, Y-ptr
;; Move all objects in game and move player from user input
GAME_TIC:
	INCSRAM		SHOT_TIC								; Inc SHOT_TIC
	call		GET_JOY_BUFFER							; Move player
	FOR_VIS		Y, ASTROID, ASTROIDS, MOVE_ASTROID		; Move astroids
	FOR_VIS		Y, ENEMY  , ENEMIES , MOVE_ENEMY		; Move enemies 
	FOR_VIS		Y, BULLET , BULLETS , MOVE_BULLET		; Move bullets in game
	FOR_VIS		Y, ENEMY  , ENEMIES , ENEMY_BULLET		; Randomly genrates new bullets 
ret

;;------------------------------------
;;------- GET_JOY_BUFFER
;; Handles user input, flushes JOY_BUFFER
;;---Uses  Y-ptr
GET_JOY_BUFFER:
	lds		r17, JOY_BUFFER			; Get input buffer 
	LOAD_Y	PLAYER					; DEC_X and DEC_Y macro req Y-ptr set
	sbrs	r17, JOY_LEFT
	rjmp	NEXT1					
	DEC_X	PLAYER_SPEED			; Move left (req Y-ptr)
Next1:
	sbrs	r17, JOY_RIGHT
	rjmp	NEXT2
	DEC_X	(-PLAYER_SPEED)			; Move right (req Y-ptr)
Next2:
	sbrs	r17, JOY_UP
	rjmp	Next3
	DEC_Y	PLAYER_SPEED			; Move up	 (req Y-ptr)
Next3:
	sbrs	r17, JOY_DOWN			
	rjmp	Next4
	DEC_Y	(-PLAYER_SPEED)			; Move down  (req Y-ptr)
Next4:
	sbrs	r17, JOY_FIRE
	rjmp	Next5
	call	NEW_BULLET				; Fire 
Next5:			
	clr		r17						; Empty joystick buffer
	sts		JOY_BUFFER, r17
ret
;;------------------------------------
;;			NEW_BULLET
;;---Uses  Y-ptr
NEW_BULLET:
	lds		r16, SHOT_TIC			; When SHOT_TIC reaches value allow new shot
	cpi		r16, RELOAD_TIME
	brlo	NO_SHOT 
	FOR_INV	Y, BULLET, BULLETS, INSERT_PLAYER_BULLET
	clr		r16
	sts		SHOT_TIC, r16			; clr SHOT_TIC
	ldi		r16, $80				; Player VIS_FLAG == TRUE 
	sts		PLAYER, r16				; Continue game after HP-loss by pressing fire	 
	ldi		r16, PLAYER_SPRITE_ID
	sts		PLAYER +1, r16			; Reset player_sprite_id 

NO_SHOT:
ret
;;------------------------------------
;;			INSERT_PLAYER_BULLET	
;;--- (Req Y-ptr)
INSERT_PLAYER_BULLET:
	push	r16	
	ldd		r16, Y + OBJECT_ID
	sbr		r16, (1 << VIS_FLAG) | (1 << PSD_FLAG)	; set visible and positive direction flags
	std		Y + OBJECT_ID, r16
	lds		r16, POS_X			
	subi	r16, -(PLAYER_SIZE_X	+ 1)			; set X-pos
	std		Y + X_ID, r16
	lds		r16, POS_Y
	subi	r16, (-PLAYER_SIZE_Y) / 2				; set Y-pos
	std		Y + Y_ID, r16

	call	PREPARE_SHOOT_SOUND
	set
	pop		r16
ret

;;------------------------------------
;;				MOVE_ASTROID
;;--- (Req Y-ptr)
MOVE_ASTROID:
	DEC_X	ASTROID_SPEED
ret

;;------------------------------------
;;				MOVE_ENEMY
;; Move enemy up or down and dec X-pos depending on bit 0-4 in OBJECT_ID
;;---(Req Y-ptr)
MOVE_ENEMY:
	ldd		r16, Y + OBJECT_ID
	andi	r16, $1F						; Load bit 0-4
	cpi		r16, 8			
	brsh	MOVE_UP							; If 8 or higher MOVE_UP
MOVE_DOWN:									; If 7 or lower MOVE_DOWN
	DEC_Y	(-1)
	inc		r16
	rjmp	END_MOVE_ENEMY	
MOVE_UP:
	DEC_Y	1
	inc		r16
	sbrc	r16, 4							; Reset if r16 == 8
	ldi		r16, 0
END_MOVE_ENEMY:
	ldd		r17, Y + OBJECT_ID
	sbr		r16, $E0			
	sbr		r17, $1F
	and		r17, r16						; Save change to bit 0-4, bit 5-7 unchanged
	std		Y + OBJECT_ID, r17
	DEC_X	ENEMY_SPEED						; Dec X-pos 
ret

;;------------------------------------
;;				MOVE_BULLET
;; Check PSD_FLAG in OBJECT_ID for bullet direction
;;---(Req Y-ptr)
MOVE_BULLET:
	ldd		r16, Y + OBJECT_ID
	sbrs	r16, PSD_FLAG		
	rjmp	Skip3
	DEC_X	(-BULLET_SPEED)					; if Player bullet
	rjmp	END_MOVE
Skip3:
	DEC_X	(Bullet_Speed)					; if Enemey bullet
END_MOVE:
	nop
ret

;;------------------------------------
;;		ENEMY_BULLET
;; Randomly generate new enemy bullets
;;--- Req Y-ptr set at ENEMEY-object, sets Z-ptr at BULLET-object for INSERT_ENEMY_BULLET
ENEMY_BULLET:
	LOAD_Z	SEED
	ld		r16, Z											; 120 >= SEED >= 8
	cpi		r16, FRAME_MAX_Y - SPRITE_SIZE - ENEMY_RELOAD	; if  SEED > 120 - EMEMY_RELOAD 
	brlo	Skip_bullet
		FOR_INV		Z, BULLET, BULLETS, INSERT_ENEMY_BULLET
Skip_bullet:
	ldd		r17, Y + X_ID
RANDOMIZE_SEED:
	call	RANDOM_SEED									; Try to randomize new SEED-value
	dec		r17											; Hopefully allows enemies in the middle of SRAM-
	brne	RANDOMIZE_SEED								; table to fire bullets
ret
;;------------------------------------
;;		INSERT_ENEMY_BULLET
;; Insert new bullet at Z-ptr, requiers X,Y pos from Y-ptr
;;---(Req Y-ptr and Z-ptr)
INSERT_ENEMY_BULLET:
	push	r16	
	ldd		r16, Z + OBJECT_ID
	sbr		r16, (1 << VIS_FLAG) | (0 << PSD_FLAG)		; set visible and negative direction flags
	std		Z + OBJECT_ID, r16
	ldd		r16, Y + X_ID								; Get X-pos from Y-ptr
	subi	r16, 1										
	std		Z + X_ID, r16								; set X-pos to Z-ptr

	ldd		r16, Y + Y_ID								; Get Y-pos from Y-ptr
	subi	r16, (-ENEMY_SIZE_Y) / 2					
	std		Z + Y_ID, r16								; set Y-pos to Z-ptr
	call	PREPARE_ENEMY_HIT_SOUND
	set
	pop		r16
ret
;////////////////////////////////////////////////////////////////////////////////////////////////////////////






;;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;;=====================					COLLISION BOX				=========================================
;;--- Uses  Z-ptr, Y-ptr
;; Handles all collision check
;; Handles player and enemies position always inside frame box
COLLISION_BOX:
	call		PLAYER_FRAME_BOX							; Check if player inside boundries
	FOR_VIS		Y , ENEMY   , ENEMIES, ENEMY_FRAME_BOX		; Check if enemies inside boundry (Upper and lower boundry)

	FOR_VIS		Y , ASTROID, ASTROIDS, CHECK_PLAYER_HIT		; Check player collides with astroids
	FOR_VIS		Y , ENEMY  , ENEMIES , CHECK_PLAYER_HIT		; Check player collides with enemies
	FOR_VIS		Y , BULLET , BULLETS , BULLET_HIT_PLAYER	; Check player collides with bullets

	FOR_VIS		Y , BULLET , BULLETS , BULLET_HIT_OBJECTS	; Check bullets collides with objects

	FOR_VIS		Y , ASTROID, ASTROIDS, FRAME_HIT			; Check astroids inside boundry (Left and right boundry)
	FOR_VIS		Y , ENEMY  , ENEMIES , FRAME_HIT			; Check enemies inside boundry (Left and right boundry)
	FOR_VIS		Y , BULLET , BULLETS , FRAME_HIT			; Check bullets inside boundry (Left and right boundry)
ret

;;------------------------------------
;;---------- PLAYER_FRAME_BOX
;;--- Uses  Z-ptr
;; Keep player inside frame
PLAYER_FRAME_BOX:
	lds		r16, POS_X
	cpi		r16, FRAME_MIN_X						; Check left boundry
	brsh	SKIP_LEFT
	ldi		r16, FRAME_MIN_X
	sts		POS_X, r16								; set POS_X
SKIP_LEFT:
	lds		r16, POS_X
	cpi		r16, FRAME_MAX_X - (2*PLAYER_SIZE_X)	; Check right boundry
	brlo	SKIP_RIGHT
	ldi		r16, FRAME_MAX_X - (2*PLAYER_SIZE_X)
	sts		POS_X, r16								; set POS_X 
SKIP_RIGHT:
	lds		r16, POS_Y
	cpi		r16, FRAME_MIN_Y						; Check upper boundry
	brsh	SKIP_UP
	ldi		r16, FRAME_MIN_Y
	sts		POS_Y, r16								; set POS_Y
SKIP_UP:
	lds		r16, POS_Y
	cpi		r16, FRAME_MAX_Y - PLAYER_SIZE_Y		; Check lower boundry
	brlo	SKIP_DOWN
	ldi		r16, FRAME_MAX_Y - PLAYER_SIZE_Y
	sts		POS_Y, r16								; set POS_Y
SKIP_DOWN:
	nop
ret

;;------------------------------------
;;--------- ENEMY_FRAME_BOX
;;---Req  Y-ptr 
;; Keep enemy inside upper and lower boundry 
ENEMY_FRAME_BOX:
	push	r16										
	ldd		r16, Y + Y_ID							; Check upper boundry
	cpi		r16, FRAME_MIN_Y
	brsh	Skip1
	ldi		r16, FRAME_MIN_Y
	std		Y + Y_ID, r16							; set Y-pos
Skip1:
	ldd		r16, Y + Y_ID							; Check lower boundry
	cpi		r16, FRAME_MAX_Y - ENEMY_SIZE_Y
	brlo	Skip2
	ldi		r16, FRAME_MAX_Y - ENEMY_SIZE_Y
	std		Y + Y_ID, r16							; set Y-pos
Skip2:
	pop		r16
ret

;;------------------------------------
;;----------- GCHECK_PLAYER_HIT
;;---Req  Y-ptr | Load Z-ptr to PLAYER
;; Check if player collide with objects of size SPRITE_SIZE
CHECK_PLAYER_HIT:
	push	r16
	push	r17

	LOAD_Z	PLAYER
	ldd		r16, Z + OBJECT_ID
	sbrs	r16, VIS_FLAG									; Skip if visible
	rjmp	NO_CHECK_PLAYER_HIT								; No check when player dead

	ldd		p_pos, Z + X_ID									; Check if hit in X-axis
	ldd		t_pos, Y + X_ID
	CHECK_HIT	p_pos, PLAYER_SIZE_X, t_pos, SPRITE_SIZE	; Object size = SPRITE_SIZE
	brtc	NO_CHECK_PLAYER_HIT
							
	ldd		p_pos, Z + Y_ID									; Check if hit in Y-axis
	ldd		t_pos, Y + Y_ID
	CHECK_HIT	p_pos, PLAYER_SIZE_Y, t_pos, SPRITE_SIZE	; Object size = SPRITE_SIZE
	brtc	NO_CHECK_PLAYER_HIT								
		
		call	PLAYER_HIT	; If hit						
NO_CHECK_PLAYER_HIT:
	pop		r17
	pop		r16
ret
;;------------------------------------
;;----------- Bullet hit player
;;---Req  Y-ptr | uses Z-ptr
;; Check if player collides with objects of size BULLET_SIZE X/Y
BULLET_HIT_PLAYER:
	push	r16
	push	r17

	LOAD_Z	PLAYER
	ldd		r16, Z + OBJECT_ID
	sbrs	r16, VIS_FLAG									; Skip if visible
	rjmp	NO_BULLET_HIT_PLAYER							; No check when player dead

	ldd		p_pos, Z + X_ID								; Check if hit in X-axis
	ldd		t_pos, Y + X_ID
	CHECK_HIT	p_pos, PLAYER_SIZE_X, t_pos, BULLET_SIZE_X	; Object size = BULLET_SIZE_X
	brtc	NO_BULLET_HIT_PLAYER
							
	ldd		p_pos, Z + Y_ID								; Check if hit in Y-axis
	ldd		t_pos, Y + Y_ID
	CHECK_HIT	p_pos, PLAYER_SIZE_Y, t_pos, BULLET_SIZE_Y  ; Object size = BULLET_SIZE_Y
	brtc	NO_BULLET_HIT_PLAYER

		call	PLAYER_HIT	; If hit
NO_BULLET_HIT_PLAYER:
	pop		r17
	pop		r16
ret
;;------------------------------------
;;--------- PLAYER_HIT
;;--- Req Y-ptr
PLAYER_HIT:
	call	CLEAR									; Remove object colliding with player
	lds		r16, HP									; Remaning HP
	subi	r16, 1
	cpi		r16, 0
	breq	HP_ZERO
		lds		r16, HP
		ldi		r16, $7F							; Player VIS_FLAG == FALSE - Re-enable by fire
		sts		PLAYER, r16
		ldi		r16, PLAYER_DEAD_SPRITE				; Set Player dead sprite
		sts		PLAYER + 1, r16
		call	PLAYER_DEAD_SOUND					; Sound for crashing
		
		lds		r16, HP								; FOR ( r16 ) 
		FOR_VIS	 Y, LIFE, LIFES, HP_DOWN 
		DECSRAM	HP									; Remove 1 HP
		rjmp	HP_LEFT
	HP_ZERO:
		call	PLAYER_DEAD_SOUND						; Sound for crashing
		
		call	NEW_GAME	
HP_LEFT:
ret
;;-------- HP_DOWN -------
;;---Req  Y-ptr | Loops thru lifes untill r16 == 0 
HP_DOWN:
	dec		r16
	brne	RE_LOOP
		call	CLEAR			; Remove object at Y-ptr ( Remove one HP from screen )
RE_LOOP:
ret

;;------------------------------------
;;---------- BULLET_HIT_OBJECTS
;;---(Req Y-ptr)
;;---Uses  Z-ptr, compares with Y-ptr
BULLET_HIT_OBJECTS:
	FOR_VIS		Z , ASTROID, ASTROIDS, ASTROID_HIT			; Astroids indestructable
	FOR_VIS		Z , ENEMY  , ENEMIES , BULLET_HIT_ENEMY		; Player bullet hit Enemy
	FOR_VIS		Z , BULLET , BULLETS , BULLET_HIT_BULLET	; Player bullet hit enemy bullet
ret
;;---------------------------
;;---------- IF_BULLET_HIT
;;--- Req Z-ptr and Y-ptr
BULLET_HIT_ENEMY:
	push	r16
	push	r17
	ldd		r16, Y + OBJECT_ID
	sbrs	r16, PSD_FLAG		; Skip if Enemy shot	( Friendly fire for enemies )
	rjmp	NO_BULLET_HIT
	mov		r16, ZL
	mov		r17, YL		
	cp		r16, r17			; If match Y- and Z-ptr in same object
	breq	NO_BULLET_HIT		; Skip compare 

	ldd		p_pos, Y + X_ID
	ldd		t_pos, Z + X_ID		; 
	CHECK_HIT	p_pos, BULLET_SIZE_X, t_pos, ENEMY_SIZE_X	; Check hit X-axis
	brtc	NO_BULLET_HIT									; Objects of size  SPRITE_SIZE 

	ldd		p_pos, Y + Y_ID
	ldd		t_pos, Z + Y_ID
	CHECK_HIT	p_pos, BULLET_SIZE_Y, t_pos, ENEMY_SIZE_Y	; Check hit Y-axis
	brtc	NO_BULLET_HIT									; Objects of size  SPRITE_SIZE 
MATCH:
	call	CLEAR									; remove object at Y-ptr
	ldd		r16, Z + OBJECT_ID
	clr		r16										; remove object at Z-ptr 
	std		Z + OBJECT_ID, r16
	call	PREPARE_RANDOM_SOUND					; Sound for crashing
	call	HIGH_SCORE								; Inc game score
NO_BULLET_HIT:
	pop		r17
	pop		r16
ret

;;---------------------------
;;---------- BULLET_HIT
;;--- Req Z-ptr and Y-ptr
BULLET_HIT_BULLET:
	push	r16
	push	r17
	ldd		r16, Y + OBJECT_ID
	sbrs	r16, PSD_FLAG		; Skip if Enemy shot	( Friendly fire for enemies )
	rjmp	NO_BULLET_HIT2
	mov		r16, ZL
	mov		r17, YL		
	cp		r16, r17			; If match Y- and Z-ptr in same object
	breq	NO_BULLET_HIT		; Skip compare 

	ldd		p_pos, Y + X_ID
	ldd		t_pos, Z + X_ID		; 
	subi	t_pos, 2			; Make it easier to hit bullets
	CHECK_HIT	p_pos, BULLET_SIZE_X, t_pos, BULLET_SIZE_X + 4	; Check hit X-axis
	brtc	NO_BULLET_HIT2									; Objects of size  SPRITE_SIZE 
	
	ldd		p_pos, Y + Y_ID
	ldd		t_pos, Z + Y_ID
	subi	t_pos, 2			; Make it easier to hit bullets
	CHECK_HIT	p_pos, BULLET_SIZE_Y, t_pos, BULLET_SIZE_Y + 4	; Check hit Y-axis
	brtc	NO_BULLET_HIT2									; Objects of size  SPRITE_SIZE 
MATCH2:
	call	CLEAR									; remove object at Y-ptr
	ldd		r16, Z + OBJECT_ID
	clr		r16										; remove object at Z-ptr 
	std		Z + OBJECT_ID, r16
	;call	PREPARE_ENEMY_HIT_SOUND					; Sound for crashing
	call	PREPARE_SPAWN_SOUND
NO_BULLET_HIT2:
	pop		r17
	pop		r16
ret

;;----------------------------------
;;---------- ASTROID_HIT
;;--- Req Z-ptr and Y-ptr
ASTROID_HIT:
	mov		r16, ZL
	mov		r17, YL		
	cp		r16, r17			; If match Y- and Z-ptr in same object
	breq	NO_BULLET_HIT1		; Skip compare 

	ldd		p_pos, Y + X_ID
	ldd		t_pos, Z + X_ID		; 
	CHECK_HIT	p_pos, BULLET_SIZE_X, t_pos, ASTROID_SIZE	; Check hit X-axis
	brtc	NO_BULLET_HIT1									; Objects of size  SPRITE_SIZE 

	ldd		p_pos, Y + Y_ID
	ldd		t_pos, Z + Y_ID
	CHECK_HIT	p_pos, BULLET_SIZE_Y, t_pos, ASTROID_SIZE	; Check hit Y-axis
	brtc	NO_BULLET_HIT1									; Objects of size  SPRITE_SIZE 
MATCH1:
	call	CLEAR									; remove object at Y-ptr
	ldd		r16, Y + OBJECT_ID
	call	PREPARE_ENEMY_HIT_SOUND					; Sound for crashing
NO_BULLET_HIT1:
ret

;;------------------------------------
;;			FRAME_HIT
;; Check if object is outside left and right boundry, if true => clear object
;;--- (Req Y-ptr)
FRAME_HIT:
	push	r17										; Check if higher then right frame side
	ldd		t_pos, Y + X_ID							; Newly spawn object always lower than right frame side
	cpi		t_pos, FRAME_MAX_X - SPRITE_SIZE + 1
	brsh	HIT_MATCH
		rjmp	NO_FRAME_HIT
HIT_MATCH:
	call	CLEAR									; Remove object
NO_FRAME_HIT:
	pop		r17
ret

;;------------------------------------
;;			HIGH_SCORE
;; Inc points, if >= 10 insert new sprite
;;--- (Req Y-ptr)
HIGH_SCORE:
	LOAD_Z	SCORE
RECURSIVELY:
	ldi		r16, (1 << VIS_FLAG)
	std		Z + OBJECT_ID, r16		; Set number VIS_FLAG

	ldd		r16, Z + SPRITE_ID		; Load number value
	inc		r16
	cpi		r16, 10					; SPRITE_ID:s value never over 10
	breq	INC_NEXT_NUM

		std		Z + SPRITE_ID, r16			; Store number value
		rjmp	SCORE_RETURN

INC_NEXT_NUM:
	clr		r16
	std		Z + SPRITE_ID, r16		; Clear number value
	adiw	ZH:ZL, OBJECT_BYTES		; Set Z-ptr to next number
	rjmp	RECURSIVELY				

SCORE_RETURN:
ret

;////////////////////////////////////////////////////////////////////////////////////////////////////////////






;;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;;=====================					NEW_GAME					=========================================
;;------ Clear SRAM, set player start pos, set LEVEL = GAME_LEVEL
;;--- Uses Y-ptr, Z-ptr
NEW_GAME:
	;ldi		r16, 255
	clr		r17
WAIT_A_LITTLE:						; Delay before reset can be made
	clr		r18
WAIT_A_LITTLE_MORE:
	ldi		r19, 5
WAIT_A_LOT:
	dec		r19
	brne	WAIT_A_LOT
	dec		r18
	brne	WAIT_A_LITTLE_MORE
	dec		r17
	brne	WAIT_A_LITTLE
LOOPEN:
	;call	PREPARE_RANDOM_SOUND
	
	call	PREPARE_SPAWN_SOUND

	ldi		r19, 15
	ldi		r17, 255
DELAY1:	
	ldi		r18, 150
DELAY2:
	sbis	PINA, JOY_FIRE
	rjmp	END_LOOPEN

	dec		r18
	brne	DELAY2

	dec		r17
	brne	DELAY1
	
	dec		r19
	brne	DELAY1
	
	;dec		r16	
	rjmp	LOOPEN
END_LOOPEN:
	;ldi		r16, 0
	;sts		TICS, r16
	call	RANDOM_SEED						; Reset SEED			
	call	RANDOM_SEED	
	;call	PLAY_TUNE						; PLAY_TUNE - Not working / gives bad sound								
	FOR		Y, PLAYER, OBJECTS, CLEAR		; Clear Object_id, X_ID, Y_ID in all objects in SRAM

	LOAD_Z	PLAYER									; set VIS_FLAG for player
	ldd		r16, Z + OBJECT_ID
	sbr		r16, (1 << VIS_FLAG)			 
	std		Z + OBJECT_ID, r16

	ldi		p_pos, FRAME_MIN_X + 10					; set player POS_X, POS_Y
	sts		POS_X, r16
	ldi		p_pos, ( (FRAME_MAX_Y - FRAME_MIN_Y ) / 2 )
	sts		POS_Y, r16
	ldi		r16, START_LEVEL							
	sts		LEVEL, r16								; set LEVEL
	ldi		r16, (ASTROID_SPAWN_TIMER / 2 )
	sts		ASTROID_TIC, r16						; set tic for astroid
	clr		r16
	sts		ENEMY_TIC, r16							; set tic for enemy

	ldi		r16, (SPRITE_SIZE*2)					; HP start X-position
	FOR		Y, LIFE, LIFES, NEW_LIFE				; Set VIS_FLAG true for HP on screen

	ldi		r16, LIFES								; Set lifes
	sts		HP, r16

	ldi		r16, (WINDOW_SIZE_X - (SCORE_SPRITE_SIZE_X + 3 ))	; Score start X-position
	FOR		Y, SCORE, SCORE_NUM, RESET_HIGH_SCORE	; Reset high score
ret

;;------------------------------------
;;		RESET_HIGH_SCORE
RESET_HIGH_SCORE:
	ldi		r17, 0
	std		Y + OBJECT_ID, r17									; Set non-visible
	std		Y + SPRITE_ID, r17									; Set sprite_id to 0
	ldi		r17, WINDOW_SIZE_Y - ( SCORE_SPRITE_SIZE_Y + 1 )	; Score Y-position
	std		Y + X_ID, r16
	std		Y + Y_ID, r17
	subi	r16, SCORE_SPRITE_SIZE_X + 2 			; Dec r16 for next call
ret

;;------------------------------------
;;			NEW_LIFE
NEW_LIFE:
	ldi		r17, $80								; set VIS_FLAG
	std		Y + OBJECT_ID, r17 
	ldi		r17, WINDOW_SIZE_Y - 8					; HP Y-position
	std		Y + X_ID, r16							; X-pos = r16
	std		Y + Y_ID, r17							; Y-pos = r17
	subi	r16, (-SPRITE_SIZE)						; Inc r16 for next call
ret

;;------------------------------------
;;				CLEAR
;; Remove object at Y-ptr, clearing r16 sets VIS_FLAG = 0
;;---  Req Y-ptr 
CLEAR:
	push	r16
	;ldd		r16, Y + OBJECT_ID	
	clr		r16								; Clear flags (0 << VIS_FLAG) | (0 << PSD_FLAG) | (0 << INV_FLAG)
	std		Y + OBJECT_ID, r16
	std		Y + X_ID, r16
	std		Y + Y_ID, r16
	pop		r16
ret

;;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\





;;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;;=====================				INIT_SPRITE_SETTINGS			=========================================
;;--- Sets sprite_id for objects
INIT_SPRITE_SETTINGS:
	LOAD_Z	PLAYER								; set player sprite = 0
	ldi		p_pos, PLAYER_SPRITE_ID
	std		Z + SPRITE_ID, p_pos

	ldi		r16, ASTROID_SPRITE_ID
	FOR		Y, ASTROID, ASTROIDS, SET_SPRITE_ID		; Set astroid sprite
	ldi		r16, ENEMY_SPRITE_ID
	FOR		Y, ENEMY  , ENEMIES , SET_SPRITE_ID		; set enemy sprite
	ldi		r16, BULLET_SPRITE_ID
	FOR		Y, BULLET , BULLETS , SET_SPRITE_ID		; set bullet sprite 
	ldi		r16, HP_SPRITE_ID
	FOR		Y, LIFE   , LIFES   , SET_SPRITE_ID		; set HP sprite
	clr		r16
	FOR		Y, SCORE , SCORE_NUM, SET_SPRITE_ID		; set 0 to score sprites
ret
;----------------------
SET_SPRITE_ID:									
	std		Y + SPRITE_ID, r16
ret

;////////////////////////////////////////////////////////////////////////////////////////////////////////////





;;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;;=====================				INTERUPPT_TIC_COUNT				=========================================
;;--- Only uses r16 and pushes r16 in macro
INTERUPPT_TIC_COUNT:
	push	r16
	in		r16,SREG
	push	r17
	push	r18
	push	r19
	;push	r20		
	;push	r21
	;push	r22
	PUSH_Z
	;PUSH_Y			

	INCSRAM	TICS						
	INCSRAM	ASTROID_TIC
	INCSRAM	ENEMY_TIC
	
	; sbic	TIMSK, OCIE2				; Check if music interupt match register is enabled
	; call	KEEP_ROCKING				; Music handling ( ; PLAY_TUNE - Not working / gives bad sound )

	;POP_Y
	POP_Z
	;pop		r22
	;pop		r21
	;pop		r20
	pop		r19
	pop		r18
	pop		r17			
	out		SREG, r16
	pop		r16
reti

;;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;;=====================				SOUND EFFECTS INTERUPT			=========================================

;;----------------------------------
;;----- SHOT_SOUND - interupt
SHOOT_SOUND:
		push	r16
		in		r16,SREG
		push	r16
		clr		r16
		out		TCNT0,r16						; Nollst�ller timer 0 r�knarv�det
		INCSRAM COUNTER_SHOOT
		lds		r16,COUNTER_SHOOT
		cpi		r16,SHOOT_SOUND_TIME
		brne	SHOOT_BEEP
END_SOUND:
		ldi		r16,(1<<OCIE1A) ; (1<<OCIE2)	; (1<<TOIE1)|St�nger av timer 0 avbrottet eftersom tillr�ckligt med ljud nu har skickats ut											
		out		TIMSK,r16
		clr		r16
		sts		COUNTER_SHOOT,r16				; Nollst�ller r�knaren
		ldi		r16, TRUE
		sts		PLAY, r16						; Re-enable PLAY
		rjmp	DONE3
SHOOT_BEEP:
		sbrc	r16, 0	;r20
		rjmp PIN_HIGH3
PIN_LOW3:
		sbi	PORTA,7
		rjmp	DONE3
PIN_HIGH3:
		cbi	PORTA,7
DONE3:
		INCSRAM		SEED
		pop r16
		out SREG,r16
		pop r16
reti
;;############################################################################################################
;;####################################### SOUND EFFECTS   ####################################################

;;----------------------------------
;;----- PLAYER_DEAD_SOUND
PLAYER_DEAD_SOUND:
		push r16
		push r17
		push r18
		push r19
		push r20 
		in r16,SREG
		push r16

		ldi r20,$91
	   	ldi r19,PLAYER_DEAD_SOUND_TIME
DELAY:

		ldi r18,255   
DELAY_T:				 	
		ldi r17,FREQUENCY_PLAYER_DEAD_SOUND		
DELAY_T_REPEAT:
		ldi r16,$FF
DELAYLOOP_T:
		dec	r16
		brne	DELAYLOOP_T
		dec r17
		brne DELAY_T_REPEAT
		
		sbrc	r18, 0	;r20
		rjmp PIN_HIGH1
PIN_LOW1:
		sbi	PORTA,7
		rjmp	DONE1
PIN_HIGH1:
		cbi	PORTA,7
DONE1:
		dec r18
		brne DELAY_T
		dec r19
		brne DELAY

		pop r16
		out SREG,r16
		pop r20
		pop r19
		pop r18
		pop r17
		pop r16
ret

;;----------------------------------
;;----- PREPARE_SHOOT_SOUND
PREPARE_SHOOT_SOUND:
		ldi		r17, FREQUENCY_SHOOT_SOUND	
		call	SET_SOUND_EFFECT
ret
;;----------------------------------
;;----- PREPARE_ENEMY_HIT_SOUND
PREPARE_ENEMY_HIT_SOUND:
		ldi		r17, FREQUENCY_ENEMY_SOUND							
		call	SET_SOUND_EFFECT
ret
;;----------------------------------
;;----- PREPARE_SPAWN_SOUND
PREPARE_SPAWN_SOUND:
		lds		r17, SEED						
		call	SET_SOUND_EFFECT
ret
;;----------------------------------
;;----- PREPARE_RANDOM_SOUND
PREPARE_RANDOM_SOUND:
		lds		r17, SEED
		lsl		r17
		call	SET_SOUND_EFFECT
ret

SET_SOUND_EFFECT:
		ldi		r16, FALSE
		sts		PLAY, r16						; Disable PLAY
		clr		r16
		sts		COUNTER_SHOOT, r16				; Reset counter 
		out		OCR0,r17						; Set match register 

		ldi		r16, (1<<OCIE0)|(1<<OCIE1A) ;| (1<<OCIE2) Timer 2 not in use			
		out		TIMSK,r16
ret
;;############################################################################################################

;; Atempt to fix music - sounds bad...

;////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
;;----------------------------------	; Not in use
;;----- MUSIC_HANDLING
KEEP_ROCKING:
	lds		r17, PLAY					; Check if playing tune
	cpi		r17, TRUE
	brne	NO_MUSIC

	lds		r18, TONE_TIC				; DECSRAM TONE_TICS
	dec		r18							; if Zero get new tone
	breq	NEW_TONE
	sts		TONE_TIC, r18
	rjmp	CONTINUE_PLAY_TONE
	
NEW_TONE:
	lds		ZH, ptrH
	lds		ZL, ptrL					; Load ptr
	;adiw	ZH:ZL, 2
	
	lpm		r18, Z+
	cpi		r18, 0						; If .eof char
	brne	KEEP_PLAYING
//End of song
	call	PLAY_TUNE
	rjmp	CONTINUE_PLAY_TONE
	ldi		r18,(1<<TOIE0)|(1<<OCIE1A)  ; End of song
	out		TIMSK,r18					; Disable OCI2 interupt
	ldi		r18, FALSE
	sts		PLAY, r18					; Disable play

KEEP_PLAYING:
	out		OCR2, r18					; Set freq to match register
	lpm		r17, Z+					
	sts		TONE_TIC, r17				; Set tone length
	sts		ptrH, ZH					; Set ptr
	sts		ptrL, ZL
CONTINUE_PLAY_TONE:
NO_MUSIC:
ret
*/
/*
;;######################################################
;;######## MUSIC_INTERUPT ##############################	Not in use
MUSIC_INTERUPT:
	push	r16
	in		r16, SREG		; Push r16, r17 save SREG
	push	r17

	lds		r17, PLAY					; Check if playing tune
	cpi		r17, TRUE
	brne	NO_TUNE
	
;	in		r17, OCR2
;	dec		r17
;	out		OCR2, r17
	
	inc		r24							; Half on half off
	sbrc	r24, 0
	rjmp	PIN_HIGH
PIN_LOW:
	cbi		PORTA, 7			
	rjmp	NO_TUNE
PIN_HIGH:
	sbi		PORTA, 7
NO_TUNE:
	call	RANDOM_SEED					; Randomize SEED

	pop		r17
	out		SREG, r16		; pop r16, r17 restore SREG
	pop		r16
reti
*/
;////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
;;######################################################################################
;;############################## PLAY_TUNE  ############################################
PLAY_TUNE:
	PUSH_Z
	ldi		ZH,HIGH (FUR_ELISE*2)		; Load pointer at song
	ldi		ZL,LOW (FUR_ELISE*2)
				
	lpm		r16, Z+						; Get Counter value
	out		OCIE2, r16					; Set freq to match register
										
	lpm		r16, Z+						; Get tone length
	sts		TONE_TIC, r16				; Set tone length
	ldi		r16, TRUE
	sts		PLAY, r16					; Enable play song
	ldi		r16, (1<<OCIE1A)|(1 << OCIE2); Enable Timer 2 interupts
	out		TIMSK, r16
	sts		ptrH, ZH					; Save pointer to SRAM
	sts		ptrL, ZL
	POP_Z
ret
*/