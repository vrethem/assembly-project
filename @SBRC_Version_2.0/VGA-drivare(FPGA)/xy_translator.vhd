library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity xy_translator is
	port( x : in std_logic_vector(9 downto 0);
			y : in std_logic_vector(8 downto 0);
			adr : out std_logic_vector(14 downto 0));
end xy_translator;

architecture xy_translator_beh of xy_translator is
	signal y_shift_5 : std_logic_vector(11 downto 0);
	signal y_shift_7 : std_logic_vector(13 downto 0);
begin

	-- Compensated two shifts because in-signal need to be /4
	y_shift_5 <= y(8 downto 2) & "00000";
	y_shift_7 <= y(8 downto 2) & "0000000";
	
	-- adr = y*160 + x
	-- Explicit concatenate to 15bit width
	--adr <= y_shift_5 + ('0' & y_shift_7) + x; 
	adr <= y_shift_5 + ('0' & y_shift_7) + x(9 downto 2); 

	
end xy_translator_beh;