;; ===========================================================================
;;					USABLE MACRO'S
;; ===========================================================================

;;=================
;;	INC / DEC SRAM
;; Increse / decrease value at adress @0 
.macro	INCSRAM
	push	r16
	lds		r16, @0
	inc		r16
	sts		@0, r16
	pop		r16
.endmacro
.macro	DECSRAM
	push	r16
	lds		r16, @0
	dec		r16
	sts		@0, r16
	pop		r16
.endmacro

;;=================
;;	MACRO FOR Z-ptr
.macro	PUSH_Z	
	push	ZH
	push	ZL
.endmacro
.macro	POP_Z
	pop		ZL
	pop		ZH
.endmacro
.macro LOAD_Z 
	ldi		ZL, LOW(@0)
	ldi		ZH, HIGH(@0)
.endmacro

;;=================
;;	MACRO FOR Z-ptr
.macro	PUSH_Y	
	push	YH
	push	YL
.endmacro
.macro	POP_Y
	pop		YL
	pop		YH
.endmacro
.macro LOAD_Y 
	ldi		YL, LOW(@0)
	ldi		YH, HIGH(@0)
.endmacro

;;=====================================================
;;============== FOR_VIS  
;; For visible objects in SRAM
;;	Sets Y-ptr, calls subroutine for all visible objects in range
;; @ Pointer, @1 Start in SRAM, @2 Iterations, @3 calls function
.macro	FOR_VIS	
push	r19
push	r20
	ldi		@0L, LOW (@1)
	ldi		@0H, HIGH(@1)	
	ldi		r19, 0
LOOP:
	ldd		r20, @0 + OBJECT_ID
	clt
	sbrc	r20, VIS_FLAG		
	call	@3			; Skip if !(VIS_FLAG)
	
	brts	DONE_2
	adiw	@0H:@0L, OBJECT_BYTES
	inc		r19
	cpi		r19, @2
	brne	LOOP
DONE_2:
pop		r20
pop		r19
.endmacro

;;=====================================================
;;============== FIND_INV  
;;  Find none-visible object in SRAM
;;	Sets Y-ptr, exit after return from subroutine
;; @0 Pointer, @1 Start in SRAM, @2 Iterations, @3 call function
.macro FIND_INV
push	r19
push	r20
	ldi		@0L, LOW (@1)
	ldi		@0H, HIGH(@1)	
	clr		r19
LOOP_1:
	ldd		r20, @0 + OBJECT_ID
	clt							; clear T in SREG
	sbrs	r20, VIS_FLAG
	call	@3					; Skip if (VIS_FLAG)

	brts	DONE_1				; branch if T-flag set
	adiw	@0H:@0L, OBJECT_BYTES
	inc		r19
	cpi		r19, @2 
	brne	LOOP_1
	//
	//	Add code for SRAM is full
	//
DONE_1:
pop		r20
pop		r19
.endmacro

;;=====================================================
;;============== FOR
;; For objects in SRAM
;;	Sets @0-ptr, call subroutine for each object
;; @0 Pointer, @1 Start in SRAM, @2 Iterations, @3 call function
.macro FOR
push	r19
push	r20
	ldi		@0L, LOW (@1)
	ldi		@0H, HIGH(@1)	
	clr		r19
LOOP_2:
	ldd		r20, @0 + OBJECT_ID
	call	@3			
	adiw	@0H:@0L, OBJECT_BYTES
	inc		r19
	cpi		r19, @2 
	brne	LOOP_2

	pop		r20
	pop		r19
.endmacro

;;=====================================================
;;==============  CHECK_HIT
;;	Req r16, r17 set 
;;	@0 p_pos,	@1 p_pos.size(),	@2 t_pos,	@3 t_pos.size()
;;	sets T-flag if collision 
.macro	CHECK_HIT
	push	r18
	clr		r18
	clt					; Clear T-flag
	or		r18, @0
	sub		@0, @2 
	brlo	NEGATIVE	; Branch if overflow set
	
	cpi		@0, @3
	brsh	NO_HIT 
	rjmp	HIT
NEGATIVE:
	sub		@2, r18
	cpi		@2, @1 
	brsh	NO_HIT
HIT:
	set					
NO_HIT:
	pop		r18
.endmacro

;;=====================================================
;;==============  DEC_X / DEC_Y		
;;!!!OBS!!!  (Req Y-ptr)
;;	Decrease X_ID / Y_ID at Y-ptr current pos
.macro	DEC_X	; @0 Decrease by how much ( Negative value will increase )
	;push	r16
	ldd		r21, Y + X_ID
	subi	r21, @0
	std		Y + X_ID, r21
	;pop		r16
.endmacro
.macro	DEC_Y	; @0 Decrease by how much ( Negative value will increase )
	;push	r16
	ldd		r21, Y + Y_ID
	subi	r21, @0
	std		Y + Y_ID, r21
	;pop	r16
.endmacro

