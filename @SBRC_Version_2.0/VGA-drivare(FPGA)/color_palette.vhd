library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity color_palette is
	port( clk		: in std_logic;
			color 	: in std_logic_vector(3 downto 0);
			blanking : in std_logic;
			r			: out std_logic_vector(3 downto 0);
			g			: out std_logic_vector(3 downto 0);
			b			: out std_logic_vector(3 downto 0));
end color_palette;

architecture color_palette_beh of color_palette is
	component color_table
		PORT(	address	: IN STD_LOGIC_VECTOR (3 DOWNTO 0);
				clock		: IN STD_LOGIC;
				q			: OUT STD_LOGIC_VECTOR (11 DOWNTO 0));
	end component;
	
	signal colors : std_logic_vector(11 downto 0);
begin

	palette_rom : color_table
		PORT MAP(	clock => clk,
						address => color,
						q => colors);
						
	process(clk)
	begin
		if rising_edge(clk) then
			if blanking = '0' then
--				r <= "0000";
--				g <= "1111";
--				b <= "1111";
				r <= colors(3 downto 0);
				g <= colors(7 downto 4);
				b <= colors(11 downto 8);
			else
				r <= (others => '0');
				g <= (others => '0');
				b <= (others => '0');
			end if;
		end if;
	end process;


end color_palette_beh;