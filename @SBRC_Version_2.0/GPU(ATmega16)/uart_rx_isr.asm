/////////////////////////  uart_rx_isr.asm   ////////////////////////////
;
; UART Receive ISR, stores stuff to SRAM
; Uses registers
;	temp		= r16
.def	temp2		= r17
.def	uart_rx_byte	= r18

; CALL_ANIMATION <sprite_id>
.macro	CALL_ANIMATION
	cpi	uart_rx_byte, @0_ID
	brne	NOT_SPRITE_@0
	call	ANIMATE_@0
NOT_SPRITE_@0:
.endmacro

; FOUR_FRAME_ANIMATION <sprite_id> <delay>
.macro	FOUR_FRAME_ANIMATION
	ldi	uart_rx_byte, @0_ANIM
	lds	temp, ANIM_COUNT
	andi	temp, 3<<@1
	ldi	temp2, @1

FOUR_FRAME_ANIMATION_LOOP_@0:
	cpi	temp2, 0
	breq	FOUR_FRAME_ANIMATION_LOOP_END_@0
	lsr	temp
	dec	temp2
	rjmp	FOUR_FRAME_ANIMATION_LOOP_@0
FOUR_FRAME_ANIMATION_LOOP_END_@0:

	add	uart_rx_byte, temp
.endmacro

/////////////////////////  UART_RX_ISR   ////////////////////////////
UART_RX_ISR:
	push	temp
	in	temp, SREG
	push	temp
	push	uart_rx_byte

; Make sure the draw_frame routine is not running
	lds	temp, DONE
	cpi	temp, $00
						#ifdef	DEBUG
							brne	RX_TOO_FAST
						#endif
	brne	RETURN_UART_RX_ISR

	in	uart_rx_byte, UDR

; Go to current state
	lds	temp, STATE
	cpi	temp, RX_STATE_GET_ID
	breq	RX_STATE0
	cpi	temp, RX_STATE_GET_X
	breq	RX_STATE1
	cpi	temp, RX_STATE_GET_Y
	breq	RX_STATE2


RX_FORBIDDEN_STATE:	; We are not supposed to be here
						#ifdef	DEBUG
							jmp	RX_FORBIDDEN_STATE	; Debug lock
						#endif
	jmp	RESET


; Get sprite id
RX_STATE0:
	; Check for end of frame
	cpi	uart_rx_byte, RX_END_FRAME
	breq	UART_DONE

	; Sanity check
	cpi	uart_rx_byte, MAX_SPRITE_ID + 1
	brsh	RETURN_UART_RX_ISR

#ifdef ANIMATIONS
	call	CHOOSE_ANIMATION
#endif

	rcall	STORE_UART
	; Set next state
	ldi	temp, RX_STATE_GET_X
	sts	STATE, temp
	rjmp	RETURN_UART_RX_ISR

; Get xpos
RX_STATE1:
	; Check for end of frame
	cpi	uart_rx_byte, RX_END_FRAME
	breq	UART_OFF_SYNC		; Out of sync

	rcall	STORE_UART
	; Set next state
	ldi	temp, RX_STATE_GET_Y
	sts	STATE, temp
	rjmp	RETURN_UART_RX_ISR

; Get ypos
RX_STATE2:
	; Check for end of frame
	cpi	uart_rx_byte, RX_END_FRAME
	breq	UART_OFF_SYNC		; Out of sync

	rcall	STORE_UART

	; Increese OBJECT_COUNT in SRAM
	lds	temp, OBJECT_COUNT
	inc	temp
	sts	OBJECT_COUNT, temp

	; Set next state
	ldi	temp, RX_STATE_GET_ID
	sts	STATE, temp

	rjmp	RETURN_UART_RX_ISR


UART_OFF_SYNC:
	call	SOFT_INIT
	rjmp	RETURN_UART_RX_ISR

; Set DONE = 1 to indicate end of frame
UART_DONE:
	lds	temp, OBJECT_COUNT
	cpi	temp, 0
	breq	RETURN_UART_RX_ISR

	ldi	temp, $01
	sts	DONE, temp
	rjmp	RETURN_UART_RX_ISR

RETURN_UART_RX_ISR:
	pop	uart_rx_byte
	pop	temp
	out	SREG, temp
	pop	temp
	reti

RX_TOO_FAST:
	rjmp	RX_TOO_FAST

;_________________________UART_RX_ISR END_________________________


/////////////////////////  STORE_UART   ////////////////////////////
STORE_UART:		// Store register UART -> SRAM
	push	YH
	push	YL

	lds	YH, OBJECT_ADR+1	;Sets the high Y-pointer to the HIGH(adress)
	lds	YL, OBJECT_ADR		;Sets the low Y-pointer to the LOW(adress)

	st	Y+, uart_rx_byte	;Stores the sprite_ID where the adress is pointing and then adds the Y-pointer with 1.

	sts	OBJECT_ADR+1, YH	;Saves the new high part of adress into SRAM
	sts	OBJECT_ADR, YL		;Saves the new low part of adress into SRAM

	pop	YL
	pop	YH

	ret

	
CHOOSE_ANIMATION:
	CALL_ANIMATION PLAYER
	CALL_ANIMATION ENEMY
	CALL_ANIMATION ASTROID
	CALL_ANIMATION LIFE
	CALL_ANIMATION DEAD
	ret

/////////////////////////  ANIMATE_PLAYER   ////////////////////////////
ANIMATE_PLAYER:/*
	ldi	uart_rx_byte, PLAYER_ANIM
	lds	temp, ANIM_COUNT
	andi	temp, $06
	lsr	temp
	add	uart_rx_byte, temp*/
	FOUR_FRAME_ANIMATION PLAYER, 0
	ret
;_________________________ANIMATE_PLAYER END_________________________

/////////////////////////  ANIMATE_ENEMY   ////////////////////////////
ANIMATE_ENEMY:
/*
	ldi	uart_rx_byte, ENEMY_ANIM
	lds	temp, ANIM_COUNT
	andi	temp, $06
	lsr	temp
	add	uart_rx_byte, temp
	*/
	FOUR_FRAME_ANIMATION ENEMY, 2
	ret
;_________________________ANIMATE_ENEMY END_________________________

/////////////////////////  ANIMATE_ASTROID   ////////////////////////////
ANIMATE_ASTROID:
/*
	ldi	uart_rx_byte, ASTROID_ANIM
	lds	temp, ANIM_COUNT
	andi	temp, $06
	lsr	temp
	add	uart_rx_byte, temp
	*/
	FOUR_FRAME_ANIMATION ASTROID, 3
	ret
;_________________________ANIMATE_ASTROID END_________________________

/////////////////////////  ANIMATE_ASTROID   ////////////////////////////
ANIMATE_LIFE:
	ldi	uart_rx_byte, LIFE_ANIM
	lds	temp, ANIM_COUNT
	sbrc	temp, 3
	inc	uart_rx_byte
	ret
;_________________________ANIMATE_ASTROID END_________________________
/////////////////////////  ANIMATE_DEAD   ////////////////////////////
ANIMATE_DEAD:
	FOUR_FRAME_ANIMATION DEAD, 3
	ret
;_________________________ANIMATE_DEAD END_________________________
