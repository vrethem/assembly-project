;;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;;===============		COLLISION BOX				====================
;;--- Uses  Z-ptr, Y-ptr
;; Handles all collision check
;; Handles player and enemies position always inside frame box
COLLISION_BOX:
	call		PLAYER_FRAME_BOX						; Check if player inside boundries
	FOR_VIS		Y , ENEMY   , ENEMIES, ENEMY_FRAME_BOX	; Check if enemies inside boundry (Upper and lower boundry)

	FOR_VIS		Y , ASTROID, ASTROIDS, CHECK_PLAYER_HIT	; Check player collides with astroids
	FOR_VIS		Y , ENEMY  , ENEMIES , CHECK_PLAYER_HIT	; Check player collides with enemies
	FOR_VIS		Y , BULLET , BULLETS , BULLET_HIT_PLAYER; Check player collides with bullets

	FOR_VIS		Y , BULLET , BULLETS , BULLET_HIT_OBJECTS; Check bullets collides with objects

	FOR_VIS		Y , ASTROID, ASTROIDS, FRAME_HIT		; Check astroids inside boundry (Left and right boundry)
	FOR_VIS		Y , ENEMY  , ENEMIES , FRAME_HIT		; Check enemies inside boundry (Left and right boundry)
	FOR_VIS		Y , BULLET , BULLETS , FRAME_HIT		; Check bullets inside boundry (Left and right boundry)
ret

;;------------------------------------
;;---------- PLAYER_FRAME_BOX
;;--- Uses  Z-ptr
;; Keep player inside frame
PLAYER_FRAME_BOX:
	lds		r16, POS_X
	cpi		r16, FRAME_MIN_X							; Check left boundry
	brsh	SKIP_LEFT
		ldi		r16, FRAME_MIN_X
		sts		POS_X, r16								; set POS_X
SKIP_LEFT:
	lds		r16, POS_X
	cpi		r16, FRAME_MAX_X - (PLAYER_SIZE_X)			; Check right boundry
	brlo	SKIP_RIGHT
		ldi		r16, FRAME_MAX_X - (PLAYER_SIZE_X)
		sts		POS_X, r16								; set POS_X 
SKIP_RIGHT:
	lds		r16, POS_Y
	cpi		r16, FRAME_MIN_Y							; Check upper boundry
	brsh	SKIP_UP
		ldi		r16, FRAME_MIN_Y
		sts		POS_Y, r16								; set POS_Y
SKIP_UP:
	lds		r16, POS_Y
	cpi		r16, FRAME_MAX_Y - PLAYER_SIZE_Y			; Check lower boundry
	brlo	SKIP_DOWN
		ldi		r16, FRAME_MAX_Y - PLAYER_SIZE_Y
		sts		POS_Y, r16								; set POS_Y
SKIP_DOWN:
	nop
ret

;;------------------------------------
;;--------- ENEMY_FRAME_BOX
;;---Req  Y-ptr 
;; Keep enemy inside upper and lower boundry 
ENEMY_FRAME_BOX:								
	ldd		r16, Y + Y_ID									; Check upper boundry
	cpi		r16, FRAME_MIN_Y
	brsh	Skip1
		ldi		r16, FRAME_MIN_Y
		std		Y + Y_ID, r16								; set Y-pos
Skip1:
	ldd		r16, Y + Y_ID									; Check lower boundry
	cpi		r16, FRAME_MAX_Y - ENEMY_SIZE_Y
	brlo	Skip2
		ldi		r16, FRAME_MAX_Y - ENEMY_SIZE_Y
		std		Y + Y_ID, r16								; set Y-pos
Skip2:
ret

;;------------------------------------
;;----------- GCHECK_PLAYER_HIT
;;---Req  Y-ptr | Load Z-ptr to PLAYER
/*
	 Check if player collides with object of size SPRITE_SIZE
	 Skip if  player VIS_FLAG = 0, (No check when player dead)
*/
CHECK_PLAYER_HIT:
	LOAD_Z	PLAYER
	ldd		r16, Z + OBJECT_ID
	sbrs	r16, VIS_FLAG									
	rjmp	NO_CHECK_PLAYER_HIT								

	ldd		p_pos, Z + X_ID									; Check if hit in X-axis
	ldd		t_pos, Y + X_ID
	CHECK_HIT	p_pos, PLAYER_SIZE_X, t_pos, SPRITE_SIZE	; Object size = SPRITE_SIZE
	brtc	NO_CHECK_PLAYER_HIT
							
	ldd		p_pos, Z + Y_ID									; Check if hit in Y-axis
	ldd		t_pos, Y + Y_ID
	CHECK_HIT	p_pos, PLAYER_SIZE_Y, t_pos, SPRITE_SIZE	; Object size = SPRITE_SIZE
	brtc	NO_CHECK_PLAYER_HIT								
		call	PLAYER_HIT	; If hit						
NO_CHECK_PLAYER_HIT:
ret

;;------------------------------------
;;----------- Bullet hit player
;;---Req  Y-ptr | uses Z-ptr
/*
	Requiers Y-ptr set at bullet object
	Skip if player VIS_FLAG = 0, (No check when player dead)
	Check if player collides with objects of size BULLET_SIZE X/Y
*/
BULLET_HIT_PLAYER:
	LOAD_Z	PLAYER
	ldd		r16, Z + OBJECT_ID
	sbrs	r16, VIS_FLAG									
	rjmp	NO_BULLET_HIT_PLAYER						

	ldd		p_pos, Z + X_ID									; Check if hit in X-axis
	ldd		t_pos, Y + X_ID
	CHECK_HIT	p_pos, PLAYER_SIZE_X, t_pos, BULLET_SIZE_X	; Object size = BULLET_SIZE_X
	brtc	NO_BULLET_HIT_PLAYER
							
	ldd		p_pos, Z + Y_ID									; Check if hit in Y-axis
	ldd		t_pos, Y + Y_ID
	CHECK_HIT	p_pos, PLAYER_SIZE_Y, t_pos, BULLET_SIZE_Y  ; Object size = BULLET_SIZE_Y
	brtc	NO_BULLET_HIT_PLAYER
		call	PLAYER_HIT	; If hit
NO_BULLET_HIT_PLAYER:
ret

;;--------------------------------------->>
;;--------- PLAYER_HIT
;;--- Req Y-ptr
/*
	Remove object at Y-ptr ( object colliding with player )
	Set player SPRITE_ID to PLAYER_DEAD_SPRITE
	Set player VIS_FLAG = 0, remove one HP
	if ( HP == 0 ) End game
*/
PLAYER_HIT:
	call	CLEAR_Y								
	lds		r16, PLAYER
	cbr		r16, (1 << VIS_FLAG)				; set player VIS_FLAG == FALSE 
	sts		PLAYER, r16
	ldi		r16, PLAYER_DEAD_SPRITE				
	sts		PLAYER + SPRITE_ID, r16
	call	PLAYER_DEAD_SOUND					; Sound for crashing
		
	lds		r16, HP								; FOR ( r16 ) 
	FOR_VIS	 Y, LIFE, LIFES, HP_DOWN			; Remove 1 HP from screen
	DECSRAM	HP									; Remove 1 HP from reg
	lds		r16, HP
	cpi		r16, 0
	brne	HP_LEFT
		HP_ZERO:
			call	SEND_DATA					; Remove last Health Point from screen
			call	NEW_GAME	
HP_LEFT:
ret
;;-------- HP_DOWN -------
;;---Req  Y-ptr 
/* Loops thru lifes untill r16 == 0		*/ 
HP_DOWN:
	dec		r16
	brne	RE_LOOP
		call	CLEAR_Y			; Remove object at Y-ptr ( Remove one HP from screen )
		set
RE_LOOP:
ret
;;-------------------------------------------<<


;;------------------------------------
;;---------- BULLET_HIT_OBJECTS
;;---(Req Y-ptr)
;;---Uses  Z-ptr, compares with Y-ptr
BULLET_HIT_OBJECTS:
	FOR_VIS		Z , ASTROID, ASTROIDS, BULLET_HIT_ASTROID	; Bullets hit astroids 
	FOR_VIS		Z , ENEMY  , ENEMIES , BULLET_HIT_ENEMY		; Player bullet hit Enemy
	FOR_VIS		Z , BULLET , BULLETS , BULLET_HIT_BULLET	; Player bullet hit enemy bullet
ret

;;---------------------------
;;---------- BULLET_HIT_ENEMY
/*
	Check if bullet hit enemy
	Skip if Enemy shot	( Friendly fire for enemies )
	if True
		clear object at Y- and Z-ptr (VIS_FLAG = false)
		Inc score
		Play sound effect
*/
;;--- Req Z-ptr and Y-ptr
BULLET_HIT_ENEMY:
	ldd		r16, Y + OBJECT_ID
	sbrs	r16, PSD_FLAG		
	rjmp	NO_BULLET_HIT
	mov		r16, ZL
	mov		r17, YL		
	cp		r16, r17			; If match Y- and Z-ptr in same object
	breq	NO_BULLET_HIT		

	ldd		p_pos, Y + X_ID
	ldd		t_pos, Z + X_ID		; 
	CHECK_HIT	p_pos, BULLET_SIZE_X, t_pos, ENEMY_SIZE_X	; Check hit X-axis
	brtc	NO_BULLET_HIT									

	ldd		p_pos, Y + Y_ID
	ldd		t_pos, Z + Y_ID
	CHECK_HIT	p_pos, BULLET_SIZE_Y, t_pos, ENEMY_SIZE_Y	; Check hit Y-axis
	brtc	NO_BULLET_HIT								 
MATCH:
		call	CLEAR_Y								; remove object at Y-ptr (bullet)
		call	CLEAR_Z								; remove object at Z-ptr (enenym)
		call	PREPARE_RANDOM_SOUND_2				; Sound for crashing
		ldi		r16, ENEMY_SHOT_BONUS
BONUS_ENEMY:
		call	HIGH_SCORE							; Inc game score
		dec		r16
		brne	BONUS_ENEMY
NO_BULLET_HIT:
ret

;;---------------------------
;;---------- BULLET_HIT_BULLET
/*
	Check if player bullet hit enemy bullet
	Skip if Enemy shot, only necessary to check player bullets
	because all bullets at Y-ptr will check all enemy bullets at Z-ptr

	Inc BULLET_SIZE_X so a bullet cannot jump over annother bullet
	Same for BULLET_SIZE_Y make it easier to hit bullets
	if True
		CLEAR object at Y- and Z-ptr
		Play sound effect
*/
;;--- Req Z-ptr and Y-ptr
BULLET_HIT_BULLET:
	ldd		r16, Y + OBJECT_ID
	sbrs	r16, PSD_FLAG		
	rjmp	NO_BULLET_HIT2
	mov		r16, ZL
	mov		r17, YL		
	cp		r16, r17			; If Y- and Z-ptr in same object
	breq	NO_BULLET_HIT		

	ldd		p_pos, Y + X_ID
	ldd		t_pos, Z + X_ID		 
	subi	t_pos, 2			
	CHECK_HIT	p_pos, BULLET_SIZE_X, t_pos, BULLET_SIZE_X + 4	
	brtc	NO_BULLET_HIT2									
	
	ldd		p_pos, Y + Y_ID
	ldd		t_pos, Z + Y_ID
	subi	t_pos, 2			
	CHECK_HIT	p_pos, BULLET_SIZE_Y, t_pos, BULLET_SIZE_Y + 4	
	brtc	NO_BULLET_HIT2									
MATCH2:
	call	CLEAR_Y							; remove object at Y-ptr (enemy bullet)
	call	CLEAR_Z							; remove object at Z-ptr (player bullet)
	call	PREPARE_RANDOM_SOUND_1
NO_BULLET_HIT2:
ret

;;----------------------------------
;;-------- BULLET_HIT_ASTROID
/*
	Check if bullet hit astroid
	if True
		CLEAR_Y object at Y-ptr
		Play sound effect
*/
;;--- Req Z-ptr and Y-ptr
BULLET_HIT_ASTROID:
	ldd		p_pos, Y + X_ID
	ldd		t_pos, Z + X_ID		
	CHECK_HIT	p_pos, BULLET_SIZE_X, t_pos, ASTROID_SIZE	; Check hit X-axis
	brtc	NO_BULLET_HIT1									

	ldd		p_pos, Y + Y_ID
	ldd		t_pos, Z + Y_ID
	CHECK_HIT	p_pos, BULLET_SIZE_Y, t_pos, ASTROID_SIZE	; Check hit Y-axis
	brtc	NO_BULLET_HIT1									
MATCH1:
	call	CLEAR_Y									; remove bullet at Y-ptr
	call	PREPARE_ASTROID_HIT_SOUND				; Astroid hit sound effect
NO_BULLET_HIT1:
ret

;;------------------------------------
;;			FRAME_HIT
/*
 Check if object is outside left and right boundry,
 Newly spawn object always lower than right frame side
 if true	
	CLEAR_Y object at Y-ptr
*/
;;--- (Req Y-ptr)
FRAME_HIT:
	push	r17								
	ldd		r17, Y + X_ID						
	cpi		r17, FRAME_MAX_X + 2
	brsh	HIT_MATCH
		rjmp	NO_FRAME_HIT
HIT_MATCH:
	call	CLEAR_Y							
NO_FRAME_HIT:
	pop		r17
ret

;;------------------------------------
;;			HIGH_SCORE
/*
	Handles score points in current game
	Sprite_id representing a number sprite 
	Inc points, if >= 10 insert new sprite
*/
;;--- (Req Y-ptr)
HIGH_SCORE:
	LOAD_Z	SCORE
RECURSIVELY:
	ldi		r16, (1 << VIS_FLAG)
	std		Z + OBJECT_ID, r16		; Set number VIS_FLAG

	ldd		r16, Z + SPRITE_ID		; Load number value
	inc		r16
	cpi		r16, 10					; SPRITE_ID:s value never over 10
	breq	INC_NEXT_NUM

		std		Z + SPRITE_ID, r16	; Store number value
		rjmp	SCORE_RETURN
INC_NEXT_NUM:
	clr		r16
	std		Z + SPRITE_ID, r16		; CLEAR_Y number value
	adiw	ZH:ZL, OBJECT_BYTES		; Set Z-ptr to next number
	rjmp	RECURSIVELY				
SCORE_RETURN:
ret
;////////////////////////////////////////////////////////////