library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity vga_counter_testbench is
end vga_counter_testbench;

architecture vga_counter_testbench_beh of vga_counter_testbench is
  component vga_counter
	port( clk		: in std_logic;
			reset		: in std_logic;
			x			: out std_logic_vector(9 downto 0);
			y			: out std_logic_vector(8 downto 0);
			h_sync	: out std_logic;
			v_sync	: out std_logic;
			blanking	: out std_logic);
	end component;
	
	signal clk : std_logic := '0';
	signal reset : std_logic := '0';
	
	signal x : std_logic_vector(9 downto 0);
	signal y : std_logic_vector(8 downto 0);
	signal h_sync	: std_logic;
	signal v_sync	: std_logic;
	signal blanking	: std_logic;
	
	constant clk_period : time := 1 ns;
begin
  
  uut: vga_counter port map(
    clk => clk,
    reset => reset,
    x => x,
    y => y,
    h_sync => h_sync,
    v_sync => v_sync,
    blanking => blanking
  );
    
  clk_process :process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
  end process;
  
  init_process :process
  begin
      wait for 100 ps;
      reset <= '1';
      wait for 100 ps;
      reset <= '0';
      wait for 100 ms;
  end process;

end vga_counter_testbench_beh;