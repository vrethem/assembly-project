;;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;;============		GAME_LEVEL		====================================
;;--- Uses  Z-ptr, Y-ptr
/*	Spawn new Enemy or Astroid
	Checks value in ASTROID_TIC and ENEMY_TIC
	Increase LEVEL_TIC when spawning new astroid or enemy
	Increase LEVEL when LEVEL_TIC reaches LEVEL_UP_VALUE
	Increase score when LEVEL++
*/
GAME_LEVEL:					
	lds		r16, ASTROID_TIC			; Spawn new Astroid if		
	cpi		r16, ASTROID_SPAWN_TIMER	; ASTROID_TIC + LEVEL > ASTROID_SPAWN_TIMER
	brlo	NO_SPWN_A	
		lds		r16, LEVEL		
		sts		ASTROID_TIC,r16			; set ASTROID_TIC to current LEVEL
		call	NEW_ASTROID				; Spawn Astroid

NO_SPWN_A:
	lds		r16, ENEMY_TIC				; Spawn new Enemy if			
	cpi		r16, ENEMY_SPAWN_TIMER		; ENEMY_TIC + LEVEL > ENEMY_SPAWN_TIMER
	brlo	NO_SPWN_E			
		lds		r16, LEVEL
		sts		ENEMY_TIC, r16			; set ENEMY_TIC to current LEVEL
		call	NEW_ENEMY				; Spawn Enemy
NO_SPWN_E:
	nop
ret

;;------------------------------------
;;			NEW_ASTROID
;; Spawn new astroid-object, inc game difficulty
;;---Uses  Y-ptr
NEW_ASTROID:
	FIND_INV		Y, ASTROID, ASTROIDS, INSERT
	// Y-ptr is set to newly inserted astroid-object
	// Add code for Astroid flags
	call	LEVEL_UP									
ret

;;------------------------------------
;;			NEW_ENEMY
;;---Uses  Y-ptr
/*
	Spawn new enemy-object, inc game difficulty
	Clear Enemy movment bits 0-4
	If SEED is below value spawn AI_ENEMY
	calls INSERT
*/
NEW_ENEMY:
	FIND_INV		Y, ENEMY, ENEMIES, INSERT
	// Y-ptr is set to newly inserted enemy-object
	ldd		r16, Y + OBJECT_ID
	cbr		r16, $1F					
	lds		r17, SEED
	cpi		r17, 120 - AI_SPAWN_RATE	
	brpl	NEW_AI_CONTROLLED_ENEMY
	rjmp	NORMAL_ENEMY
NEW_AI_CONTROLLED_ENEMY:
	sbr		r16, (1 << AI_FLAG)
NORMAL_ENEMY:
	std		Y + OBJECT_ID, r16
	call	LEVEL_UP							
ret

;;------------------------------------
;;		LEVEL_UP
/*  
	Skip if player VIS_FLAG not set
	Inc LEVEL and check its not higher than max_level
*/
LEVEL_UP:
	lds		r16, PLAYER
	sbrc	r16, VIS_FLAG				
	rjmp	END_LEVEL_UP				; if player VIS_FLAG == false

	INCSRAM	 LEVEL_TIC
	lds		r16, LEVEL_TIC
	cpi		r16, LEVEL_UP_VALUE		
	brne	END_LEVEL_UP		
		clr		r16
		sts		LEVEL_TIC, r16			; Reset LEVEL_TIC
		lds		r16, LEVEL
		cpi		r16, MAX_LEVEL
		breq	END_LEVEL_UP
			INCSRAM LEVEL				; if not max_level inc LEVEL
			ldi		r16, BONUS_PER_LEVEL
LEVEL_BONUS:
			call	HIGH_SCORE			; Bonus score for level up
			dec		r16
			brne	LEVEL_BONUS
END_LEVEL_UP:
ret

;;------------------------------------
;;			INSERT
;;---Uses  Z-ptr, req Y-ptr
/*
	Gets call from FIND_INV 
	Sets coordinates for obejct at Y-ptr 
	X set to FRAM_MAX_X
	Y set to random value
	Sets T-flag in SREG -> Lets calling macro know object was inserted
*/
INSERT:
	push	r16
	ldd		r16, Y + OBJECT_ID
	ldi		r16, (1 << VIS_FLAG) 			; Set VIS_FLAG true
	std		Y + OBJECT_ID, r16	

	lds		r16, SEED						; Get random value (SEED) for Y-pos						
	std		Y + Y_ID, r16					; set Y-pos
	com		r16
	andi	r16, $7F						; Invert SEED for extra random
	sts		SEED, r16 

	ldi		r16, FRAME_MAX_X				; set X-pos
	std		Y + X_ID, r16

	set										; Set T-flag (to exit loop)
	pop		r16
ret
;///////////////////////////////////////////////////////////////////////