/////////////////////////  ram_allocation.inc   ////////////////////////////
;
; SRAM variables

.dseg
.org	SRAM_START

	STATE:		.byte 1
	DONE:		.byte 1

	ANIM_COUNT:	.byte 1
	ANIM_PRESCALER:	.byte 1

	OBJECT_COUNT:	.byte 1

	OBJECT_ADR:	.byte 2
	OBJECT_LIST:	.byte (MAX_OBJECTS * OBJECT_SIZE)
.cseg
