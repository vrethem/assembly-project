;; ===========================================================================
;;					GAME - SETTINGS
;;  Game settings
;;	Difficulty 
;;	Sprite settings
;;	Layout in sram
;; ===========================================================================
.equ	TRUE	= 1
.equ	FALSE	= 0
;;--------------------------------------------------------
;;					GAME SETTINGS
/*
	Game constants
*/
.equ BASE_CLK		= 252	; Interupt frequenzy ( Req timer1 to be set at BASE_CLK Hz )
.equ FPS			= 39	; Fps  
.equ GAME_SPEED		= FPS	; Update frame after (BASE_CLK / GAME_SPEED) second

.equ PLAYER_SPEED	= 2		; Pixel movments per tic
.equ ENEMY_SPEED	= 1		; Pixel movments per tic
.equ ASTROID_SPEED	= 1		; Pixel movments per tic
.equ BULLET_SPEED	= 3		; Pixel movments per tic
.equ RELOAD_TIME	= 4		; Player shots per second   ( GAME_SPEED/x = RELOAD_TIME <=> GAME_SPEED/RELOAD_TIME = x )
.equ ENEMY_RELOAD	= 1		; If SEED > 120 - ENEMY_RELOAD, insert enemy bullet

.equ ENEMIES		= 15	; Max number of enemies
.equ ASTROIDS		= 10	; Max number of Astroids
.equ BULLETS		= 25	; Max number of bullets
.equ LIFES			= 3		; Number of Health Points
.equ SCORE_NUM		= 5		; Max number of score numbers
.equ OBJECTS		= ( ENEMIES + ASTROIDS + BULLETS + LIFES + SCORE_NUM + 1 ) ;Max nbr of objects in game


;; --- Monitor mode ---
.equ MONITOR_MODE		= TRUE

;; --- Game level
.equ HARD				 = 100
.equ MEDIUM				 = 50
.equ EASY				 = 10
.equ START_LEVEL		 = EASY		; LEVEL set to START_LEVEL at new game
.equ MAX_LEVEL			 = 150		; LEVEL's maximum value
.equ LEVEL_UP_VALUE		 = 5		; After X object hit left screen LEVEL++

;; --- High Score options
.equ BONUS_PER_LEVEL	 = 2
.equ ENEMY_SHOT_BONUS	 = 5

;; ---(SPAWN TIMER)
.equ ASTROID_SPAWN_TIMER = 200		; Spawn after X * (1 / FPS) seconds	
.equ ENEMY_SPAWN_TIMER	 = 225		; Spawn after X * (1 / FPS) seconds

;; --- ENEMY AI ---
.equ AI_ENGAGE_X		 = 70		; Engage distance X
.equ AI_ENGAGE_Y		 = 80		; Engage distance Y
.equ AI_SPAWN_RATE		 = 20		; Higher value will increase possibility


;;--------------------------------------------------------
;;				SPRITE SETTINGS
/* 
	Size / Collision box of sprites 
	Sprite ID to objects
*/
.equ SPRITE_SIZE	= 8		; 8x8 pixels 
.equ PLAYER_SIZE_X	= 6		
.equ PLAYER_SIZE_Y	= 4		
.equ ASTROID_SIZE	= SPRITE_SIZE		
.equ ENEMY_SIZE_X	= SPRITE_SIZE	
.equ ENEMY_SIZE_Y	= SPRITE_SIZE
.equ BULLET_SIZE_X	= 1
.equ BULLET_SIZE_Y	= 1
.equ SCORE_SPRITE_SIZE_X = 3 
.equ SCORE_SPRITE_SIZE_Y = 5 

;; --- Sprite ID
.equ PLAYER_SPRITE_ID   = 10
.equ PLAYER_DEAD_SPRITE = 15
.equ ENEMY_SPRITE_ID    = 11
.equ ASTROID_SPRITE_ID  = 12
.equ BULLET_SPRITE_ID   = 13
.equ HP_SPRITE_ID	    = 14


;;--------------------------------------------------------
;;					WINDOW SIZE
/*
	Settting for window size
	Settting for game frame box
*/
.equ WINDOW_SIZE_X	= 160 + SPRITE_SIZE			;Number of pixel to paint to screen
.equ WINDOW_SIZE_Y	= 120 + SPRITE_SIZE

.equ FRAME_MIN_X	= SPRITE_SIZE				;Actual game size for moving objects
.equ FRAME_MAX_X	= 160 + SPRITE_SIZE
.equ FRAME_MIN_Y	= SPRITE_SIZE + 8 
.equ FRAME_MAX_Y	= 120 + SPRITE_SIZE - 8 


;;--------------------------------------------------------
;;				OBJECT LAYOUT	
/*
	Layout of objects
	Objects can represent diffrent types
	for example enemy, astroid, player, score number, health point
*/		
.equ OBJECT_BYTES	= 4		; 4 bytes per object
.equ OBJECT_ID		= 0		; Position of object_id in object
.equ SPRITE_ID		= 1		; Position of sprite_id in object
.equ X_ID			= 2		; Position of x_id in object
.equ Y_ID			= 3		; Position of y_id in object

;;	-------- Position of bits in OBJECT_ID --------
.equ VIS_FLAG		= 7		; Visible flag

;; Special bits for BULLET
.equ PSD_FLAG		= 6		; Positive Shot Direction

;; Special bits for ENEMY 
.equ AI_FLAG		= 6		; AI controlled enemy
; bit 0-4 in use for enemy movement pattern


;;=========================================================
;;========= MEMORY LAYOUT IN SRAM
/*
	Allocated memory in SRAM
 (*)ASTROID_TIC and ENEMY_TIC is set to current LEVEL after 
	each new spawn. When LEVEL increases astroids and enemies will spawn more frequently
	thus they will be closer to ASTROID_SPAWN_TIMER and ENEMY_SPAWN_TIMER
*/
.dseg
			.org	SRAM_START
TICS:		.byte	1			; Game update counter
SEED:		.byte	1			; Random value ( 0 - 120 )
JOY_BUFFER: .byte	1			; Input buffer
LEVEL:		.byte	1;(*)		; Current game level
LEVEL_TIC:  .byte	1			; LEVEL_TIC == LEVEL_UP_VALUE => LEVEL++
HP:			.byte	1			; Current Health Points

ASTROID_TIC:.byte	1			; ASTROID_TIC  > ASTROID_SPAWN_TIMER => spawn new enemy
ENEMY_TIC:  .byte	1			; ENEMY_TIC    > ENEMY_SPAWN_TIMER   => spawn new astroid
SHOT_TIC :	.byte	1			; Shot limit counter
PLAY_TUNE:	.byte	1			; Choose tune
Tone_TIC:   .byte	1			; (Sound effect) counter

PLAYER_AI_X: .byte	1			; Movement in X-axis
PLAYER_AI_Y: .byte	1			; Movement in Y-axis

;; --- Objects ----
PLAYER:		.byte	(Object_bytes - 2)
POS_X:		.byte	1							; Player pos
POS_Y:		.byte	1
ENEMY:		.byte	(ENEMIES  * OBJECT_BYTES)	; Nbr of enemies multiplied by nbr of B for one object
ASTROID:	.byte	(ASTROIDS * OBJECT_BYTES)
BULLET:		.byte	(BULLETS  * OBJECT_BYTES)
LIFE:		.byte	(LIFES	  * OBJECT_BYTES)
SCORE:		.byte	(SCORE_NUM* OBJECT_BYTES)
.cseg


;;=========================================================
;;========= Sound effects
;;
.equ TONE_LENGTH				 = 150	; Sound effect beep length
.equ FREQUENCY_SHOOT_SOUND		 =  50
.equ FREQUENCY_ENEMY_SOUND		 =  30	
.equ FREQUENCY_ASTROID_SOUND	 =  40
.equ PLAYER_DEAD_SOUND_TIME		 =   3 
.equ FREQUENCY_PLAYER_DEAD_SOUND =  10

;;----------------------------------------------
;;	---- JOYSTICK constants
.equ JOY_LEFT		= 0
.equ JOY_RIGHT		= 1
.equ JOY_UP			= 2
.equ JOY_DOWN		= 3
.equ JOY_FIRE		= 4
.equ SPEAKER_PIN	= 7

.def	p_pos	=	r16
.def	t_pos	=	r17
.def	t2_pos	=	r18

;; --- UART transmit protocol
.equ START_FRAME		= $FF	
.equ END_FRAME			= START_FRAME

