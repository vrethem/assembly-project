-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- PROGRAM		"Quartus II 64-Bit"
-- VERSION		"Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Web Edition"
-- CREATED		"Sun Feb 05 17:59:30 2017"

LIBRARY ieee;
USE ieee.std_logic_1164.all; 

LIBRARY work;

ENTITY block_vga_controller IS 
	PORT
	(
		CLK_RAW :  IN  STD_LOGIC;
		clear :  IN  STD_LOGIC;
		write :  IN  STD_LOGIC;
		adr :  IN  STD_LOGIC_VECTOR(14 DOWNTO 0);
		color :  IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
		h_sync :  OUT  STD_LOGIC;
		v_sync :  OUT  STD_LOGIC;
		blue :  OUT  STD_LOGIC_VECTOR(3 DOWNTO 0);
		green :  OUT  STD_LOGIC_VECTOR(3 DOWNTO 0);
		red :  OUT  STD_LOGIC_VECTOR(3 DOWNTO 0)
	);
END block_vga_controller;

ARCHITECTURE bdf_type OF block_vga_controller IS 

COMPONENT xy_translator
	PORT(x : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		 y : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
		 adr : OUT STD_LOGIC_VECTOR(14 DOWNTO 0)
	);
END COMPONENT;

COMPONENT mux20
	PORT(sel : IN STD_LOGIC;
		 data0x : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
		 data1x : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
		 result : OUT STD_LOGIC_VECTOR(19 DOWNTO 0)
	);
END COMPONENT;

COMPONENT clear_counter
	PORT(clk : IN STD_LOGIC;
		 reset : IN STD_LOGIC;
		 wr : OUT STD_LOGIC;
		 adr : OUT STD_LOGIC_VECTOR(14 DOWNTO 0);
		 color : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
	);
END COMPONENT;

COMPONENT color_palette
	PORT(blanking : IN STD_LOGIC;
		 color : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		 b : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
		 g : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
		 r : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
	);
END COMPONENT;

COMPONENT vga_counter
	PORT(clk : IN STD_LOGIC;
		 reset : IN STD_LOGIC;
		 h_sync : OUT STD_LOGIC;
		 v_sync : OUT STD_LOGIC;
		 blanking : OUT STD_LOGIC;
		 x : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		 y : OUT STD_LOGIC_VECTOR(8 DOWNTO 0)
	);
END COMPONENT;

COMPONENT constant_high
	PORT(		 logic_one : OUT STD_LOGIC;
		 logic_zero : OUT STD_LOGIC
	);
END COMPONENT;

COMPONENT pll_unit
	PORT(inclk0 : IN STD_LOGIC;
		 c0 : OUT STD_LOGIC;
		 c1 : OUT STD_LOGIC
	);
END COMPONENT;

COMPONENT vram160x120x4
	PORT(wren : IN STD_LOGIC;
		 rden : IN STD_LOGIC;
		 wrclock : IN STD_LOGIC;
		 rdclock : IN STD_LOGIC;
		 data : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		 rdaddress : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
		 wraddress : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
		 q : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
	);
END COMPONENT;

SIGNAL	CLK_200 :  STD_LOGIC;
SIGNAL	CLK_25 :  STD_LOGIC;
SIGNAL	constant_high :  STD_LOGIC;
SIGNAL	constant_low :  STD_LOGIC;
SIGNAL	mega_mux_a :  STD_LOGIC_VECTOR(19 DOWNTO 0);
SIGNAL	mega_mux_b :  STD_LOGIC_VECTOR(19 DOWNTO 0);
SIGNAL	mega_result :  STD_LOGIC_VECTOR(19 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_0 :  STD_LOGIC_VECTOR(9 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_1 :  STD_LOGIC_VECTOR(8 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_2 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_3 :  STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_4 :  STD_LOGIC_VECTOR(14 DOWNTO 0);


BEGIN 



b2v_640x480-160x120 : xy_translator
PORT MAP(x => SYNTHESIZED_WIRE_0,
		 y => SYNTHESIZED_WIRE_1,
		 adr => SYNTHESIZED_WIRE_4);


b2v_clear_mux : mux20
PORT MAP(sel => clear,
		 data0x => mega_mux_a,
		 data1x => mega_mux_b,
		 result => mega_result);


b2v_clear_unit : clear_counter
PORT MAP(clk => CLK_200,
		 reset => constant_low,
		 wr => mega_mux_b(19),
		 adr => mega_mux_b(14 DOWNTO 0),
		 color => mega_mux_b(18 DOWNTO 15));


b2v_color_table : color_palette
PORT MAP(blanking => SYNTHESIZED_WIRE_2,
		 color => SYNTHESIZED_WIRE_3,
		 b => blue,
		 g => green,
		 r => red);


b2v_pixel_counter : vga_counter
PORT MAP(clk => CLK_25,
		 reset => constant_low,
		 h_sync => h_sync,
		 v_sync => v_sync,
		 blanking => SYNTHESIZED_WIRE_2,
		 x => SYNTHESIZED_WIRE_0,
		 y => SYNTHESIZED_WIRE_1);


b2v_stupid_constant : constant_high
PORT MAP(		 logic_one => constant_high,
		 logic_zero => constant_low);


b2v_SYSTEM_PLL : pll_unit
PORT MAP(inclk0 => CLK_RAW,
		 c0 => CLK_25,
		 c1 => CLK_200);


b2v_vram : vram160x120x4
PORT MAP(wren => constant_high,
		 rden => constant_high,
		 wrclock => mega_result(19),
		 rdclock => CLK_25,
		 data => mega_result(18 DOWNTO 15),
		 rdaddress => SYNTHESIZED_WIRE_4,
		 wraddress => mega_result(14 DOWNTO 0),
		 q => SYNTHESIZED_WIRE_3);


mega_mux_a(14 DOWNTO 0) <= adr;
mega_mux_a(18 DOWNTO 15) <= color;
mega_mux_a(19) <= write;
END bdf_type;