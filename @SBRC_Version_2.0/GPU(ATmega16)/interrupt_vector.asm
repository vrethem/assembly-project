/////////////////////////  interrupt_vector.asm   ////////////////////////////
;
; Complete interrupt vector listing

.org	0x0000		; Reset
	jmp	RESET
.org	INT0addr	; External Interrupt Request 0
	jmp	INT0_ISR
.org	INT1addr	; External Interrupt Request 1
.org	OC2addr		; Timer/Counter2 Compare Match
.org	OVF2addr	; Timer/Counter2 Overflow
.org	ICP1addr	; Timer/Counter1 Capture Event
.org	OC1Aaddr	; Timer/Counter1 Compare Match A
.org	OC1Baddr	; Timer/Counter1 Compare Match B
.org	OVF1addr	; Timer/Counter1 Overflow
.org	OVF0addr	; Timer/Counter0 Overflow
	jmp	TIMER0_OVERFLOW_ISR
.org	SPIaddr		; Serial Transfer Complete
.org	URXCaddr	; USART, Rx Complete
	jmp	UART_RX_ISR
.org	UDREaddr	; USART Data Register Empty
.org	UTXCaddr	; USART, Tx Complete
.org	ADCCaddr	; ADC Conversion Complete
.org	ERDYaddr	; EEPROM Ready
.org	ACIaddr		; Analog Comparator
.org	TWIaddr		; 2-wire Serial Interface
.org	INT2addr	; External Interrupt Request 2
.org	OC0addr		; Timer/Counter0 Compare Match
.org	SPMRaddr	; Store Program Memory Ready


.org	INT_VECTORS_SIZE
