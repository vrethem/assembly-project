
.def	count	= r17;

	; Force some crap into SRAM to force a draw
	ldi	temp, $01
	sts	DONE, temp
	
	lds	YL, OBJECT_ADR
	lds	YH, OBJECT_ADR+1
	ldi	count, $a0
	sts	OBJECT_COUNT, count

poop_sprites:
	ldi	temp, 7
	and	temp, count
	st	Y+, temp	; Sprite_id

	ldi	temp, 9
	mul	temp, count
	st	Y+, r0
	mul	count, count
	st	Y+, r0
	
	dec	count
	brne	poop_sprites
	
	sts	OBJECT_ADR, YL
	sts	OBJECT_ADR+1, YH
