;; ===========================================================================
;;					GAME - SETTINGS
;;	Layout in sram
;;	Sprite settings
;;  Game speed / difficulty and more 
;; ===========================================================================
.def	p_pos	=	r16
.def	t_pos	=	r17
.def	t2_pos	=	r18

.equ	TRUE	= 1
.equ	FALSE	= 0
;;--------------------------------------------------------
;;				SPRITE SETTINGS
;;
.equ SPRITE_SIZE	= 8		; 8x8 pixels (Size of all sprites)
.equ PLAYER_SIZE_X	= 6		; 6x6 pixels (Collision box)
.equ PLAYER_SIZE_Y	= 4		
.equ ASTROID_SIZE	= SPRITE_SIZE		
.equ ENEMY_SIZE_X	= SPRITE_SIZE	
.equ ENEMY_SIZE_Y	= SPRITE_SIZE
.equ BULLET_SIZE_X	= 1
.equ BULLET_SIZE_Y	= 1
.equ SCORE_SPRITE_SIZE_X = 3 ;3
.equ SCORE_SPRITE_SIZE_Y = 5 ;5


.equ PLAYER_SPRITE_ID  = 10
.equ PLAYER_DEAD_SPRITE = 15
.equ ENEMY_SPRITE_ID   = 11
.equ ASTROID_SPRITE_ID = 12
.equ BULLET_SPRITE_ID  = 13
.equ HP_SPRITE_ID	   = 14

.equ END_FRAME		= $FF	; Send END_FRAME after each frame is sent


;;--------------------------------------------------------
;;					WINDOW SIZE
;;
.equ WINDOW_SIZE_X	= 160 + SPRITE_SIZE			;Number of pixel to paint to screen
.equ WINDOW_SIZE_Y	= 120 + SPRITE_SIZE

.equ FRAME_MIN_X	= SPRITE_SIZE				;Actual game size (Frame size)
.equ FRAME_MAX_X	= 160 + SPRITE_SIZE + 8
.equ FRAME_MIN_Y	= SPRITE_SIZE + 8 
.equ FRAME_MAX_Y	= 120 + SPRITE_SIZE - 8 


;;--------------------------------------------------------
;;					GAME SETTINGS
;;
.equ FPS			= 100	; Maximum fps  ( Req timer to be set at 60 hz )
.equ GAME_SPEED		= 3		; Update frame after (1/100 * GAME_SPEED ) second
.equ PLAYER_SPEED	= 2		; Pixel movments per tic
.equ ENEMY_SPEED	= 1		; Pixel movments per tic
.equ ASTROID_SPEED	= 1		; Pixel movments per tic
.equ BULLET_SPEED	= 3		; Pixel movments per tic
.equ RELOAD_TIME	= 5		; Player shots per second   ( GAME_SPEED/x = RELOAD_TIME <=> GAME_SPEED/RELOAD_TIME = x )
.equ ENEMY_RELOAD	= 2		; If SEED > 120 - ENEMY_RELOAD, insert enemy bullet

.equ ENEMIES		= 30	; Max number of enemies
.equ ASTROIDS		= 30	; Max number of Astroids
.equ BULLETS		= 30	; Max number of bullets
.equ LIFES			= 3		; Number of Health Points
.equ SCORE_NUM		= 5		; Max number of score numbers
.equ OBJECTS		= ( ENEMIES + ASTROIDS + BULLETS + LIFES + SCORE_NUM + 1 ) ;Max nbr of objects in game

;;	---- JOYSTICK constants
.equ JOY_LEFT		= 0
.equ JOY_RIGHT		= 1
.equ JOY_UP			= 2
.equ JOY_DOWN		= 3
.equ JOY_FIRE		= 4

;; -------- Difficulty (SPAWN TIMER)
.equ ASTROID_SPAWN_TIMER = 100		; Spawn new astroid after X * 1/FPS seconds	
.equ ENEMY_SPAWN_TIMER	 = 120		; Spawn new enemy after X * 1/FPS seconds

.equ HARD				 = 40
.equ MEDIUM				 = 15
.equ EASY				 = 5

.equ LEVEL_UP_COUNTER	 = 10
.equ START_LEVEL		 = 40
.equ MAX_LEVEL			 = 75		; Spawn new Enemy / Astroid after (60-45) * 1/60  second


;;--------------------------------------------------------
;;					POSITION OF 
;;			object_id / sprite_id / x / y 
;;					IN ONE OBJECT
.equ OBJECT_BYTES	= 4		; 4 bytes per object
.equ OBJECT_ID		= 0		; Position of object_id in object
.equ SPRITE_ID		= 1		; Position of sprite_id in object
.equ X_ID			= 2		; Position of x_id in object
.equ Y_ID			= 3		; Position of y_id in object

;;	--------- Position of bits in OBJECT_ID --------
.equ VIS_FLAG		= 7		; Visible flag
.equ PSD_FLAG		= 6		; Positive Shot Direction
;.equ INV_FLAG		= 5		; Invert flag
; bit 0-4 f�r ENEMY movment


;;=========================================================
;;========= MEMORY LAYOUT IN SRAM
;;
.dseg
			.org	SRAM_START
TICS:		.byte	1			; Game tics
SEED:		.byte	1			; For Random
JOY_BUFFER: .byte	1			; Input buffer
LEVEL:		.byte	1			; Inc spawn timer for new Astroid / Enemy
LEVEL_TIC:  .byte	1			; if LEVEL_TIC == LEVEL_UP_COUNTER => LEVEL++
HP:			.byte	1			; Health Points

ASTROID_TIC:.byte	1			; When astroid_TIC  > ASTROID_SPAWN_TIMER, spawn new enemy
ENEMY_TIC:  .byte	1			; When enemy_TIC    > ENEMY_SPAWN_TIMER  , spawn new astroid
SHOT_TIC :	.byte	1			; Set time limit on shots
TONE_TIC:	.byte	1			; Beep length
PLAY	:	.byte	1			; Enable music
ptrH:		.byte	1			; Copy ZH
ptrL:		.byte	1			; Copy ZL
COUNTER_ENEMY: .byte 1			; (Sound effect) Enemy sound
COUNTER_SHOOT: .byte 1			; (SOund effect) Shot sound

PLAYER:		.byte	(Object_bytes - 2)
POS_X:		.byte	1							; Player pos
POS_Y:		.byte	1
ENEMY:		.byte	(ENEMIES  * OBJECT_BYTES)	; Nbr of enemies multiply by nbr of B for one object
ASTROID:	.byte	(ASTROIDS * OBJECT_BYTES)
BULLET:		.byte	(BULLETS  * OBJECT_BYTES)
LIFE:		.byte	(LIFES	  * OBJECT_BYTES)
SCORE:		.byte	(SCORE_NUM* OBJECT_BYTES)
.cseg


;;=========================================================
;;========= Sound effects
;;
.equ FREQUENCY_SHOOT_SOUND = 50
.equ SHOOT_SOUND_TIME = 150
.equ FREQUENCY_ENEMY_SOUND =30	
.equ ENEMY_HIT_SOUND_TIME = 20  
.equ PLAYER_DEAD_SOUND_TIME = 3 
.equ FREQUENCY_PLAYER_DEAD_SOUND = 10

