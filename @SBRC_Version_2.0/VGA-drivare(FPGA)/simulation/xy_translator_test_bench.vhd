library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity xy_translator_test_bench is
end xy_translator_test_bench;

architecture xy_translator_test_bench_beh of xy_translator_test_bench is
  component xy_translator
	port( x : in std_logic_vector(9 downto 0);
			y : in std_logic_vector(8 downto 0);
			adr : out std_logic_vector(14 downto 0));
	end component;
	
	signal x : std_logic_vector(9 downto 0);
	signal y : std_logic_vector(8 downto 0);
	signal adr : std_logic_vector(14 downto 0);
	
	constant clk_period : time := 1 ns;
begin
  
	uut: xy_translator port map(
		x => x,
		y => y,
		adr => adr
	);
 
	clk_process :process
	begin
		if x < 159 then
			x <= x+1;
		else
			x <= (others => '0');
			if y < 119 then
				y <= y+1;
			else
				y <= (others => '0');
			end if;
		end if;
		wait for clk_period/2;
	end process;

end xy_translator_test_bench_beh;