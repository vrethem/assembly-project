///////////////////////// draw_frame.asm //////////////////////////////////
;
; Uses register:
; Y pointer for reading object stack
; Z pointer for reading sprite data from Flash

.def	mult_low	= r0	; Multiplication result registers
.def	mult_high	= r1

;	temp		= r16	(Global)
.def	temp2		= r17
.def	object_counter	= r18
.def	x_counter	= r19
.def	y_counter	= r20
.def	x_pos		= r21
.def	y_pos		= r22
.def	spr_id		= r23
.def	word_low	= r24
.def	word_high	= r25


/////////////////////////// DRAW FRAME //////////////////////////////
DRAW_FRAME:
; Clear the screen
CLEAR_SCREEN:
	sbi	PORTC, CLEAR_PIN
	cbi	PORTC, CLEAR_PIN

; Wait for the clear to finnish
WAIT_FOR_RDY:
	sbis	PIND,READY_PIN
	rjmp	WAIT_FOR_RDY


	lds	object_counter, OBJECT_COUNT
	lds	YL, OBJECT_ADR
	lds	YH, OBJECT_ADR+1
	
						#ifdef	DEBUG	; Debug trap
							lds	temp, OBJECT_COUNT
							cpi	temp, 0
							brne	NO_SHITFUCK3
						SHITFUCK3:
							rjmp	SHITFUCK3
						NO_SHITFUCK3:
						#endif

; For each object in the object stack
DRAW_OBJECTS_LOOP:
	ld	y_pos, -Y
	ld	x_pos, -Y
	ld	spr_id, -Y
	
						#ifdef	DEBUG	; Debug trap
							lds	temp, OBJECT_ADR+1
							cpi	temp, 0
							brne	NO_SHITFUCK2
							lds	temp, OBJECT_ADR
							cpi	temp, $65
							brsh	NO_SHITFUCK2
						SHITFUCK2:
							rjmp	SHITFUCK2
						NO_SHITFUCK2:
						#endif


	cpi	spr_id, BULLET_ID
	brne	NOT_A_BULLET
	call	DRAW_BULLET
	rjmp	SPR_DONE
NOT_A_BULLET:

	call	DRAW_SPRITE

	

SPR_DONE:
	dec	object_counter
	brne	DRAW_OBJECTS_LOOP
;____ DRAW_OBJECTS_LOOP END ____


						#ifdef	DEBUG
							lds	temp, OBJECT_ADR
							cpi	YL, $65
							brne	ADR_NOT_RESET
							rjmp	ADR_WAS_RESET
						ADR_NOT_RESET:
							rjmp	ADR_NOT_RESET
						ADR_WAS_RESET:
						#endif
	sts	OBJECT_ADR, YL
	sts	OBJECT_ADR+1, YH
	clr	temp
	sts	OBJECT_COUNT, temp
	sts	DONE, temp
	
						#ifdef	DEBUG	; Debug trap
							lds	temp, OBJECT_ADR+1
							cpi	temp, 0
							brne	NO_SHITFUCK
							lds	temp, OBJECT_ADR
							cpi	temp, $65
							brsh	NO_SHITFUCK
							SHITFUCK:
								rjmp	SHITFUCK
						NO_SHITFUCK:
						#endif

	;call	SOFT_INIT
	ret
;__________________________ DRAW FRAME END __________________________

/////////////////////////// DRAW SPRITE /////////////////////////////
DRAW_SPRITE:
	; Init Z with sprite lookup address for sprite data in Flash
	ldi	ZH, HIGH(FIRST_SPRITE*2)
	ldi	ZL, LOW(FIRST_SPRITE*2)

	; Add offset for current sprite to Z pointer (two colors per byte)
	ldi	temp, (SPR_SIZE*SPR_SIZE/2)
	mul	temp, spr_id
	add	ZL, mult_low
	adc	ZH, mult_high

	; Set pixel address adr = SCREEN_WIDTH * (y_pos - SPRITE_SIZE) + (x_pos - SPRITE_SIZE)
	ldi	temp, -SPR_SIZE
	add	temp, y_pos
	brcc	Y_OFF_SCREEN
Y_ON_SCREEN:
	mul	temp, screen_width	;Multiplies the y_pos-8 with the screen width
	rjmp	Y_PIXEL_ADDRESS_SET
Y_OFF_SCREEN:
	neg	temp
	mul	temp, screen_width	;Multiplies the -(y_pos-8) with the screen width
	com	mult_high
	com	mult_low
	ldi	temp, 1
	add	mult_low, temp
	adc	mult_high, zero
Y_PIXEL_ADDRESS_SET:


	ldi	temp, -SPR_SIZE
	add	temp, x_pos
	brcc	X_OFF_SCREEN
X_ON_SCREEN:
	add	mult_low, temp		;Adds the x_pos to the result of last operation
	adc	mult_high, zero		;With carry
	rjmp	PIXEL_ADDRESS_SET
X_OFF_SCREEN:
	neg	temp
	sub	mult_low, temp
	sbc	mult_high, zero
PIXEL_ADDRESS_SET:

	; For each row in the sprite
	ldi	y_counter, SPR_SIZE
SPRITE_Y_LOOP:
		// Y boundry check
		cpi	y_pos, spr_size			; Check y_pos lower boundry
		brlo	SKIP_THIS_LINE			; Skip rest of line

		cpi	y_pos, scr_height+spr_size	; Check y_pos upper boundy
		brsh	SKIP_THIS_SPRITE		; Skip rest of sprite

		; For each pixel in the row
		ldi	x_counter, SPR_SIZE
	SPRITE_X_LOOP:
			// X boudary check
			cpi	x_pos, SPR_SIZE			; Check x_pos lower boundry
			brlo	SKIP_PIXEL			; Skip this pixel

			cpi	x_pos, SCR_WIDTH + SPR_SIZE	; Check x_pos higher boundry
			brsh	SKIP_PIXEL			; Skip this pixel

			; Write address to FPGA
			mov	temp, mult_high
			andi	temp, $7F		; 0aaa aaaa
			out	PORTB, temp		; Set PORTB 6..0 to video buffer addr, force 7(wr) to 0
			out	PORTA, mult_low

		DRAW_PIXEL:
			; Get correct color data
			sbrc	x_counter, 0
			rjmp	odd_x_counter
		EVEN_X_COUNTER:
			; The high nibble if even x counter, don't increese Z
			lpm	temp, Z
			rjmp	color_selected
		ODD_X_COUNTER:
			; The low nibble if odd x counter, increese Z
			lpm	temp, Z+
			swap	temp
		COLOR_SELECTED:
			andi	temp, $F0
			cpi	temp, KEY_COLOR<<4	; Skip pixel if transparent
			breq	SPRITE_X_LOOP_END


			; Write color to FPGA
			in	temp2, PORTD
			andi	temp2, $0F
			or	temp, temp2
			out	PORTD, temp

			; Write pixel
			sbi	PORTB, WRITE_PIN
			cbi	PORTB, WRITE_PIN

			rjmp	SPRITE_X_LOOP_END

		SKIP_PIXEL:
			; Decide if Z is to be increesed (on each odd x_counter)
			sbrc	x_counter, 0
			lpm	temp, Z+

			rjmp	SPRITE_X_LOOP_END

		SPRITE_X_LOOP_END:
			; Increese video memory address to next pixel
			ldi	temp, 1
			add	mult_low, temp
			adc	mult_high, zero

			inc	x_pos
			dec	x_counter
			brne	SPRITE_X_LOOP
			rjmp	SPRITE_Y_LOOP_END
	;
	;_____________ END SPRITE_X_LOOP

	SKIP_THIS_LINE:
		adiw	ZL, spr_size/2

		ldi	temp, 8
		add	mult_low, temp
		adc	mult_high, zero

	SPRITE_Y_LOOP_END:
		inc	y_pos		;Increase the y_pos
		ldd	x_pos, Y+1	;reset x_pos

		; Update video memory address to next row
		ldi	temp, 152
		add	mult_low, temp
		adc	mult_high, zero

		dec	y_counter
		brne	SPRITE_Y_LOOP
;
;_____________ END SPRITE_Y_LOOP
SKIP_THIS_SPRITE:
	ret
;__________________________ DRAW SPRITE END _________________________


/////////////////////////// DRAW BULLET /////////////////////////////
DRAW_BULLET:

	// Y boundry check
	cpi	y_pos, spr_size			; Check y_pos lower boundry
	brlo	SKIP_BULLET			; Skip bullet

	cpi	y_pos, scr_height+spr_size	; Check y_pos upper boundy
	brsh	SKIP_BULLET			; Skip bullet

	// X boudary check
	cpi	x_pos, spr_size			; Check x_pos lower boundry
	brlo	SKIP_BULLET			; Skip bullet

	cpi	x_pos, scr_width+spr_size	; Check x_pos higher boundry
	brsh	SKIP_BULLET			; Skip bullet
	
	; Set pixel address, SPR_SIZE pixels "invisible area" on top and to the left of the screen
	ldi	temp, -SPR_SIZE
	add	temp, y_pos
	mul	temp, screen_width	;Multiplies the y_pos-8 with the screen width
	ldi	temp, -SPR_SIZE
	add	temp, x_pos
	add	mult_low, temp		;Adds the x_pos to the result of last operation
	adc	mult_high, zero		;With carry
	
	out	PORTA, mult_low
	mov	temp, mult_high
	andi	temp, $7F
	out	PORTB, mult_high

	in	temp, PORTD
	andi	temp, $0F
	ori	temp, BULLET_COLOR << 4
	out	PORTD, temp
	
	call	SHORT_DELAY

	; Write pixel
	sbi	PORTB, WRITE_PIN
	cbi	PORTB, WRITE_PIN

SKIP_BULLET:
	ret

;__________________________ DRAW BULLET END _________________________

; Supershort delay
SHORT_DELAY:
	;nop
	;nop
	ret
