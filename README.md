# @MEGA SBRC  
@MEGA Super Boost Random Comando is a game prototype  
constructed with two ATmega16 chipset and one FPGA

## The game 
![game](Bilder/game.png)  

## Design 
![system](Bilder/system.png)  

## Hardware
The system contains a CPU(ATmega16), GPU(ATMega16)  
and  VGA-driver(FPGA)  
![ovansida](Bilder/ovansida.JPG)  

![undersida](Bilder/undersida.JPG)  

## Gameplay 
<iframe width="640" height="360" src="https://www.youtube.com/embed/VLB_5_NhCv0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
