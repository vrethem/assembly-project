;
; GPU.asm
;
; Created: 2017-02-09 10:37:52
; Author : Oppo_Oskar
;
; Revised 2017-02-24 by Jonatan

.include "defines.inc"
.include "ram_allocation.inc"

.include "interrupt_vector.asm"
.include "uart_rx_isr.asm"
.include "int0_isr.asm"
.include "timer0_overflow_isr.asm"
.include "init.asm"
.include "draw_frame.asm"
.include "sprites.inc"

MAIN_LOOP:
	lds	temp, DONE
	cpi	temp, 0
	breq	MAIN_LOOP
	
	;cli DRAW_FRAME should be "thread safe"
						#ifdef	DEBUG
							lds	temp, OBJECT_COUNT
							cpi	temp, $00
							brne	NOT_FUCKED_UP
						FUCKED_UP:
							rjmp	FUCKED_UP
						NOT_FUCKED_UP:
						#endif
	call	DRAW_FRAME
	;sei

	rjmp	MAIN_LOOP

