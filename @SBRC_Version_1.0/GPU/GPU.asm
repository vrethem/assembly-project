;
; GPU.asm
;
; Created: 2017-02-09 10:37:52
; Author : Oppo_Oskar
;


.equ	OBJECTS = 41
.equ	OBJECT_BYTES = 3
.equ	spr_size = 8
.equ	scr_width =160
.equ	scr_height = 120

.equ	KEY_COLOR = $f

.equ	CLEAR_PIN = 7
.equ	READY_PIN = 3
.equ	RUNNING_PIN = 2

.def	mult_low = r0
.def	mult_high = r1
.def	zero = r2
.def	screen_width = r6
.def	screen_height = r7 

.def	temp = r16
.def	temp2 = r17
.def	UART = r18
.def	LOW_ADR = r19
.def	HIGH_ADR = r20
.def	count = r21
.def	count2 = r24
.def	count3 = r25	

.def	YPOS = r18
.def	XPOS = r22
.def	spr_id = r23

.dseg
.org	SRAM_START
	STATE:		.byte 1
	DONE:		.byte 1

	ANIM_COUNT:	.byte 1

	OBJECT_COUNT:	.byte 1

	OBJECT_ADR:	.byte 2
	OBJECT_LIST:	.byte (OBJECTS * OBJECT_BYTES)
.cseg

.org	0x0000		; Reset
	rjmp	init

.org	URXCaddr	; Uart receive interrupt
	rjmp	UART_receive

; Replace with your application code
init:
	;Set stack pointer
	ldi	temp, HIGH(RAMEND)
	out	SPH, temp
	ldi	temp, LOW(RAMEND)
	out	SPL, temp

	;Set screen width and height
	ldi	temp, scr_width
	mov	screen_width, temp
	ldi	temp, scr_height
	mov	screen_height, temp

	ldi	temp, $FF
	out	DDRA, temp
	out	DDRB, temp
	ldi	temp, $80    ;1000 0000
	out	DDRC, temp
	ldi	temp, $F0    ;1111 0000
	out	DDRD, temp

	;Set all SRAM values to default value
	ldi	temp,$00
	sts	STATE, temp
	sts	DONE, temp
	sts	OBJECT_COUNT, temp
	sts	ANIM_COUNT, temp
	
	;set the object adress
	ldi	temp, LOW(OBJECT_LIST)		;Getting LOW adress og OBJECT_LIST into temp
	sts	OBJECT_ADR, temp		;Saving that adress in OBJECT_ADR+1
	ldi	temp, HIGH(OBJECT_LIST)		;Getting the HIGH adress of OBJECT_LIST into temp
	sts	OBJECT_ADR+1, temp		;Saving that adress in OBJECT_ADR

	;SETUP UART
	;Set braud-rate
	ldi	temp, $00    ; 0000 0000
	out	UBRRH, temp
	ldi	temp, $22   ; 0010 0010
	out	UBRRL, temp

	;Set UART interupts
	ldi	temp, (1<<RXCIE) | (1 << RXEN)
	out	UCSRB, temp

	;Set frame format 8-bit data
	ldi	temp, (1 << URSEL) | (1 << UCSZ0) | (1 << UCSZ1)
	out	UCSRC, temp
	

/*
	; Force some crap into SRAM to force a draw
	ldi	temp, $01
	sts	DONE, temp
	
	lds	YH, OBJECT_ADR
	lds	YL, OBJECT_ADR+1
	ldi	count, 255
	sts	OBJECT_COUNT, count

poop_sprites:
	ldi	temp, 7
	and	temp, count
	st	Y+, temp	; Sprite_id

	ldi	temp, 9
	mul	temp, count
	st	Y+, r0
	mul	count, count
	st	Y+, r0
	
	dec	count
	brne	poop_sprites

	sts	OBJECT_ADR+1, YL
	sts	OBJECT_ADR, YH
	*/
	/*
	ldi	temp, $05
	sts	OBJECT_COUNT, temp

	ldi	temp, $74
	sts	OBJECT_ADR+1, temp	; Object adr

	ldi	temp, $04
	sts	OBJECT_ADR+2, temp	; Sprite ID
	ldi	temp, 160
	sts	OBJECT_ADR+3, temp	; Sprite X
	ldi	temp, 8
	sts	OBJECT_ADR+4, temp	; Sprite Y

	ldi	temp, $04
	sts	OBJECT_ADR+5, temp	; Sprite ID
	ldi	temp, 161
	sts	OBJECT_ADR+6, temp	; Sprite X
	ldi	temp, 17
	sts	OBJECT_ADR+7, temp	; Sprite Y

	ldi	temp, $04
	sts	OBJECT_ADR+8, temp	; Sprite ID
	ldi	temp, 163
	sts	OBJECT_ADR+9, temp	; Sprite X
	ldi	temp, 26
	sts	OBJECT_ADR+10, temp	; Sprite Y

	ldi	temp, $04
	sts	OBJECT_ADR+11, temp	; Sprite ID
	ldi	temp, 164
	sts	OBJECT_ADR+12, temp	; Sprite X
	ldi	temp, 35
	sts	OBJECT_ADR+13, temp	; Sprite Y

	ldi	temp, $04
	sts	OBJECT_ADR+14, temp	; Sprite ID
	ldi	temp, 165
	sts	OBJECT_ADR+15, temp	; Sprite X
	ldi	temp, 44
	sts	OBJECT_ADR+16, temp	; Sprite Y
	*/

	; Setting global interupt flag.
	sei

    rjmp main_loop


/////////////////////////  UART RECEIVE ISR   ////////////////////////////
UART_receive:
	push	temp
	in	temp, SREG
	push	temp
	push	UART

	lds	temp, DONE
	cpi	temp, $00
	;brne	state_error
	
	in	UART, UDR

	lds	temp, STATE
	cpi	temp, $00	;0000 0000
	breq	state0
	cpi	temp, $01	;0000 0001
	breq	state1
	cpi	temp, $02	;0000 0010
	breq	state2
	rjmp	state_error



state0:			;STATE FOR GETTING SPRITES
	cpi	UART, $FF
	breq	UART_done		;Checks if sprite_ID ==0xFF
	rcall	store_uart
	ldi	temp, 1
	sts	STATE, temp
	rjmp	return_interupt
		
state1:			;STATE FOR GETTING X-POS
	rcall	store_uart
	ldi	temp, 2
	sts	STATE, temp
	rjmp	return_interupt
state2:			;STATE FOR GETTING Y-POS
	rcall	store_uart
	lds	temp, OBJECT_COUNT
	inc	temp
	sts	OBJECT_COUNT, temp
	ldi	temp, 0
	sts	STATE, temp
	rjmp	return_interupt

state_error:
	rjmp	state_error

UART_done:		;Goes here if sprite_ID==0xFF
	ldi	temp, $01
	sts	DONE, temp
	rjmp	return_interupt

return_interupt:
	pop	UART
	pop	temp
	out	SREG, temp
	pop	temp
	reti

store_uart:		// Store register UART -> SRAM
	push	YH
	push	YL
	push	temp

	lds	YH, OBJECT_ADR+1	;Sets the high Y-pointer to the HIGH(adress)
	lds	YL, OBJECT_ADR		;Sets the low Y-pointer to the LOW(adress)

	st	Y+, UART		;Stores the sprite_ID where the adress is pointing and then adds the Y-pointer with 1.

	sts	OBJECT_ADR+1, YH	;Saves the new high part of adress into SRAM
	sts	OBJECT_ADR, YL		;Saves the new low part of adress into SRAM
	
	pop	temp
	pop	YL
	pop	YH

	ret
//________________________UART RECEIVE ISR END_________________________

proccess:
	; Clear the screen
	sbi	PORTC,CLEAR_PIN
	cbi	PORTC,CLEAR_PIN
	; Wait for the ready signal
no_ready_signal:
	sbis	PIND,READY_PIN
	rjmp	no_ready_signal
	; Proccess the data:
	lds	count, OBJECT_COUNT	;Loads the length of the list into "count" 
	lds	YL, OBJECT_ADR		;Loads the low Y-pointer to the LOW(adress)
	lds	YH, OBJECT_ADR+1	;Loads the high Y-pointer to the HIGH(adress)

proccess_again:
	ld	YPOS, -Y
	ld	XPOS, -Y
	ld	spr_id, -Y

	// Init Z with sprite lookup address
	ldi	ZH, HIGH(FIRST_SPRITE*2)
	ldi	ZL, LOW(FIRST_SPRITE*2)
	ldi	temp, (spr_size*spr_size/2)	; Multiply spride_id with half num of pixel in one sprite (2pixel/byte)
	mul	temp, spr_id
	add	ZL, r0
	adc	ZH, r1

	// Loop for sprite:
	ldi	count2, spr_size
draw_sprite:				;START HEIGHT LOOP
	// Height loop counter
	cpi	count2, $00
	breq	spr_done

	//Do height stuff here
	// Y boudary check
	cpi	YPOS, spr_size			; Check YPOS lower boundry
	brlo	skip_this_line			; Skip rest of line

	cpi	YPOS, scr_height+spr_size	;Check YPOS upper boundy
	brsh	spr_done			; Skip rest of sprite

	ldi	count3, spr_size
draw_spr_length:			; START LENGTH LOOP
	// Width loop counter
	cpi	count3, $00
	breq	spr_height_done

	//Do length stuff here
	// X boudary check
	cpi	XPOS, spr_size			; Check XPOS lower boundry
	brlo	skip_pixel			; Skip this pixel

	cpi	XPOS, scr_width+spr_size	; Check XPOS higher boundry
	brsh	skip_pixel			; Skip this pixel

	rjmp	draw_pixel

skip_pixel:
	; Decide if Z is to be increesed (on each odd counter3)
	sbrc	count3, 0
	lpm	temp, Z+
	rjmp	spr_length_done

draw_pixel:
	 ; Get correct color data
	 sbrc	count3, 0
	 rjmp	odd_x_counter
even_x_counter:
	 ; The high nibble if even x counter, don't increese Z
	 lpm	temp, Z
	 andi	temp, $F0
	 rjmp	color_selected
odd_x_counter:
	 ; The low nibble if odd x counter, increese Z
	 lpm	temp, Z+
	 swap	temp
	 andi	temp, $F0
color_selected:
	 cpi	temp, KEY_COLOR<<4	; Skip if transparent
	 breq	spr_length_done

	 in	temp2, PORTD
	 andi	temp2, $0F
	 or	temp, temp2
	 out	PORTD, temp

	 /*
	 lpm	temp, Z		; Load pixeldata from flash
	 cpi	temp, $F
	 sbrs	count3, 0	; Skip line if odd
	 andi	temp, $0F	; 0000 1111
	 cpi	temp, $0F
	 breq	spr_length_done

	 sbrc	count3, 0	  ; Skip line if even
	 andi	temp, $F0 ; 1111 0000
	 cpi	temp, $F0
	 breq	spr_length_done
	 out	PORTD, temp
	 */

	 ; Set pixel address
	 ldi	temp, -8
	 add	temp, YPOS
	 mul	temp, screen_width	;Multiplies the YPOS-8 with the screen width
	 ldi	temp, -8
	 add	temp, XPOS
	 add	mult_low, temp		;Adds the XPOS to the result of last operation
	 clr	temp
	 adc	mult_high, temp		;With carry

	 ; Write address to FPGA
	 mov	temp, mult_high
	 andi	temp, $7F ; 0111 1111
	 out	PORTB, temp   // Set PORTB 6..0 to video buffer addr, force 7 to 0
	 out	PORTA, mult_low

	 call	sDelay

	 ; Write pixel
	 sbi	PORTB, 7
	 cbi	PORTB, 7

spr_length_done:
	inc	XPOS			;Icreases XPOS
	;ldi	temp, (spr_size*spr_size)
	/*
	ldi	temp, 1			;Increase the adress
	add	mult_low, temp		;--"--
	ldi	temp, $00		;--"--
	adc	mult_high, temp		;--"--
	*/
	dec	count3
	rjmp	draw_spr_length		; END LENGTH LOOP

skip_this_line:
	adiw	ZL, spr_size/2
	rjmp	spr_height_done
	/*
	ldi	temp, spr_size			;Increase the adress with one height
	add	mult_low, temp		;--"--
	ldi	temp, $00		;--"--
	adc	mult_high, temp		;--"--
	rjmp	spr_height_done
	*/

spr_height_done:
	inc	YPOS		;Increase the YPOS
	ldd	XPOS, Y+1	;reset XPOS

	dec	count2
	rjmp	draw_sprite		;END HEIGHT LOOP

spr_done:
	dec	count
	cpi	count, $00
	breq	proccess_ready		;Jump to ready if no objects in list
	rjmp	proccess_again

proccess_ready:
	ldi	temp, $00
	sts	DONE, temp
	sts	OBJECT_COUNT, temp
	rjmp	main_loop

main_loop:

	lds	temp, DONE
	ldi	temp2, $00
	cpse	temp, temp2
	rjmp	proccess

	rjmp	main_loop

sDelay:
	nop
	nop

	ret


FIRST_SPRITE:
; Sprite 0 Player
.db	$f1, $ee, $e3, $3f
.db	$41, $77, $e3, $3e
.db	$41, $00, $0e, $ee
.db	$f1, $ee, $ee, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff
.db	$ff, $ff, $ff, $ff
; Sprite 1 Enemy
.db	$1d, $dd, $cc, $a4
.db	$ff, $dd, $cc, $a1
.db	$ff, $fe, $cc, $a4
.db	$ff, $f6, $cc, $a1
.db	$ff, $f6, $cc, $a4
.db	$ff, $fe, $cc, $a1
.db	$ff, $dd, $cc, $a4
.db	$1d, $dd, $cc, $a1
; Sprite 2 Astroid
.db	$ff, $88, $88, $ff
.db	$f9, $88, $88, $cf
.db	$18, $99, $99, $14
.db	$18, $aa, $aa, $14
.db	$18, $aa, $aa, $14
.db	$aa, $aa, $aa, $99
.db	$fc, $ca, $a9, $af
.db	$ff, $cc, $99, $ff
; Sprite 3 Bullet
.db	$7F, $FF, $FF, $FF
.db	$FF, $FF, $FF, $FF
.db	$FF, $FF, $FF, $FF
.db	$FF, $FF, $FF, $FF
.db	$FF, $FF, $FF, $FF
.db	$FF, $FF, $FF, $FF
.db	$FF, $FF, $FF, $FF
.db	$FF, $FF, $FF, $FF
; Sprite 4
.db	$e1, $23, $45, $67
.db	$89, $ab, $cd, $ef
.db	$00, $11, $22, $33
.db	$00, $11, $22, $33
.db	$44, $55, $66, $77
.db	$44, $55, $66, $77
.db	$88, $99, $aa, $bb
.db	$88, $99, $aa, $bb
; Sprite 5 Enemy
.db	$1d, $dd, $cc, $a4
.db	$ff, $dd, $cc, $a1
.db	$ff, $fe, $cc, $a4
.db	$ff, $f6, $cc, $a1
.db	$ff, $f6, $cc, $a4
.db	$ff, $fe, $cc, $a1
.db	$ff, $dd, $cc, $a4
.db	$1d, $dd, $cc, $a1
; Sprite 6
.db	$3F, $aa, $bb, $bb
.db	$FF, $aa, $FF, $cc
.db	$64, $FF, $FF, $cc
.db	$aa, $86, $66, $cc
.db	$dd, $aa, $22, $cc
.db	$FF, $bb, $FF, $cc
.db	$cc, $cF, $FF, $cc
.db	$FF, $FF, $FF, $cc
; Sprite 7
.db	$3F, $FF, $55, $33
.db	$FF, $FF, $44, $27
.db	$FF, $ee, $ee, $77
.db	$FF, $ee, $ee, $aa
.db	$FF, $ee, $ee, $aa
.db	$dd, $ee, $ee, $ee
.db	$dd, $dd, $dd, $dd
.db	$dd, $dd, $dd, $dd
; Sprite 9 Flagga
.db	$aa, $aa, $aa, $aa
.db	$aa, $bb, $bb, $aa
.db	$aa, $bb, $bb, $aa
.db	$aa, $bb, $bb, $aa
.db	$aa, $bb, $bb, $aa
.db	$aa, $bb, $bb, $aa
.db	$aa, $bb, $bb, $aa
.db	$aa, $aa, $aa, $aa
; Sprite 8 Gubben
.db	$f1, $fe, $ee, $ff
.db	$f1, $f5, $5f, $ff
.db	$ff, $18, $81, $1f
.db	$ff, $f8, $8f, $f1
.db	$ff, $f8, $8f, $ff
.db	$ff, $ef, $fe, $ff
.db	$ff, $ef, $fe, $ff
.db	$f9, $9f, $f9, $9f