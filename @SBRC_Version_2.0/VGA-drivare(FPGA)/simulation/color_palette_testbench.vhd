library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity color_palette_testbench is
end color_palette_testbench;

architecture color_palette_testbench_beh of color_palette_testbench is
	component color_palette
		port( clk		: in std_logic;
				color 	: in std_logic_vector(3 downto 0);
				blanking : in std_logic;
				r			: out std_logic_vector(3 downto 0);
				g			: out std_logic_vector(3 downto 0);
				b			: out std_logic_vector(3 downto 0));
	end component;
	
	signal clk			: std_logic := '0';
	signal color 		: std_logic_vector(3 downto 0) := "0000";
	signal blanking 	: std_logic := '0';
	signal r				: std_logic_vector(3 downto 0);
	signal g				: std_logic_vector(3 downto 0);
	signal b				: std_logic_vector(3 downto 0);
	
	constant clk_period : time := 100 ps;
begin
  
	uut: color_palette port map(
		clk => clk,
		color => color,
		blanking => blanking,
		r => r,
		g => g,
		b => b
	);
 
	clk_process :process
	begin
		wait for clk_period/2;
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
	end process;
 
	adr_process :process
	begin
		wait for clk_period;
		
		if color < 15 then
			color <= color+1;
		else
			color <= (others => '0');
		end if;
		
	end process;

end color_palette_testbench_beh;