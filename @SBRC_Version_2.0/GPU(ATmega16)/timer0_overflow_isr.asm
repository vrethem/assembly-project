/////////////////////////  timer0_overflow_isr.asm   ////////////////////////////
;
; ISR to update animation counter
; Uses registers
;	temp		= r16

/////////////////////////  TIMER0_OVERFLOW_ISR   ////////////////////////////
TIMER0_OVERFLOW_ISR:
	push	temp
	in	temp, SREG
	push	temp

	lds	temp, ANIM_PRESCALER
	dec	temp
	sts	ANIM_PRESCALER, temp
	brne	RETURN_TIMER0_OVERFLOW_ISR

	ldi	temp, ANIM_PRESCALER_VALUE
	sts	ANIM_PRESCALER, temp

	lds	temp, ANIM_COUNT
	inc	temp
	sts	ANIM_COUNT, temp




RETURN_TIMER0_OVERFLOW_ISR:
	pop	temp
	out	SREG, temp
	pop	temp
	reti
;_________________________ TIMER0_OVERFLOW_ISR END _________________________