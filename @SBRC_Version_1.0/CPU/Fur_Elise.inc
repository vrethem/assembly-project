;;######################################################################################
;;############################### FUR ELISE	############################################

.org $2A
FUR_ELISE:
//Measure 1

  .db           E6, EIGHTHNOTE ;
  .db           DS6, EIGHTHNOTE ;
  
  //Measure 2
  .db           E6, EIGHTHNOTE ;
  .db           DS6, EIGHTHNOTE ;
  .db           E6, EIGHTHNOTE ;
  .db	        B5, EIGHTHNOTE ;
  .db       	D6, EIGHTHNOTE ;
  .db		C6, EIGHTHNOTE ;

  //Measure 3
  .db		A3, EIGHTHNOTE ;
  .db		E4, EIGHTHNOTE ;
  .db		A4, EIGHTHNOTE ;
  .db		C5, EIGHTHNOTE ;
  .db		E5, EIGHTHNOTE ;
  .db		A5, EIGHTHNOTE ;
  
  //Measure 4
  .db		E3, EIGHTHNOTE ;
  .db		E4, EIGHTHNOTE ;
  .db		GS4, EIGHTHNOTE ;
  .db		E5, EIGHTHNOTE ;
  .db		GS5, EIGHTHNOTE ;
  .db		B5, EIGHTHNOTE ;
  
  //Measure 5
  .db		A3, EIGHTHNOTE ;
  .db		E4, EIGHTHNOTE ;
  .db		A4, EIGHTHNOTE ;
  .db		E5, EIGHTHNOTE ;
  .db		E6, EIGHTHNOTE ;
  .db		DS6, EIGHTHNOTE ;
  
  //Measure 6
  .db		E6, EIGHTHNOTE ;
  .db		DS6, EIGHTHNOTE ;
  .db		E6, EIGHTHNOTE ;
  .db		B5, EIGHTHNOTE ;
  .db		D6, EIGHTHNOTE ;
  .db		C6, EIGHTHNOTE ;
  
  //Measure 7
  .db		A3, EIGHTHNOTE ;
  .db		E4, EIGHTHNOTE ;
  .db		A4, EIGHTHNOTE ;
  .db		C5, EIGHTHNOTE ;
  .db		E5, EIGHTHNOTE ;
  .db		A5, EIGHTHNOTE ;
  
  //Measure 8
  .db		E3, EIGHTHNOTE ;
  .db		E4, EIGHTHNOTE ;
  .db		GS4, EIGHTHNOTE ;
  .db		E5, EIGHTHNOTE ;
  .db		GS5, EIGHTHNOTE ;
  .db		B5, EIGHTHNOTE ;
  
  //Measure 9
  .db		A3, EIGHTHNOTE ;
  .db		E4, EIGHTHNOTE ;
  .db		A4, EIGHTHNOTE ;
  .db		E5, EIGHTHNOTE ;
  .db		C6, EIGHTHNOTE ;
  .db		D6, EIGHTHNOTE ;
  
  //Measure 10
  .db		C4, EIGHTHNOTE ;
  .db		G4, EIGHTHNOTE ;
  .db		C5, EIGHTHNOTE ;
  .db		G5, EIGHTHNOTE ;
  .db		F6, EIGHTHNOTE ;
  .db		E6, EIGHTHNOTE ;
  
  //Measure 11
  .db		G3, EIGHTHNOTE ;
  .db		G4, EIGHTHNOTE ;
  .db		B4, EIGHTHNOTE ;
  .db		F5, EIGHTHNOTE ;
  .db		E6, EIGHTHNOTE ;
  .db		D6, EIGHTHNOTE ;
  
  //Measure 12
  .db		A3, EIGHTHNOTE ;
  .db		E4, EIGHTHNOTE ;
  .db		A4, EIGHTHNOTE ;
  .db		E5, EIGHTHNOTE ;
  .db		D6, EIGHTHNOTE ;
  .db		C6, EIGHTHNOTE ;
  
  //Measure 13
  .db		E3, EIGHTHNOTE ;
  .db		E4, EIGHTHNOTE ;
  .db		A5, EIGHTHNOTE ;

;;######################################################################################
;;###############################	END		############################################
PROGRAM_START: