;;######################################################
;;############	SOUND EFFECTS  #########################

;;---------------------------------->>
;;----- PREPARE_SHOOT_SOUND
PREPARE_SHOOT_SOUND:
		ldi		r17, FREQUENCY_SHOOT_SOUND	
		call	SET_SOUND_EFFECT
ret
;;----------------------------------
;;----- PREPARE_ENEMY_BULLET_SOUND
PREPARE_ENEMY_BULLET_SOUND:
		ldi		r17, FREQUENCY_ENEMY_SOUND							
		call	SET_SOUND_EFFECT
ret
PREPARE_ASTROID_HIT_SOUND:
		ldi		r17, FREQUENCY_ASTROID_SOUND
		call	SET_SOUND_EFFECT
ret
;;----------------------------------
;;----- PREPARE_RANDOM_SOUND_1
PREPARE_RANDOM_SOUND_1:
		lds		r17, SEED						
		call	SET_SOUND_EFFECT
ret
;;----------------------------------
;;----- PREPARE_RANDOM_SOUND_2
PREPARE_RANDOM_SOUND_2:
		lds		r17, SEED
		lsl		r17
		call	SET_SOUND_EFFECT
ret

SET_SOUND_EFFECT:
		clr		r16
		sts		TONE_TIC, r16					; Reset counter 
		out		OCR0,r17						; Set match register (frequency)
		ldi		r16, (1<<OCIE0)|(1<<OCIE1A)		; Enable sound effect interupt			
		out		TIMSK,r16
ret
;;-----------------------------------<<


;;------------------------------------
;;			RANDOM_DEATH_SONG
;; Hangs program - plays lovely tune
;; Exit loop upon JOY_FIRE 
;;---  
RANDOM_DEATH_SONG:
	clr		r17
WAIT_A_LITTLE:						; Small delay before reset can be made
	clr		r18
WAIT_A_LITTLE_MORE:
	ldi		r19, 5
WAIT_A_LOT:
	dec		r19
	brne	WAIT_A_LOT
	dec		r18
	brne	WAIT_A_LITTLE_MORE
	dec		r17
	brne	WAIT_A_LITTLE

LOOPEN:
	lds		r16, PLAY_TUNE
	sbrc	r16, 0
	call	PREPARE_RANDOM_SOUND_2		; Sound for game_over
	
	lds		r16, PLAY_TUNE
	sbrs	r16, 0
	call	PREPARE_RANDOM_SOUND_1		; Sound for new_game

	ldi		r19, 15
	ldi		r17, 255
DELAY1:	
	ldi		r18, 150
DELAY2:
	sbis	PINA, JOY_FIRE				; Exit loop if JOY_FIRE pressed
	rjmp	END_LOOPEN
	dec		r18
	brne	DELAY2
	dec		r17
	brne	DELAY1
	dec		r19
	brne	DELAY1
	
	rjmp	LOOPEN						; Forever loop
END_LOOPEN:
ret

;;----------------------------------
;;----- PLAYER_DEAD_SOUND
;; Put game on pause - plays short sound 
PLAYER_DEAD_SOUND:
		push r16
		push r17
		push r18
		push r19
		push r20 
		in r16,SREG
		push r16

		ldi r20,$91
	   	ldi r19,PLAYER_DEAD_SOUND_TIME
DELAY:
		ldi r18,255   
DELAY_T:				 	
		ldi r17,FREQUENCY_PLAYER_DEAD_SOUND		
DELAY_T_REPEAT:
		ldi r16,$FF
DELAYLOOP_T:
		dec	r16
		brne	DELAYLOOP_T
		dec r17
		brne DELAY_T_REPEAT
		
		sbrc	r18, 0	
		rjmp PIN_HIGH1
PIN_LOW1:
		sbi	PORTA,7
		rjmp	DONE1
PIN_HIGH1:
		cbi	PORTA,7
DONE1:
		dec r18
		brne DELAY_T
		dec r19
		brne DELAY

		pop r16
		out SREG,r16
		pop r20
		pop r19
		pop r18
		pop r17
		pop r16
ret
;;###################################################