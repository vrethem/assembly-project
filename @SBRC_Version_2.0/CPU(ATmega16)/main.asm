 /*
 * _MEGA_SBRC.asm
 *
 *  Created: 2/3/2017 10:55:38 AM
 */ 
 .include "settings.inc"
 .include "macros.inc"
 .include "HW_INIT.asm"
 .include "NEW_GAME.asm"
 .include "GAME_LEVEL.asm"
 .include "GAME_TIC.asm"
 .include "COLLISION_BOX.asm"
 .include "Interupts.asm"
 .include "SOUND_EFFECTS.asm"
 .include "Monitor_mode.asm"


;;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;;=================		MAIN LOOP		================================
;;---Uses  Z-ptr, Y-ptr
MAIN:
	call	READ_JOYSTICK				; Read joystick input
	call	GAME_LEVEL					; Insert new ASTROID or ENEMY
	call	GAME_UPDATE					; Checks if game should update
	call	RANDOM_SEED					; Inc SEED
rjmp MAIN

;;------------------------------------
;;			READ_JOYSTICK
;; PORTA weak pullup, Trigg on Zero
;;---Uses  Z-ptr
;; Put input to Joy_buffer
READ_JOYSTICK:
	in		r16, PINA						
	com		r16							; Invert input
	andi	r16, $1F					; Player input on pin 0-4
	LOAD_Z	JOY_BUFFER
	ld		r17, Z		
	or		r17, r16					; OR -> Don't miss any input 
	st		Z, r17
ret

;;------------------------------------
;;			GAME_UPDATE
;;---Uses  Z-ptr
/*
 Decide if update 
 SEND_DATA after collision check -> Always send data inside boundries	
*/
GAME_UPDATE:
	lds		r16, TICS					; Get TICS
	cpi		r16, ( BASE_CLK / FPS )
	brlo	NO_UPDATE
UPDATE:	
	ldi		r16, MONITOR_MODE
	sbrc	r16, 0
	call	MONITOR_AI_PLAYER

	call	GAME_TIC					; Update game internally
	call	COLLISION_BOX				; Check collisions
	call	SEND_DATA					; Send objects 
	clr		r16									
	sts		TICS, r16					; Reset TICS
NO_UPDATE:
	nop
ret

;;------------------------------------
;;				SEND_DATA
/*	Send 3 bytes thrue UART, SPRITE_ID, X_ID, Y_ID
	Send START_FRAME two times at begining of new frame transfer
	Always send player 
	Send all VISIBLE objects in SRAM, start at ENEMY
	Send END_FRAME
*/
SEND_DATA:
	ldi		r16, START_FRAME					 
	call	UART_TRANSMIT						
	ldi		r16, START_FRAME
	call	UART_TRANSMIT
	LOAD_Y	PLAYER								
	call	SEND_OBJECT								
	FOR_VIS	Y, ENEMY, (OBJECTS -1), SEND_OBJECT	 
	ldi		r16, END_FRAME
	call	UART_TRANSMIT						
ret

;; ---- SEND one object ----
SEND_OBJECT:
	ldd		r16, Y + SPRITE_ID
	call	UART_TRANSMIT
	ldd		r16, Y + X_ID
	call	UART_TRANSMIT
	ldd		r16, Y + Y_ID
	call	UART_TRANSMIT
ret

;; ---- UART_Transmit ----
/* Require r16  */
UART_TRANSMIT:
	sbis	UCSRA, UDRE		; Wait for empty Transmit buffer
	rjmp	UART_Transmit
	
	out		UDR, r16		; Put data(r16) into buffer, sends the data
ret

;;------------------------------------
;;			Random_SEED
;; Inc SEED 
;; FRAME_MIN_Y <= SEED <= FRAME_MAX_Y
;;---
RANDOM_SEED:
	push	r16			
	INCSRAM	SEED
	lds		r16, SEED
	cpi		r16, FRAME_MAX_Y - SPRITE_SIZE
	brne	NOT_EQUAL
	ldi		r16, FRAME_MIN_Y
	sts		SEED, r16
NOT_EQUAL:
	pop		r16			
ret
;////////////////////////////////////////////////////////////////