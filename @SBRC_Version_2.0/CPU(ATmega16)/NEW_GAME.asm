;;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;;===========		NEW_GAME	==============================
;;--- Uses Y-ptr, Z-ptr
/*
	 CLEAR_Y SRAM, set player start pos, set LEVEL = START_LEVEL
	 Reset counters 
	 Add new HP-sprites
	 reset score sprites
*/
NEW_GAME:
	call	RANDOM_DEATH_SONG

	FOR		Y, PLAYER, OBJECTS, CLEAR_Y				; CLEAR all objects in SRAM

	LOAD_Z	PLAYER									; set VIS_FLAG for player
	ldi		r16, (1 << VIS_FLAG)			 
	std		Z + OBJECT_ID, r16

	ldi		p_pos, FRAME_MIN_X + 10					; set player POS_X, POS_Y
	sts		POS_X, r16
	ldi		p_pos, ( (FRAME_MAX_Y - FRAME_MIN_Y ) / 2 )
	sts		POS_Y, r16

	ldi		r16, START_LEVEL							
	sts		LEVEL, r16								; set LEVEL

	clr		r16
	sts		TICS, r16 
	sts		LEVEL_TIC, r16
	sts		SHOT_TIC, r16
	sts		TONE_TIC, r16							; Reset counters
	sts		PLAYER_AI_X, r16
	ldi		r16, 20
	sts		PLAYER_AI_Y, r16

	ldi		r16, (ASTROID_SPAWN_TIMER / 2 )
	sts		ASTROID_TIC, r16						; set tic for astroid

	clr		r16
	sts		ENEMY_TIC, r16							; set tic for enemy

	ldi		r16, (SPRITE_SIZE*2)					; HP start X-position
	FOR		Y, LIFE, LIFES, NEW_LIFE				; Set VIS_FLAG true for HP on screen

	ldi		r16, LIFES								; Set lifes
	sts		HP, r16

	ldi		r16, (WINDOW_SIZE_X - (SCORE_SPRITE_SIZE_X + 3 )); Score start X-position
	FOR		Y, SCORE, SCORE_NUM, RESET_HIGH_SCORE	
ret

;;------------------------------------
;;		RESET_HIGH_SCORE
;;---(Req r16)
/* Set sprite id of number-sprites to 0 */
RESET_HIGH_SCORE:
	ldi		r17, 0
	std		Y + OBJECT_ID, r17									; Set non-visible
	std		Y + SPRITE_ID, r17									; Set sprite_id to 0
	ldi		r17, WINDOW_SIZE_Y - ( SCORE_SPRITE_SIZE_Y + 1 )	; Score Y-position
	std		Y + X_ID, r16
	std		Y + Y_ID, r17
	subi	r16, SCORE_SPRITE_SIZE_X + 2 			; Dec r16 for next call
ret

;;------------------------------------
;;			NEW_LIFE
;;--- (Req r16)
NEW_LIFE:
	ldi		r17, $80								; set VIS_FLAG
	std		Y + OBJECT_ID, r17 
	ldi		r17, WINDOW_SIZE_Y - 8					; HP Y-position
	std		Y + X_ID, r16							; X-pos = r16
	std		Y + Y_ID, r17							; Y-pos = WINDOW_SIZE_Y - 8
	subi	r16, (-SPRITE_SIZE)						; Inc r16 for next call
ret

;;------------------------------------
;;				CLEAR Y / Z
;;---  Req Y-ptr 
/* Clear object at ptr	*/
CLEAR_Y:
	push	r16
	clr		r16								
	std		Y + OBJECT_ID, r16				; Clear flags 
	std		Y + X_ID, r16					; Clear X-pos
	std		Y + Y_ID, r16					; Clear Y-pos
	pop		r16
ret
CLEAR_Z:
	push	r16
	clr		r16								
	std		Z + OBJECT_ID, r16				; Clear flags 
	std		Z + X_ID, r16					; Clear X-pos
	std		Z + Y_ID, r16					; Clear Y-pos
	pop		r16
ret
;;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\





;;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;;===========		INIT_SPRITE_SETTINGS	======================
;;--- Sets sprite_id for objects
INIT_SPRITE_SETTINGS:
	LOAD_Z	PLAYER								; set player sprite = 0
	ldi		p_pos, PLAYER_SPRITE_ID
	std		Z + SPRITE_ID, p_pos
	ldi		r16, ASTROID_SPRITE_ID
	FOR		Y, ASTROID, ASTROIDS, SET_SPRITE_ID	; Set astroid sprite
	ldi		r16, ENEMY_SPRITE_ID
	FOR		Y, ENEMY  , ENEMIES , SET_SPRITE_ID	; set enemy sprite
	ldi		r16, BULLET_SPRITE_ID
	FOR		Y, BULLET , BULLETS , SET_SPRITE_ID	; set bullet sprite 
	ldi		r16, HP_SPRITE_ID
	FOR		Y, LIFE   , LIFES   , SET_SPRITE_ID	; set HP sprite
	clr		r16
	FOR		Y, SCORE , SCORE_NUM, SET_SPRITE_ID	; set 0 to score sprites
ret

;---- SET_SPRITE_ID ----
SET_SPRITE_ID:									
	std		Y + SPRITE_ID, r16
ret
;///////////////////////////////////////////////////////////////