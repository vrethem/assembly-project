;;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;;==============	INTERUPPTS		===========================

;;----------------------------------
;;----- INTERUPT_BEEP
;; Increase counters and SEED value
INTERUPPT_TIC_COUNT:
	push	r16
	in		r16,SREG
	push	r17
	push	r18
	push	r19
	push	r20		
	push	r21		

	INCSRAM	TICS						
	INCSRAM	ASTROID_TIC
	INCSRAM	ENEMY_TIC
	call RANDOM_SEED 
	
	pop		r21
	pop		r20
	pop		r19
	pop		r18
	pop		r17			
	out		SREG, r16
	pop		r16
reti

;;----------------------------------
;;----- INTERUPT_BEEP
;; Increases tone tic
;; Set PORTA, 7 half high half low
INTERUPT_BEEP:
		push	r16
		in		r16,SREG
		push	r16
		clr		r16
		out		TCNT0,r16			; Reset timer 0 match register
		INCSRAM TONE_TIC
		lds		r16, TONE_TIC
		cpi		r16, TONE_LENGTH
		brne	SHOOT_BEEP
END_SOUND:
		ldi		r16,(1<<OCIE1A)		; Disable sound effect interupt	(INTERUPT_BEEP)									
		out		TIMSK,r16
		clr		r16
		sts		TONE_TIC, r16		; Clear TONE_TIC counter
		rjmp	DONE3
SHOOT_BEEP:
		sbrc	r16, 0	
		rjmp PIN_HIGH3
PIN_LOW3:
		sbi	PORTA, SPEAKER_PIN
		rjmp	DONE3
PIN_HIGH3:
		cbi	PORTA, SPEAKER_PIN
DONE3:
		INCSRAM		SEED
		pop r16
		out SREG,r16
		pop r16
reti

;;======================================================
;;//////////////////////////////////////////////////////