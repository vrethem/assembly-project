;;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
;;=========				GAME TIC		================================
;;---Uses  Z-ptr, Y-ptr
/*
	Move player from user input
	Move all objects in game
	Generate new enemy bullets
*/
GAME_TIC:
	INCSRAM		SHOT_TIC								; Inc SHOT_TIC
	call		GET_JOY_BUFFER							; Move player
	FOR_VIS		Y, ASTROID, ASTROIDS, MOVE_ASTROID		; Move astroids
	FOR_VIS		Y, ENEMY  , ENEMIES , MOVE_ENEMY		; Move enemies 
	FOR_VIS		Y, BULLET , BULLETS , MOVE_BULLET		; Move bullets in game
	FOR_VIS		Y, ENEMY  , ENEMIES , ENEMY_BULLET		; Randomly genrates new bullets 
ret

;;---------------------------------------------------------------->>
;;------- GET_JOY_BUFFER
;;  Reads JOY_BUFFER then flushes JOY_BUFFER
;;---Uses  Y-ptr
GET_JOY_BUFFER:
	lds		r17, JOY_BUFFER			; Get input buffer 
	LOAD_Y	PLAYER					; DEC_X and DEC_Y macro req Y-ptr 
	sbrs	r17, JOY_LEFT
	rjmp	NEXT1					
	DEC_X	PLAYER_SPEED			; Move left (req Y-ptr)
Next1:
	sbrs	r17, JOY_RIGHT
	rjmp	NEXT2
	DEC_X	(-PLAYER_SPEED)			; Move right (req Y-ptr)
Next2:
	sbrs	r17, JOY_UP
	rjmp	Next3
	DEC_Y	PLAYER_SPEED			; Move up	 (req Y-ptr)
Next3:
	sbrs	r17, JOY_DOWN			
	rjmp	Next4
	DEC_Y	(-PLAYER_SPEED)			; Move down  (req Y-ptr)
Next4:
	sbrs	r17, JOY_FIRE
	rjmp	Next5
	call	NEW_BULLET				; Fire 

Next5:			
	clr		r17						; Empty joystick buffer
	sts		JOY_BUFFER, r17
ret

;;------------------------------------
;;			NEW_BULLET
;;---Uses  Y-ptr
/*
	Always set PLAYER + SPRITE_ID = PLAYER_SPRITE_ID
	thus game will continue if PLAYER_DEAD_SPRITE is set 
	Reset SHOT_TIC 
*/
NEW_BULLET:
	lds		r16, SHOT_TIC				; When SHOT_TIC reaches value allow new shot
	cpi		r16, GAME_SPEED / RELOAD_TIME
	brlo	NO_SHOT 
	FIND_INV	Y, BULLET, BULLETS, INSERT_PLAYER_BULLET
	clr		r16
	sts		SHOT_TIC, r16				; clr SHOT_TIC
	ldi		r16, $80					; Player VIS_FLAG == TRUE 
	sts		PLAYER, r16					; Continue game after HP-loss here
	ldi		r16, PLAYER_SPRITE_ID		; Player sprite changed when player dead
	sts		PLAYER + SPRITE_ID, r16		; Reset player_sprite_id 
NO_SHOT:
ret

;;------------------------------------
;;			INSERT_PLAYER_BULLET	
;;--- (Req Y-ptr)
/*
	Sets VIS_FLAG and PSD_FLAG and coordinates 
	for object at Y-ptr
	set visible and positive direction flags
	Adds a new bullet 1 pixel infront of player to avoid collision
*/
INSERT_PLAYER_BULLET:
	push	r16	
	ldd		r16, Y + OBJECT_ID
	sbr		r16, (1 << VIS_FLAG) | (1 << PSD_FLAG) 
	std		Y + OBJECT_ID, r16
	lds		r16, POS_X			
	subi	r16, -(PLAYER_SIZE_X	+ 1)			; set X-pos
	std		Y + X_ID, r16
	lds		r16, POS_Y
	subi	r16, (-PLAYER_SIZE_Y) / 2				; set Y-pos
	std		Y + Y_ID, r16

	call	PREPARE_SHOOT_SOUND
	set												; set T-flag 
	pop		r16
ret
;;----------------------------------------------------------------<<

;;------------------------------------
;;				MOVE_ASTROID
;;--- (Req Y-ptr)
MOVE_ASTROID:
	DEC_X	ASTROID_SPEED
ret

;;---------------------------------------------------------------->>
;;				MOVE_ENEMY
/*
	Require Y-ptr to be set at enemy object
	Move enemy in up or down pattern depending on bit 0-4 in OBJECT_ID
	Enemy is AI-controlled if AI_FLAG 
	Dec X-pos
*/ 
;;---(Req Y-ptr)
MOVE_ENEMY:
	clt
	ldd		r16, Y + OBJECT_ID
	sbrc	r16, AI_FLAG
	call	AI_ENEMY						
	andi	r16, $1F						; Load bit 0-4
	brts	END_MOVE_ENEMY					; T-flag set if AI_ENEMY decide new direction

	cpi		r16, 8			
	brsh	MOVE_UP							

MOVE_DOWN:									; If 7 or lower MOVE_DOWN
	DEC_Y	(-1)
	inc		r16
	DEC_X	ENEMY_SPEED						; Dec X-pos 
	rjmp	END_MOVE_ENEMY	
MOVE_UP:									; If 8 or higher MOVE_UP
	DEC_Y	1
	inc		r16
	sbrc	r16, 4							; Reset if r16 == 8
	ldi		r16, 0
	DEC_X	ENEMY_SPEED						; Dec X-pos 
END_MOVE_ENEMY:
	ldd		r17, Y + OBJECT_ID
	sbr		r16, $E0			
	sbr		r17, $1F
	and		r17, r16						; Save change to bit 0-4, bit 5-7 unchanged
	std		Y + OBJECT_ID, r17

	
ret

;;------------------------------------
;;			AI_ENEMY
;; AI-ish controlled enemies
;;---(Req Y-ptr) (Req r16)
/*
	Check distance to player in X- and Y-axis
	Makes a new pattern towards the player when bit 0-4 in OBJECT_ID
	is equal to 0 or 8
*/
AI_ENEMY:
	lds		r17, PLAYER
	sbrs	r17, VIS_FLAG					; Skip if player dead
	rjmp	AI_END

	andi	r16, $1F						; Load bit 0-4
	ldd		r17, Y + Y_ID
	lds		r18, POS_Y
	sub		r17, r18						; Check distance Y-axis
	brlo	NEGATIVE_1
	cpi		r17, AI_ENGAGE_Y				; If below AI_ENGAGE_Y engage
	brpl	AI_END

NEGATIVE_1:
	ldd		r17, Y + Y_ID
	lds		r18, POS_Y						; Check distance Y-axis
	sub		r18, r17
	cpi		r18, AI_ENGAGE_Y				; If below AI_ENGAGE_Y engage
	brpl	AI_END

	ldd		r17, Y + X_ID
	cpi		r17, (FRAME_MAX_X - 12 )		; Dont let Enemy go outside right frame side
	brlo	INSIDE_FRAME
	rjmp	AI_END

INSIDE_FRAME:
	lds		r18, POS_X
	sub		r17, r18						; Check distance X-axis
	brlo	NEXT_MOVE
	cpi		r17, AI_ENGAGE_X				; If below AI_ENGAGE_X engage
	brpl	AI_END

	cpi		r16, 0			
	breq	NEXT_MOVE						; If 0 or 8, make new move direction 
	cpi		r16, 8
	breq	NEXT_MOVE						

	rjmp	AI_END							; Continue move pattern

NEXT_MOVE:
	lds		r17, POS_Y
	ldd		r18, Y + Y_ID					; New move pattern
	subi	r18, -15
	cp		r17, r18
	brpl	AI_DOWN
AI_UP:
	ldi		r16, 7
	;DEC_Y	1
	inc		r16
	DEC_X	-ENEMY_SPEED
	set
	rjmp	AI_END
AI_DOWN:
	ldi		r16, 0
	;DEC_Y	(-1)
	inc		r16
	DEC_X	-ENEMY_SPEED
	set
AI_END:
ret
;;----------------------------------------------------------------<<


;;------------------------------------
;;				MOVE_BULLET
;;---(Req Y-ptr)
/*
	Require Y-ptr to be set at bullet object
	Check PSD_FLAG in OBJECT_ID for bullet direction
*/
MOVE_BULLET:
	ldd		r16, Y + OBJECT_ID
	sbrs	r16, PSD_FLAG		
	rjmp	Skip3
	DEC_X	(-BULLET_SPEED)					; if Player bullet
	rjmp	END_MOVE
Skip3:
	DEC_X	(Bullet_Speed)					; if Enemey bullet
END_MOVE:
	nop
ret

;;---------------------------------------------------------------->>
;;		ENEMY_BULLET
;;--- (Req Y-ptr)(Uses Z-ptr)
/*
	Require Y-ptr set at ENEMEY-object
	Sets Z-ptr at BULLET-object for call to INSERT_ENEMY_BULLET
	Randomly generate new enemy bullets
*/
ENEMY_BULLET:
	LOAD_Z	SEED
	ld		r16, Z											; 120 >= SEED >= 8
	cpi		r16, FRAME_MAX_Y - SPRITE_SIZE - ENEMY_RELOAD	; if  SEED > 120 - EMEMY_RELOAD 
	brlo	Skip_bullet
		FIND_INV		Z, BULLET, BULLETS, INSERT_ENEMY_BULLET
Skip_bullet:
	ldd		r17, Y + Y_ID ; or X_ID
RANDOMIZE_SEED:
	call	RANDOM_SEED								; Try to randomize new SEED-value
	dec		r17										; Allows enemies in the middle of SRAM-
	brne	RANDOMIZE_SEED							; table to fire bullets
ret

;;------------------------------------
;;		INSERT_ENEMY_BULLET
;;---(Req Y-ptr)(Req Z-ptr)
/*
	Insert new bullet at Z-ptr, requiers X,Y coordinat from Y-ptr
	Sets visible and negative direction flags
*/
INSERT_ENEMY_BULLET:
	push	r16	
	ldd		r16, Z + OBJECT_ID
	sbr		r16, (1 << VIS_FLAG) | (0 << PSD_FLAG)		 
	std		Z + OBJECT_ID, r16
	ldd		r16, Y + X_ID								; Get X-pos from Y-ptr
	subi	r16, 1										
	std		Z + X_ID, r16								; set X-pos to Z-ptr

	ldd		r16, Y + Y_ID								; Get Y-pos from Y-ptr
	subi	r16, (-ENEMY_SIZE_Y) / 2					
	std		Z + Y_ID, r16								; set Y-pos to Z-ptr
	call	PREPARE_ENEMY_BULLET_SOUND
	set
	pop		r16
ret
;;----------------------------------------------------------------<<

;///////////////////////////////////////////////////////////////////////