/////////////////////////  int0_isr.asm   ////////////////////////////
;
; ISR that turns off the outputs to protect FPGA in case of FPGA-restart
; Uses registers
;	temp		= r16


/////////////////////////  INT0 ISR   ////////////////////////////

INT0_ISR:
	out	PORTA, zero
	out	PORTB, zero
	out	PORTC, zero
	out	DDRA, zero
	out	DDRB, zero
	out	DDRC, zero

	jmp	RESET
	reti

;_________________________ INT0 ISR END _________________________
