/////////////////// init.asm ////////////////////
; Contains RESET routine
; Contains routines for initiating hardware and variables
; Uses registers:
;	temp	= r16
.def	counter	= r17

RESET:
	cli

	; Secure the FPGA as fast as possible
	clr	temp
	out	PORTA, temp
	out	PORTB, temp
	out	PORTC, temp
	out	DDRA, temp
	out	DDRB, temp
	out	DDRC, temp
	
	;Set stack pointer
	ldi	temp, HIGH(RAMEND)
	out	SPH, temp
	ldi	temp, LOW(RAMEND)
	out	SPL, temp

	rcall	HARD_INIT
	rcall	SOFT_INIT

	; Enable interrupts
	sei
	;.include "fill_sram_static.asm"
	jmp	main_loop

HARD_INIT:
	cli

	; Set registers for faster calculations
	ldi	temp, SCR_WIDTH
	mov	screen_width, temp
	ldi	temp, SCR_HEIGHT
	mov	screen_height, temp
	clr	zero

	; Enable external interrupt INT0 on falling edge
	ldi	temp, 1 << ISC01
	out	MCUCR, temp
	ldi	temp, 1 << INT0
	out	GICR, temp


	//////////////// UART SETUP ////////////////
	;Set braud-rate
	ldi	temp, UBRRH_SETTING
	out	UBRRH, temp
	ldi	temp, UBRRL_SETTING
	out	UBRRL, temp

	;Set UART interupts
	ldi	temp, (1<<RXCIE) | (1 << RXEN)
	out	UCSRB, temp

	;Set frame format 8-bit data
	ldi	temp, (1 << URSEL) | (1 << UCSZ0) | (1 << UCSZ1)
	out	UCSRC, temp
	;_______________ UART SETUP ________________


	; Make sure the FPGA is configured for 5V input before setting I/O
	; The RUNNING pin must be high for > xx ms
	ldi	temp, 5
	out	TCCR0, temp		; Prescaler = 1024
	clr	temp
	out	TCNT0, temp		; Reset counter0
	in	temp, TIMSK
	sbr	temp, 1 << TOIE0
	out	TIMSK, temp		; Enable overflow interrupt

	ldi	counter, FPGA_STARTUP_DELAY
WAIT_FOR_FPGA_RUNNING:
	; Reset counter if READY pin ever go to 0
	sbis	PIND, RUNNING_PIN
	ldi	counter, FPGA_STARTUP_DELAY

	; Wait for timer0 overflow
	in	temp, TIFR
	sbrs	temp, TOV0
	rjmp	WAIT_FOR_FPGA_RUNNING

	; Reset overflow flag (sbi doesn't work on TIFR)
	in	temp, TIFR
	out	TIFR, temp

	; dec counter if timer0 overflow
	dec	counter
	; Keep waiting until counter = 0
	brne	WAIT_FOR_FPGA_RUNNING

	; Setup I/O
	ldi	temp, $FF
	out	DDRA, temp
	out	DDRB, temp
	ldi	temp, $80    ;1000 0000
	out	DDRC, temp
	ldi	temp, $F0    ;1111 0000
	out	DDRD, temp

	ret

SOFT_INIT:
	push	temp
	in	temp, SREG
	push	temp

	;Set all SRAM variables to default value
	clr	temp
	sts	DONE, temp
	sts	OBJECT_COUNT, temp
	;sts	ANIM_COUNT, temp

	ldi	temp, RX_STATE_GET_ID
	sts	STATE, temp

	;set Object stack pointer to bottom of stack
	ldi	temp, LOW(OBJECT_LIST)
	sts	OBJECT_ADR, temp
	ldi	temp, HIGH(OBJECT_LIST)
	sts	OBJECT_ADR+1, temp
	
	pop	temp
	out	SREG, temp
	pop	temp
	ret
