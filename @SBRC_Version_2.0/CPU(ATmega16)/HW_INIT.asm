.org $00
	jmp HW_INIT
.org $026								; Timer 0  Sound effects 
	jmp INTERUPT_BEEP					
.org OC1Aaddr							; Timer 1 Game tics
	jmp INTERUPPT_TIC_COUNT				

.org	$2A
HW_INIT:
	ldi		r16, HIGH(RAMEND)			; set Stack-pointer
	out		SPH, r16
	ldi		r16, LOW(RAMEND)
	out		SPL, r16

;; Timer 0 - Setup sound effects
	ldi		r16,3						
	out		TCCR0,r16				; S�tter timer 0 prescaler till clk/64
	
	clr		r16
	sts		TONE_TIC, r16			; Sound effects byte in SRAM

; Timer 1 - 16-bit Timer, setup interupt for 252 Hz
	ldi		r16,(1<<CS12)|(0<<CS11)|(1<<CS10)|(1 << WGM12)	; Prescaler CLK/1024
	out		TCCR1B,r16
	ldi		r16, $00	
	out		OCR1AH, r16						; Set match registerA
	ldi		r16, $3E
	out		OCR1AL,r16

	ldi		r16,(1<<OCIE1A)					; Enable timer 1 interupts
	out		TIMSK,r16

;; Setup SPEAKER
	ldi		r16, $80		; Weak pullup PORTA 0-4
	out		DDRA, r16		; Pin7 Audio output pin
	ldi		r16,  $1F
	out		PORTA, r16

UART_INIT:
; Set baud rate 
	ldi		r17, $00
	ldi		r16, $00
	out		UBRRH, r17
	out		UBRRL, r16
; Enable Transmit
	ldi		r16, (1 << TXEN)
	out		UCSRB, r16
; Set frame format 8-bit data, 1 stop bit
	ldi		r16, (1<<URSEL) | (1<<UCSZ1) | (1<<UCSZ0)
	out		UCSRC, r16

;; Setup sprite-id
	call	INIT_SPRITE_SETTINGS
	
	sei		; ENABLE INTERUPPTS GLOBALLY !
	ldi		r16, 0
	sts		PLAY_TUNE, r16		; Enable startup song
	call	NEW_GAME
	ldi		r16, 1
	sts		PLAY_TUNE, r16		; Enable endgame song
	rjmp	MAIN
