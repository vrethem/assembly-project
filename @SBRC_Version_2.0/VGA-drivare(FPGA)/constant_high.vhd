library ieee;
use ieee.std_logic_1164.all;

entity constant_high is
	port( logic_one  : out std_logic;
			logic_zero : out std_logic);
end constant_high;

architecture constant_high_beh of constant_high is
begin
	logic_one <= '1';
	logic_zero <= '0';
end constant_high_beh;