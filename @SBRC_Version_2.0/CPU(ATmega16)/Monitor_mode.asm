;;------------------------------------
;;			MONITOR_AI_PLAYER
;;--- Move player after pattern
;; 
MONITOR_AI_PLAYER:
// Move in X-axis
	lds		r16, PLAYER_AI_X
	clr		r17
	cpi		r16, 40
	brlo	RIGHT
LEFT:
	sbr		r17, (1 << JOY_LEFT)
	rjmp	X_AXIS_DONE
RIGHT:
	sbr		r17, (1 << JOY_LEFT)
X_AXIS_DONE:
	lds		r16, PLAYER_AI_X
	inc		r16
	cpi		r16, 80
	brlo	SKIP_CLEAR
		clr		r16
SKIP_CLEAR:
	sts		PLAYER_AI_X, r16

// Move in Y-axis
	lds		r16, PLAYER_AI_Y
	cpi		r16, 40
	brlo	DOWN
UP:
	sbr		r17, (1 << JOY_UP)
	rjmp	Y_AXIS_DONE
DOWN:
	sbr		r17, (1 << JOY_DOWN)
Y_AXIS_DONE:
	lds		r16, PLAYER_AI_Y
	inc		r16
	cpi		r16, 80
	brlo	SKIP_CLEAR2
		clr		r16
SKIP_CLEAR2:
	sts		PLAYER_AI_Y, r16
	sts		JOY_BUFFER, r17
ret


;; --------------------------------------------
;; -------- MONITOR_RANDOM_AI
;; Value of SEED controlls player movement
;;
;;
MONITOR_RANDOM_AI:
	// Move in X-axis
	lds		r16, PLAYER_AI_X
	cpi		r16, 0
	brne	NO_NEW_MOVE
NEW_MOVE:
	lds		r16, SEED
	andi	r16, $03

	cpi		r16, 0
	breq	NORTH_EAST
	cpi		r16, 1
	breq	NORTH_WEST
	cpi		r16, 2
	breq	SOUTH_WEST
	cpi		r16, 3
	breq	SOUTH_EAST

NORTH_EAST:
	ldi		r16, 0
	sts		PLAYER_AI_X, r16
	ldi		r16, 3
	sts		PLAYER_AI_Y, r16
	rjmp	NEW_MOVE_COMPLETE
NORTH_WEST:
	ldi		r16, 3
	sts		PLAYER_AI_X, r16
	ldi		r16, 3
	sts		PLAYER_AI_Y, r16
	rjmp	NEW_MOVE_COMPLETE
SOUTH_WEST:
	ldi		r16, 3
	sts		PLAYER_AI_X, r16
	ldi		r16, 0
	sts		PLAYER_AI_Y, r16
	rjmp	NEW_MOVE_COMPLETE
SOUTH_EAST:
	ldi		r16, 0
	sts		PLAYER_AI_X, r16
	ldi		r16, 0
	sts		PLAYER_AI_Y, r16
	rjmp	NEW_MOVE_COMPLETE

NEW_MOVE_COMPLETE:
	lds		r16, PLAYER_AI_X		
NO_NEW_MOVE:
	clr		r17
	cpi		r16, 3
	brlo	RIGHT1
LEFT1:
	sbr		r17, (1 << JOY_LEFT)
	rjmp	X_AXIS_DONE1
RIGHT1:
	sbr		r17, (1 << JOY_LEFT)
X_AXIS_DONE1:
	lds		r16, PLAYER_AI_X
	inc		r16
	cpi		r16, 6
	brlo	SKIP_CLEAR3
		clr		r16
SKIP_CLEAR3:
	sts		PLAYER_AI_X, r16

// Move in Y-axis
	lds		r16, PLAYER_AI_Y
	cpi		r16, 3
	brlo	DOWN1
UP1:
	sbr		r17, (1 << JOY_UP)
	rjmp	Y_AXIS_DONE1
DOWN1:
	sbr		r17, (1 << JOY_DOWN)
Y_AXIS_DONE1:
	lds		r16, PLAYER_AI_Y
	inc		r16
	cpi		r16, 6
	brlo	SKIP_CLEAR4
		clr		r16
SKIP_CLEAR4:
	sts		PLAYER_AI_Y, r16
	sts		JOY_BUFFER, r17
ret