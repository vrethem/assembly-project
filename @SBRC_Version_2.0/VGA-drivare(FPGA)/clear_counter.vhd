library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity clear_counter is
	port( clk   : in std_logic;
			reset : in std_logic;
			adr   : buffer std_logic_vector(14 downto 0);
			wr    : out std_logic;
			color	: out std_logic_vector(3 downto 0));
end clear_counter;

architecture clear_counter_beh of clear_counter is
	constant clear_color : std_logic_vector(3 downto 0) := "1010";
begin

	process(clk, reset)
	begin
		if (reset = '1') then
			adr <= (others => '0');
		elsif falling_edge(clk) then
			if adr < 19199 then
				adr <= adr+1;
			else
				adr <= (others => '0');
			end if;
		end if;
	end process;
	
	color <= adr(3 downto 0);
	--color <= "0010";
	wr <= clk;
	
end clear_counter_beh;