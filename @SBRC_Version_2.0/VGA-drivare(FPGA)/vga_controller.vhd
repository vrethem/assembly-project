library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity vga_controller is
	port( clk		: in std_logic;
			adr		: in std_logic_vector(14 downto 0);
			color		: in std_logic_vector(3 downto 0);
			wr			: in std_logic;
			clear		: in std_logic;
			r			: out std_logic_vector(3 downto 0);
			g			: out std_logic_vector(3 downto 0);
			b			: out std_logic_vector(3 downto 0);
			h_sync	: out std_logic;
			v_sync	: out std_logic);
end vga_controller;

architecture vga_controller_beh of vga_controller is
	component pll_unit
		port(	inclk0	: IN STD_LOGIC;
				c0			: OUT STD_LOGIC ;
				c1			: OUT STD_LOGIC);
	end component;
	
	component vga_counter
		port( clk		: in std_logic;
				reset		: in std_logic;
				x			: out std_logic_vector(9 downto 0);
				y			: out std_logic_vector(8 downto 0);
				h_sync	: out std_logic;
				v_sync	: out std_logic;
				blanking	: out std_logic);
	end component;
	
	component xy_translator
		port( x		: in std_logic_vector(9 downto 0);
				y		: in std_logic_vector(8 downto 0);
				adr	: out std_logic_vector(14 downto 0));
	end component;
	
	component clear_counter
		port( clk   : in std_logic;
				reset : in std_logic;
				adr   : buffer std_logic_vector(14 downto 0);
				wr    : out std_logic;
				color	: out std_logic_vector(3 downto 0));
	end component;
	
	component mux20
		PORT( data0x	: IN STD_LOGIC_VECTOR (19 DOWNTO 0);
				data1x	: IN STD_LOGIC_VECTOR (19 DOWNTO 0);
				sel		: IN STD_LOGIC ;
				result	: OUT STD_LOGIC_VECTOR (19 DOWNTO 0));
	end component;
	
	component vram160x120x4
		port(	data			: IN STD_LOGIC_VECTOR (3 DOWNTO 0);
				rdaddress	: IN STD_LOGIC_VECTOR (14 DOWNTO 0);
				rdclock		: IN STD_LOGIC;
				rden			: IN STD_LOGIC;
				wraddress	: IN STD_LOGIC_VECTOR (14 DOWNTO 0);
				wrclock		: IN STD_LOGIC;
				wren			: IN STD_LOGIC;
				q				: OUT STD_LOGIC_VECTOR (3 DOWNTO 0));
	end component;
	
	component color_palette
		port( clk		: in std_logic;
				color 	: in std_logic_vector(3 downto 0);
				blanking : in std_logic;
				r			: out std_logic_vector(3 downto 0);
				g			: out std_logic_vector(3 downto 0);
				b			: out std_logic_vector(3 downto 0));
	end component;
	
	signal clk_25		: std_logic;
	signal clk_200		: std_logic;
	
	signal vram_mux_0 : std_logic_vector(19 downto 0);
	signal vram_mux_1 : std_logic_vector(19 downto 0);
	signal vram_mux_r : std_logic_vector(19 downto 0);
	
	signal pixel_x			: std_logic_vector(9 downto 0);
	signal pixel_y			: std_logic_vector(8 downto 0);
	signal pixel_adr		: std_logic_vector(14 downto 0);
	signal h_sync_pipe	: std_logic_vector(3 downto 0);
	signal v_sync_pipe	: std_logic_vector(3 downto 0);
	signal blanking_pipe : std_logic_vector(3 downto 0);
	
	signal color_index	: std_logic_vector(3 downto 0);
begin

	main_pll : pll_unit
		PORT MAP (	inclk0	=> clk,
						c0			=> clk_25,
						c1			=> clk_200);

	pixel_counter : vga_counter
		PORT MAP(	clk =>  clk_25,
						reset => '0',
						x => pixel_x,
						y => pixel_y,
						h_sync => h_sync_pipe(0),
						v_sync => v_sync_pipe(0),
						blanking => blanking_pipe(0));

	
	from640x480to160x120 : xy_translator
		PORT MAP(	x		=> pixel_x,
						y		=> pixel_y,
						adr	=> pixel_adr);
						
	clear_screen : clear_counter
		PORT MAP(	clk   => clk_200,
						reset => '0',
						adr   => vram_mux_1(14 downto 0),
						wr    => vram_mux_1(19),
						color => vram_mux_1(18 downto 15));
						
	clear_mux : mux20
		PORT MAP(	data0x	=> vram_mux_0,
						data1x	=> vram_mux_1,
						sel		=> clear,
						result	=> vram_mux_r);
						
	vram : vram160x120x4
		PORT MAP(	data			=> vram_mux_r(18 downto 15),
						rdaddress	=> pixel_adr,
						rdclock		=> clk_25,
						rden			=> '1',
						wraddress	=> vram_mux_r(14 downto 0),
						wrclock		=> vram_mux_r(19),
						wren			=> '1',
						q				=> color_index);
						
	color_rom : color_palette
		PORT MAP(	clk		=> clk_25,
						color 	=> color_index,
						blanking => blanking_pipe(1),
						r			=> r,
						g			=> g,
						b			=> b);
						
	process(clk_25)
	begin
		if rising_edge(clk_25) then
			h_sync_pipe(3 downto 1)		<= h_sync_pipe(2 downto 0);
			v_sync_pipe(3 downto 1)		<= v_sync_pipe(2 downto 0);
			blanking_pipe(3 downto 1)	<= blanking_pipe(2 downto 0);
		end if;
	end process;
	
	vram_mux_0(14 downto 0)		<= adr;
	vram_mux_0(18 downto 15)	<= color;
	vram_mux_0(19)					<= wr;
	
	
	
	h_sync <= not h_sync_pipe(2);
	v_sync <= not v_sync_pipe(2);

end vga_controller_beh;

