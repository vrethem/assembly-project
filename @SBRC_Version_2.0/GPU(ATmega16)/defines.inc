/////////////////////////  defines.inc   ////////////////////////////
;
; Constants and defined registers

; Uncomment to enable debug locks
;#define DEBUG
#define ANIMATIONS

.equ	MAX_OBJECTS	= 255
.equ	OBJECT_SIZE	= 3
.equ	SPR_SIZE	= 8

.equ	SCR_WIDTH	= 160
.equ	SCR_HEIGHT	= 120

; Transparancy color
.equ	KEY_COLOR	= $f
; Bullet color
.equ	BULLET_COLOR	= 7

; Sprite indices
.equ	PLAYER_ID	= 10
.equ	ENEMY_ID	= 11
.equ	ASTROID_ID	= 12
.equ	BULLET_ID	= 13
.equ	LIFE_ID		= 14
.equ	DEAD_ID		= 15
; MAX_SPRITE_ID
.equ	MAX_SPRITE_ID	= 15

; Animated frames translates to this id (+ part of anim_count)
.equ	PLAYER_ANIM	= 18
.equ	ENEMY_ANIM	= 22
.equ	ASTROID_ANIM	= 26
.equ	LIFE_ANIM	= 30
.equ	DEAD_ANIM	= 32

; Animation prescaler
.equ	ANIM_PRESCALER_VALUE	= 4

; I/O settings
.equ	WRITE_PIN	= 7	; Port B
.equ	CLEAR_PIN	= 7	; Port C
.equ	READY_PIN	= 3	; Port D
.equ	RUNNING_PIN	= 2	; Port D Interrupt driven INT0

; Startup delay for FPGA
.equ	FPGA_STARTUP_DELAY	= $06 ; No lower than 6

; States for uart rx
.equ	RX_STATE_GET_ID	= 0
.equ	RX_STATE_GET_X	= 1
.equ	RX_STATE_GET_Y	= 2

.equ	RX_END_FRAME	= $ff

; UART Settings
.equ	UBRRH_SETTING	= $0
.equ	UBRRL_SETTING	= $0






; Globally used registers
.def	zero		= r2	; "Constant" registers for faster calculations
.def	screen_width	= r3
.def	screen_height	= r4 
.def	temp		= r16
