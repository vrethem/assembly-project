-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Web Edition"

-- DATE "02/06/2017 22:30:00"

-- 
-- Device: Altera EP2C5T144C8 Package TQFP144
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEII;
LIBRARY IEEE;
USE CYCLONEII.CYCLONEII_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	vga_controller IS
    PORT (
	clk : IN std_logic;
	adr : IN std_logic_vector(14 DOWNTO 0);
	color : IN std_logic_vector(3 DOWNTO 0);
	wr : IN std_logic;
	clear : IN std_logic;
	r : OUT std_logic_vector(3 DOWNTO 0);
	g : OUT std_logic_vector(3 DOWNTO 0);
	b : OUT std_logic_vector(3 DOWNTO 0);
	h_sync : OUT std_logic;
	v_sync : OUT std_logic
	);
END vga_controller;

-- Design Ports Information
-- r[0]	=>  Location: PIN_114,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- r[1]	=>  Location: PIN_115,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- r[2]	=>  Location: PIN_118,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- r[3]	=>  Location: PIN_119,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- g[0]	=>  Location: PIN_120,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- g[1]	=>  Location: PIN_121,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- g[2]	=>  Location: PIN_122,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- g[3]	=>  Location: PIN_125,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- b[0]	=>  Location: PIN_126,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- b[1]	=>  Location: PIN_129,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- b[2]	=>  Location: PIN_132,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- b[3]	=>  Location: PIN_133,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- h_sync	=>  Location: PIN_113,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- v_sync	=>  Location: PIN_112,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- clk	=>  Location: PIN_17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- adr[14]	=>  Location: PIN_104,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
-- clear	=>  Location: PIN_74,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
-- adr[13]	=>  Location: PIN_103,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
-- adr[12]	=>  Location: PIN_101,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
-- color[0]	=>  Location: PIN_79,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
-- adr[0]	=>  Location: PIN_87,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
-- adr[1]	=>  Location: PIN_88,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
-- adr[2]	=>  Location: PIN_89,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
-- adr[3]	=>  Location: PIN_90,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
-- adr[4]	=>  Location: PIN_91,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
-- adr[5]	=>  Location: PIN_92,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
-- adr[6]	=>  Location: PIN_93,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
-- adr[7]	=>  Location: PIN_94,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
-- adr[8]	=>  Location: PIN_96,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
-- adr[9]	=>  Location: PIN_97,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
-- adr[10]	=>  Location: PIN_99,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
-- adr[11]	=>  Location: PIN_100,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
-- color[1]	=>  Location: PIN_80,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
-- color[2]	=>  Location: PIN_81,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
-- color[3]	=>  Location: PIN_86,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
-- wr	=>  Location: PIN_73,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default


ARCHITECTURE structure OF vga_controller IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_adr : std_logic_vector(14 DOWNTO 0);
SIGNAL ww_color : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_wr : std_logic;
SIGNAL ww_clear : std_logic;
SIGNAL ww_r : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_g : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_b : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_h_sync : std_logic;
SIGNAL ww_v_sync : std_logic;
SIGNAL \color_rom|palette_rom|altsyncram_component|auto_generated|ram_block1a0_PORTAADDR_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \color_rom|palette_rom|altsyncram_component|auto_generated|ram_block1a0_PORTADATAOUT_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \main_pll|altpll_component|pll_INCLK_bus\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \main_pll|altpll_component|pll_CLK_bus\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a16_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a16_PORTBDATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a16_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a16_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a16_PORTADATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a8_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a8_PORTBDATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a8_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a8_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a8_PORTADATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a4_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a4_PORTBDATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a4_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a4_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a4_PORTADATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a0_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a0_PORTBDATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a0_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a0_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a0_PORTADATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a12_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a12_PORTBDATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a12_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a12_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a12_PORTADATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a17_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a17_PORTBDATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a17_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a17_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a17_PORTADATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a5_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a5_PORTBDATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a5_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a5_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a5_PORTADATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a9_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a9_PORTBDATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a9_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a9_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a9_PORTADATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a1_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a1_PORTBDATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a1_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a1_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a1_PORTADATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a13_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a13_PORTBDATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a13_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a13_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a13_PORTADATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a18_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a18_PORTBDATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a18_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a18_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a18_PORTADATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a10_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a10_PORTBDATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a10_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a10_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a10_PORTADATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a6_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a6_PORTBDATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a6_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a6_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a6_PORTADATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a2_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a2_PORTBDATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a2_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a2_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a2_PORTADATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a14_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a14_PORTBDATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a14_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a14_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a14_PORTADATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a19_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a19_PORTBDATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a19_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a19_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a19_PORTADATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a7_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a7_PORTBDATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a7_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a7_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a7_PORTADATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a11_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a11_PORTBDATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a11_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a11_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a11_PORTADATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a3_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a3_PORTBDATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a3_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a3_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a3_PORTADATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a15_PORTADATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a15_PORTBDATAIN_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a15_PORTAADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a15_PORTBADDR_bus\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a15_PORTADATAOUT_bus\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \main_pll|altpll_component|_clk0~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \clear_mux|LPM_MUX_component|auto_generated|result_node[19]~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \main_pll|altpll_component|_clk1~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \main_pll|altpll_component|pll~CLK2\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a8~portadataout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a4~portadataout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a1~portadataout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a13~portadataout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a10~portadataout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a6~portadataout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a7~portadataout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a11~portadataout\ : std_logic;
SIGNAL \from640x480to160x120|Add0~0_combout\ : std_logic;
SIGNAL \from640x480to160x120|Add0~2_combout\ : std_logic;
SIGNAL \from640x480to160x120|Add0~8_combout\ : std_logic;
SIGNAL \from640x480to160x120|Add0~10_combout\ : std_logic;
SIGNAL \from640x480to160x120|Add0~12_combout\ : std_logic;
SIGNAL \pixel_counter|y_reg[0]~10_combout\ : std_logic;
SIGNAL \pixel_counter|y_reg[1]~12_combout\ : std_logic;
SIGNAL \pixel_counter|LessThan0~0_combout\ : std_logic;
SIGNAL \pixel_counter|process_2~0_combout\ : std_logic;
SIGNAL \pixel_counter|process_2~1_combout\ : std_logic;
SIGNAL \pixel_counter|process_2~2_combout\ : std_logic;
SIGNAL \pixel_counter|process_2~3_combout\ : std_logic;
SIGNAL \pixel_counter|LessThan5~1_combout\ : std_logic;
SIGNAL \pixel_counter|LessThan5~2_combout\ : std_logic;
SIGNAL \clk~combout\ : std_logic;
SIGNAL \main_pll|altpll_component|_clk0\ : std_logic;
SIGNAL \main_pll|altpll_component|_clk0~clkctrl_outclk\ : std_logic;
SIGNAL \clear~combout\ : std_logic;
SIGNAL \clear_screen|adr[0]~15_combout\ : std_logic;
SIGNAL \clear_screen|adr[12]~40\ : std_logic;
SIGNAL \clear_screen|adr[13]~41_combout\ : std_logic;
SIGNAL \clear_screen|adr[13]~42\ : std_logic;
SIGNAL \clear_screen|adr[14]~43_combout\ : std_logic;
SIGNAL \clear_screen|adr[10]~35_combout\ : std_logic;
SIGNAL \clear_screen|adr[6]~27_combout\ : std_logic;
SIGNAL \clear_screen|LessThan0~1_combout\ : std_logic;
SIGNAL \clear_screen|LessThan0~0_combout\ : std_logic;
SIGNAL \clear_screen|LessThan0~2_combout\ : std_logic;
SIGNAL \clear_screen|LessThan0~3_combout\ : std_logic;
SIGNAL \clear_screen|LessThan0~4_combout\ : std_logic;
SIGNAL \clear_screen|adr[0]~16\ : std_logic;
SIGNAL \clear_screen|adr[1]~17_combout\ : std_logic;
SIGNAL \clear_screen|adr[1]~18\ : std_logic;
SIGNAL \clear_screen|adr[2]~19_combout\ : std_logic;
SIGNAL \clear_screen|adr[2]~20\ : std_logic;
SIGNAL \clear_screen|adr[3]~22\ : std_logic;
SIGNAL \clear_screen|adr[4]~23_combout\ : std_logic;
SIGNAL \clear_screen|adr[4]~24\ : std_logic;
SIGNAL \clear_screen|adr[5]~26\ : std_logic;
SIGNAL \clear_screen|adr[6]~28\ : std_logic;
SIGNAL \clear_screen|adr[7]~29_combout\ : std_logic;
SIGNAL \clear_screen|adr[7]~30\ : std_logic;
SIGNAL \clear_screen|adr[8]~32\ : std_logic;
SIGNAL \clear_screen|adr[9]~33_combout\ : std_logic;
SIGNAL \clear_screen|adr[9]~34\ : std_logic;
SIGNAL \clear_screen|adr[10]~36\ : std_logic;
SIGNAL \clear_screen|adr[11]~37_combout\ : std_logic;
SIGNAL \clear_screen|adr[11]~38\ : std_logic;
SIGNAL \clear_screen|adr[12]~39_combout\ : std_logic;
SIGNAL \clear_mux|LPM_MUX_component|auto_generated|result_node[12]~2_combout\ : std_logic;
SIGNAL \clear_mux|LPM_MUX_component|auto_generated|result_node[14]~0_combout\ : std_logic;
SIGNAL \clear_mux|LPM_MUX_component|auto_generated|result_node[13]~1_combout\ : std_logic;
SIGNAL \wr~combout\ : std_logic;
SIGNAL \main_pll|altpll_component|_clk1\ : std_logic;
SIGNAL \main_pll|altpll_component|_clk1~clkctrl_outclk\ : std_logic;
SIGNAL \clear_mux|LPM_MUX_component|auto_generated|result_node[19]~clkctrl_outclk\ : std_logic;
SIGNAL \pixel_counter|y_reg[0]~11\ : std_logic;
SIGNAL \pixel_counter|y_reg[1]~13\ : std_logic;
SIGNAL \pixel_counter|y_reg[2]~14_combout\ : std_logic;
SIGNAL \pixel_counter|y_reg[3]~16_combout\ : std_logic;
SIGNAL \pixel_counter|x_next[3]~8_combout\ : std_logic;
SIGNAL \pixel_counter|Add0~0_combout\ : std_logic;
SIGNAL \pixel_counter|x_next[0]~1_combout\ : std_logic;
SIGNAL \pixel_counter|Add0~1\ : std_logic;
SIGNAL \pixel_counter|Add0~3\ : std_logic;
SIGNAL \pixel_counter|Add0~4_combout\ : std_logic;
SIGNAL \pixel_counter|x_next[2]~9_combout\ : std_logic;
SIGNAL \pixel_counter|Add0~5\ : std_logic;
SIGNAL \pixel_counter|Add0~7\ : std_logic;
SIGNAL \pixel_counter|Add0~8_combout\ : std_logic;
SIGNAL \pixel_counter|x_next[4]~7_combout\ : std_logic;
SIGNAL \pixel_counter|Equal0~0_combout\ : std_logic;
SIGNAL \pixel_counter|LessThan0~1_combout\ : std_logic;
SIGNAL \pixel_counter|x_next[6]~6_combout\ : std_logic;
SIGNAL \pixel_counter|x_next[5]~2_combout\ : std_logic;
SIGNAL \pixel_counter|Add0~9\ : std_logic;
SIGNAL \pixel_counter|Add0~11\ : std_logic;
SIGNAL \pixel_counter|Add0~13\ : std_logic;
SIGNAL \pixel_counter|Add0~14_combout\ : std_logic;
SIGNAL \pixel_counter|x_next[7]~5_combout\ : std_logic;
SIGNAL \pixel_counter|Add0~15\ : std_logic;
SIGNAL \pixel_counter|Add0~16_combout\ : std_logic;
SIGNAL \pixel_counter|x_next[8]~4_combout\ : std_logic;
SIGNAL \pixel_counter|Add0~17\ : std_logic;
SIGNAL \pixel_counter|Add0~18_combout\ : std_logic;
SIGNAL \pixel_counter|x_next[9]~3_combout\ : std_logic;
SIGNAL \pixel_counter|Equal0~1_combout\ : std_logic;
SIGNAL \pixel_counter|Add0~2_combout\ : std_logic;
SIGNAL \pixel_counter|x_next[1]~0_combout\ : std_logic;
SIGNAL \pixel_counter|Equal0~2_combout\ : std_logic;
SIGNAL \pixel_counter|Equal0~3_combout\ : std_logic;
SIGNAL \pixel_counter|LessThan1~0_combout\ : std_logic;
SIGNAL \pixel_counter|y_reg[7]~24_combout\ : std_logic;
SIGNAL \pixel_counter|LessThan1~1_combout\ : std_logic;
SIGNAL \pixel_counter|y_reg[8]~27\ : std_logic;
SIGNAL \pixel_counter|y_reg[9]~28_combout\ : std_logic;
SIGNAL \pixel_counter|LessThan1~2_combout\ : std_logic;
SIGNAL \pixel_counter|y_reg[2]~15\ : std_logic;
SIGNAL \pixel_counter|y_reg[3]~17\ : std_logic;
SIGNAL \pixel_counter|y_reg[4]~18_combout\ : std_logic;
SIGNAL \pixel_counter|y_reg[4]~19\ : std_logic;
SIGNAL \pixel_counter|y_reg[5]~21\ : std_logic;
SIGNAL \pixel_counter|y_reg[6]~22_combout\ : std_logic;
SIGNAL \pixel_counter|y_reg[6]~23\ : std_logic;
SIGNAL \pixel_counter|y_reg[7]~25\ : std_logic;
SIGNAL \pixel_counter|y_reg[8]~26_combout\ : std_logic;
SIGNAL \from640x480to160x120|Add0~1\ : std_logic;
SIGNAL \from640x480to160x120|Add0~3\ : std_logic;
SIGNAL \from640x480to160x120|Add0~5\ : std_logic;
SIGNAL \from640x480to160x120|Add0~7\ : std_logic;
SIGNAL \from640x480to160x120|Add0~9\ : std_logic;
SIGNAL \from640x480to160x120|Add0~11\ : std_logic;
SIGNAL \from640x480to160x120|Add0~13\ : std_logic;
SIGNAL \from640x480to160x120|Add0~14_combout\ : std_logic;
SIGNAL \from640x480to160x120|Add0~6_combout\ : std_logic;
SIGNAL \from640x480to160x120|Add0~4_combout\ : std_logic;
SIGNAL \pixel_counter|Add0~12_combout\ : std_logic;
SIGNAL \pixel_counter|Add0~10_combout\ : std_logic;
SIGNAL \pixel_counter|Add0~6_combout\ : std_logic;
SIGNAL \pixel_counter|Add2~1_cout\ : std_logic;
SIGNAL \pixel_counter|Add2~3\ : std_logic;
SIGNAL \pixel_counter|Add2~5\ : std_logic;
SIGNAL \pixel_counter|Add2~7\ : std_logic;
SIGNAL \pixel_counter|Add2~9\ : std_logic;
SIGNAL \pixel_counter|Add2~11\ : std_logic;
SIGNAL \pixel_counter|Add2~13\ : std_logic;
SIGNAL \pixel_counter|Add2~15\ : std_logic;
SIGNAL \pixel_counter|Add2~16_combout\ : std_logic;
SIGNAL \pixel_counter|Add2~14_combout\ : std_logic;
SIGNAL \pixel_counter|Add2~12_combout\ : std_logic;
SIGNAL \from640x480to160x120|adr[5]~1\ : std_logic;
SIGNAL \from640x480to160x120|adr[6]~3\ : std_logic;
SIGNAL \from640x480to160x120|adr[7]~5\ : std_logic;
SIGNAL \from640x480to160x120|adr[8]~7\ : std_logic;
SIGNAL \from640x480to160x120|adr[9]~9\ : std_logic;
SIGNAL \from640x480to160x120|adr[10]~11\ : std_logic;
SIGNAL \from640x480to160x120|adr[11]~13\ : std_logic;
SIGNAL \from640x480to160x120|adr[12]~15\ : std_logic;
SIGNAL \from640x480to160x120|adr[13]~17\ : std_logic;
SIGNAL \from640x480to160x120|adr[14]~18_combout\ : std_logic;
SIGNAL \from640x480to160x120|adr[13]~16_combout\ : std_logic;
SIGNAL \from640x480to160x120|adr[12]~14_combout\ : std_logic;
SIGNAL \pixel_counter|Add2~2_combout\ : std_logic;
SIGNAL \pixel_counter|Add2~4_combout\ : std_logic;
SIGNAL \pixel_counter|Add2~6_combout\ : std_logic;
SIGNAL \pixel_counter|Add2~8_combout\ : std_logic;
SIGNAL \pixel_counter|Add2~10_combout\ : std_logic;
SIGNAL \from640x480to160x120|adr[5]~0_combout\ : std_logic;
SIGNAL \from640x480to160x120|adr[6]~2_combout\ : std_logic;
SIGNAL \from640x480to160x120|adr[7]~4_combout\ : std_logic;
SIGNAL \from640x480to160x120|adr[8]~6_combout\ : std_logic;
SIGNAL \from640x480to160x120|adr[9]~8_combout\ : std_logic;
SIGNAL \from640x480to160x120|adr[10]~10_combout\ : std_logic;
SIGNAL \from640x480to160x120|adr[11]~12_combout\ : std_logic;
SIGNAL \clear_mux|LPM_MUX_component|auto_generated|result_node[15]~3_combout\ : std_logic;
SIGNAL \clear_mux|LPM_MUX_component|auto_generated|result_node[0]~4_combout\ : std_logic;
SIGNAL \clear_mux|LPM_MUX_component|auto_generated|result_node[1]~5_combout\ : std_logic;
SIGNAL \clear_mux|LPM_MUX_component|auto_generated|result_node[2]~6_combout\ : std_logic;
SIGNAL \clear_screen|adr[3]~21_combout\ : std_logic;
SIGNAL \clear_mux|LPM_MUX_component|auto_generated|result_node[3]~7_combout\ : std_logic;
SIGNAL \clear_mux|LPM_MUX_component|auto_generated|result_node[4]~8_combout\ : std_logic;
SIGNAL \clear_screen|adr[5]~25_combout\ : std_logic;
SIGNAL \clear_mux|LPM_MUX_component|auto_generated|result_node[5]~9_combout\ : std_logic;
SIGNAL \clear_mux|LPM_MUX_component|auto_generated|result_node[6]~10_combout\ : std_logic;
SIGNAL \clear_mux|LPM_MUX_component|auto_generated|result_node[7]~11_combout\ : std_logic;
SIGNAL \clear_screen|adr[8]~31_combout\ : std_logic;
SIGNAL \clear_mux|LPM_MUX_component|auto_generated|result_node[8]~12_combout\ : std_logic;
SIGNAL \clear_mux|LPM_MUX_component|auto_generated|result_node[9]~13_combout\ : std_logic;
SIGNAL \clear_mux|LPM_MUX_component|auto_generated|result_node[10]~14_combout\ : std_logic;
SIGNAL \clear_mux|LPM_MUX_component|auto_generated|result_node[11]~15_combout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a0~portadataout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a[0]~feeder_combout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs407w[0]~0_combout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~2_combout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a12~portadataout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs407w[0]~1_combout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|decode4|w_anode344w[3]~0_combout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode344w[3]~0_combout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a16~portadataout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|mux5|muxlut_result0w~0_combout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~1_combout\ : std_logic;
SIGNAL \clear_mux|LPM_MUX_component|auto_generated|result_node[16]~16_combout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a5~portadataout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~0_combout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a9~portadataout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs455w[0]~0_combout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs455w[0]~1_combout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a17~portadataout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|mux5|muxlut_result1w~0_combout\ : std_logic;
SIGNAL \clear_mux|LPM_MUX_component|auto_generated|result_node[17]~17_combout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a2~portadataout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs503w[0]~0_combout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a14~portadataout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs503w[0]~1_combout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a18~portadataout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|mux5|muxlut_result2w~0_combout\ : std_logic;
SIGNAL \clear_mux|LPM_MUX_component|auto_generated|result_node[18]~18_combout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a3~portadataout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs551w[0]~0_combout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a15~portadataout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs551w[0]~1_combout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a19~portadataout\ : std_logic;
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|mux5|muxlut_result3w~0_combout\ : std_logic;
SIGNAL \pixel_counter|y_reg[5]~20_combout\ : std_logic;
SIGNAL \pixel_counter|Equal1~0_combout\ : std_logic;
SIGNAL \pixel_counter|process_2~4_combout\ : std_logic;
SIGNAL \pixel_counter|process_2~5_combout\ : std_logic;
SIGNAL \pixel_counter|blanking~regout\ : std_logic;
SIGNAL \blanking_pipe[1]~feeder_combout\ : std_logic;
SIGNAL \color_rom|r~0_combout\ : std_logic;
SIGNAL \color_rom|r~1_combout\ : std_logic;
SIGNAL \color_rom|r~2_combout\ : std_logic;
SIGNAL \color_rom|r~3_combout\ : std_logic;
SIGNAL \color_rom|g~0_combout\ : std_logic;
SIGNAL \color_rom|g~1_combout\ : std_logic;
SIGNAL \color_rom|g~2_combout\ : std_logic;
SIGNAL \color_rom|g~3_combout\ : std_logic;
SIGNAL \color_rom|b~0_combout\ : std_logic;
SIGNAL \color_rom|b~1_combout\ : std_logic;
SIGNAL \color_rom|b~2_combout\ : std_logic;
SIGNAL \color_rom|b~3_combout\ : std_logic;
SIGNAL \pixel_counter|LessThan5~0_combout\ : std_logic;
SIGNAL \pixel_counter|LessThan5~3_combout\ : std_logic;
SIGNAL \pixel_counter|h_sync~regout\ : std_logic;
SIGNAL \pixel_counter|Equal1~1_combout\ : std_logic;
SIGNAL \pixel_counter|Equal1~2_combout\ : std_logic;
SIGNAL \pixel_counter|v_sync~regout\ : std_logic;
SIGNAL \v_sync_pipe[1]~feeder_combout\ : std_logic;
SIGNAL \v_sync_pipe[2]~feeder_combout\ : std_logic;
SIGNAL \color_rom|r\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \pixel_counter|y_reg\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \clear_screen|adr\ : std_logic_vector(14 DOWNTO 0);
SIGNAL \color_rom|palette_rom|altsyncram_component|auto_generated|q_a\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \color_rom|b\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \color~combout\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \color_rom|g\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \pixel_counter|x_reg\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \adr~combout\ : std_logic_vector(14 DOWNTO 0);
SIGNAL \clear_mux|LPM_MUX_component|auto_generated|result_node\ : std_logic_vector(19 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|decode4|w_anode297w\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode334w\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode324w\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode314w\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode297w\ : std_logic_vector(3 DOWNTO 0);
SIGNAL v_sync_pipe : std_logic_vector(3 DOWNTO 0);
SIGNAL h_sync_pipe : std_logic_vector(3 DOWNTO 0);
SIGNAL blanking_pipe : std_logic_vector(3 DOWNTO 0);
SIGNAL \main_pll|altpll_component|ALT_INV__clk1~clkctrl_outclk\ : std_logic;
SIGNAL ALT_INV_v_sync_pipe : std_logic_vector(2 DOWNTO 2);
SIGNAL ALT_INV_h_sync_pipe : std_logic_vector(2 DOWNTO 2);

BEGIN

ww_clk <= clk;
ww_adr <= adr;
ww_color <= color;
ww_wr <= wr;
ww_clear <= clear;
r <= ww_r;
g <= ww_g;
b <= ww_b;
h_sync <= ww_h_sync;
v_sync <= ww_v_sync;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\color_rom|palette_rom|altsyncram_component|auto_generated|ram_block1a0_PORTAADDR_bus\ <= (\vram|altsyncram_component|auto_generated|altsyncram1|mux5|muxlut_result3w~0_combout\ & 
\vram|altsyncram_component|auto_generated|altsyncram1|mux5|muxlut_result2w~0_combout\ & \vram|altsyncram_component|auto_generated|altsyncram1|mux5|muxlut_result1w~0_combout\ & 
\vram|altsyncram_component|auto_generated|altsyncram1|mux5|muxlut_result0w~0_combout\);

\color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(0) <= \color_rom|palette_rom|altsyncram_component|auto_generated|ram_block1a0_PORTADATAOUT_bus\(0);
\color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(1) <= \color_rom|palette_rom|altsyncram_component|auto_generated|ram_block1a0_PORTADATAOUT_bus\(1);
\color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(2) <= \color_rom|palette_rom|altsyncram_component|auto_generated|ram_block1a0_PORTADATAOUT_bus\(2);
\color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(3) <= \color_rom|palette_rom|altsyncram_component|auto_generated|ram_block1a0_PORTADATAOUT_bus\(3);
\color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(4) <= \color_rom|palette_rom|altsyncram_component|auto_generated|ram_block1a0_PORTADATAOUT_bus\(4);
\color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(5) <= \color_rom|palette_rom|altsyncram_component|auto_generated|ram_block1a0_PORTADATAOUT_bus\(5);
\color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(6) <= \color_rom|palette_rom|altsyncram_component|auto_generated|ram_block1a0_PORTADATAOUT_bus\(6);
\color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(7) <= \color_rom|palette_rom|altsyncram_component|auto_generated|ram_block1a0_PORTADATAOUT_bus\(7);
\color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(8) <= \color_rom|palette_rom|altsyncram_component|auto_generated|ram_block1a0_PORTADATAOUT_bus\(8);
\color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(9) <= \color_rom|palette_rom|altsyncram_component|auto_generated|ram_block1a0_PORTADATAOUT_bus\(9);
\color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(10) <= \color_rom|palette_rom|altsyncram_component|auto_generated|ram_block1a0_PORTADATAOUT_bus\(10);
\color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(11) <= \color_rom|palette_rom|altsyncram_component|auto_generated|ram_block1a0_PORTADATAOUT_bus\(11);

\main_pll|altpll_component|pll_INCLK_bus\ <= (gnd & \clk~combout\);

\main_pll|altpll_component|_clk0\ <= \main_pll|altpll_component|pll_CLK_bus\(0);
\main_pll|altpll_component|_clk1\ <= \main_pll|altpll_component|pll_CLK_bus\(1);
\main_pll|altpll_component|pll~CLK2\ <= \main_pll|altpll_component|pll_CLK_bus\(2);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a16_PORTADATAIN_bus\(0) <= vcc;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a16_PORTBDATAIN_bus\(0) <= \clear_mux|LPM_MUX_component|auto_generated|result_node[15]~3_combout\;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a16_PORTAADDR_bus\ <= (\from640x480to160x120|adr[11]~12_combout\ & \from640x480to160x120|adr[10]~10_combout\ & \from640x480to160x120|adr[9]~8_combout\ & 
\from640x480to160x120|adr[8]~6_combout\ & \from640x480to160x120|adr[7]~4_combout\ & \from640x480to160x120|adr[6]~2_combout\ & \from640x480to160x120|adr[5]~0_combout\ & \pixel_counter|Add2~10_combout\ & \pixel_counter|Add2~8_combout\ & 
\pixel_counter|Add2~6_combout\ & \pixel_counter|Add2~4_combout\ & \pixel_counter|Add2~2_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a16_PORTBADDR_bus\ <= (\clear_mux|LPM_MUX_component|auto_generated|result_node[11]~15_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[10]~14_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[9]~13_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[8]~12_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[7]~11_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[6]~10_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[5]~9_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[4]~8_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[3]~7_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[2]~6_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[1]~5_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[0]~4_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a16~portadataout\ <= \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a16_PORTADATAOUT_bus\(0);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a8_PORTADATAIN_bus\(0) <= vcc;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a8_PORTBDATAIN_bus\(0) <= \clear_mux|LPM_MUX_component|auto_generated|result_node[15]~3_combout\;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a8_PORTAADDR_bus\ <= (\from640x480to160x120|adr[11]~12_combout\ & \from640x480to160x120|adr[10]~10_combout\ & \from640x480to160x120|adr[9]~8_combout\ & \from640x480to160x120|adr[8]~6_combout\
& \from640x480to160x120|adr[7]~4_combout\ & \from640x480to160x120|adr[6]~2_combout\ & \from640x480to160x120|adr[5]~0_combout\ & \pixel_counter|Add2~10_combout\ & \pixel_counter|Add2~8_combout\ & \pixel_counter|Add2~6_combout\ & 
\pixel_counter|Add2~4_combout\ & \pixel_counter|Add2~2_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a8_PORTBADDR_bus\ <= (\clear_mux|LPM_MUX_component|auto_generated|result_node[11]~15_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[10]~14_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[9]~13_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[8]~12_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[7]~11_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[6]~10_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[5]~9_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[4]~8_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[3]~7_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[2]~6_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[1]~5_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[0]~4_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a8~portadataout\ <= \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a8_PORTADATAOUT_bus\(0);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a4_PORTADATAIN_bus\(0) <= vcc;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a4_PORTBDATAIN_bus\(0) <= \clear_mux|LPM_MUX_component|auto_generated|result_node[15]~3_combout\;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a4_PORTAADDR_bus\ <= (\from640x480to160x120|adr[11]~12_combout\ & \from640x480to160x120|adr[10]~10_combout\ & \from640x480to160x120|adr[9]~8_combout\ & \from640x480to160x120|adr[8]~6_combout\
& \from640x480to160x120|adr[7]~4_combout\ & \from640x480to160x120|adr[6]~2_combout\ & \from640x480to160x120|adr[5]~0_combout\ & \pixel_counter|Add2~10_combout\ & \pixel_counter|Add2~8_combout\ & \pixel_counter|Add2~6_combout\ & 
\pixel_counter|Add2~4_combout\ & \pixel_counter|Add2~2_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a4_PORTBADDR_bus\ <= (\clear_mux|LPM_MUX_component|auto_generated|result_node[11]~15_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[10]~14_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[9]~13_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[8]~12_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[7]~11_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[6]~10_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[5]~9_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[4]~8_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[3]~7_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[2]~6_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[1]~5_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[0]~4_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a4~portadataout\ <= \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a4_PORTADATAOUT_bus\(0);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a0_PORTADATAIN_bus\(0) <= vcc;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a0_PORTBDATAIN_bus\(0) <= \clear_mux|LPM_MUX_component|auto_generated|result_node[15]~3_combout\;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a0_PORTAADDR_bus\ <= (\from640x480to160x120|adr[11]~12_combout\ & \from640x480to160x120|adr[10]~10_combout\ & \from640x480to160x120|adr[9]~8_combout\ & \from640x480to160x120|adr[8]~6_combout\
& \from640x480to160x120|adr[7]~4_combout\ & \from640x480to160x120|adr[6]~2_combout\ & \from640x480to160x120|adr[5]~0_combout\ & \pixel_counter|Add2~10_combout\ & \pixel_counter|Add2~8_combout\ & \pixel_counter|Add2~6_combout\ & 
\pixel_counter|Add2~4_combout\ & \pixel_counter|Add2~2_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a0_PORTBADDR_bus\ <= (\clear_mux|LPM_MUX_component|auto_generated|result_node[11]~15_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[10]~14_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[9]~13_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[8]~12_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[7]~11_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[6]~10_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[5]~9_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[4]~8_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[3]~7_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[2]~6_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[1]~5_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[0]~4_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a0~portadataout\ <= \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a0_PORTADATAOUT_bus\(0);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a12_PORTADATAIN_bus\(0) <= vcc;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a12_PORTBDATAIN_bus\(0) <= \clear_mux|LPM_MUX_component|auto_generated|result_node[15]~3_combout\;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a12_PORTAADDR_bus\ <= (\from640x480to160x120|adr[11]~12_combout\ & \from640x480to160x120|adr[10]~10_combout\ & \from640x480to160x120|adr[9]~8_combout\ & 
\from640x480to160x120|adr[8]~6_combout\ & \from640x480to160x120|adr[7]~4_combout\ & \from640x480to160x120|adr[6]~2_combout\ & \from640x480to160x120|adr[5]~0_combout\ & \pixel_counter|Add2~10_combout\ & \pixel_counter|Add2~8_combout\ & 
\pixel_counter|Add2~6_combout\ & \pixel_counter|Add2~4_combout\ & \pixel_counter|Add2~2_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a12_PORTBADDR_bus\ <= (\clear_mux|LPM_MUX_component|auto_generated|result_node[11]~15_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[10]~14_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[9]~13_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[8]~12_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[7]~11_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[6]~10_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[5]~9_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[4]~8_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[3]~7_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[2]~6_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[1]~5_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[0]~4_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a12~portadataout\ <= \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a12_PORTADATAOUT_bus\(0);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a17_PORTADATAIN_bus\(0) <= vcc;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a17_PORTBDATAIN_bus\(0) <= \clear_mux|LPM_MUX_component|auto_generated|result_node[16]~16_combout\;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a17_PORTAADDR_bus\ <= (\from640x480to160x120|adr[11]~12_combout\ & \from640x480to160x120|adr[10]~10_combout\ & \from640x480to160x120|adr[9]~8_combout\ & 
\from640x480to160x120|adr[8]~6_combout\ & \from640x480to160x120|adr[7]~4_combout\ & \from640x480to160x120|adr[6]~2_combout\ & \from640x480to160x120|adr[5]~0_combout\ & \pixel_counter|Add2~10_combout\ & \pixel_counter|Add2~8_combout\ & 
\pixel_counter|Add2~6_combout\ & \pixel_counter|Add2~4_combout\ & \pixel_counter|Add2~2_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a17_PORTBADDR_bus\ <= (\clear_mux|LPM_MUX_component|auto_generated|result_node[11]~15_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[10]~14_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[9]~13_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[8]~12_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[7]~11_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[6]~10_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[5]~9_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[4]~8_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[3]~7_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[2]~6_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[1]~5_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[0]~4_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a17~portadataout\ <= \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a17_PORTADATAOUT_bus\(0);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a5_PORTADATAIN_bus\(0) <= vcc;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a5_PORTBDATAIN_bus\(0) <= \clear_mux|LPM_MUX_component|auto_generated|result_node[16]~16_combout\;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a5_PORTAADDR_bus\ <= (\from640x480to160x120|adr[11]~12_combout\ & \from640x480to160x120|adr[10]~10_combout\ & \from640x480to160x120|adr[9]~8_combout\ & \from640x480to160x120|adr[8]~6_combout\
& \from640x480to160x120|adr[7]~4_combout\ & \from640x480to160x120|adr[6]~2_combout\ & \from640x480to160x120|adr[5]~0_combout\ & \pixel_counter|Add2~10_combout\ & \pixel_counter|Add2~8_combout\ & \pixel_counter|Add2~6_combout\ & 
\pixel_counter|Add2~4_combout\ & \pixel_counter|Add2~2_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a5_PORTBADDR_bus\ <= (\clear_mux|LPM_MUX_component|auto_generated|result_node[11]~15_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[10]~14_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[9]~13_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[8]~12_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[7]~11_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[6]~10_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[5]~9_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[4]~8_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[3]~7_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[2]~6_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[1]~5_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[0]~4_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a5~portadataout\ <= \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a5_PORTADATAOUT_bus\(0);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a9_PORTADATAIN_bus\(0) <= vcc;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a9_PORTBDATAIN_bus\(0) <= \clear_mux|LPM_MUX_component|auto_generated|result_node[16]~16_combout\;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a9_PORTAADDR_bus\ <= (\from640x480to160x120|adr[11]~12_combout\ & \from640x480to160x120|adr[10]~10_combout\ & \from640x480to160x120|adr[9]~8_combout\ & \from640x480to160x120|adr[8]~6_combout\
& \from640x480to160x120|adr[7]~4_combout\ & \from640x480to160x120|adr[6]~2_combout\ & \from640x480to160x120|adr[5]~0_combout\ & \pixel_counter|Add2~10_combout\ & \pixel_counter|Add2~8_combout\ & \pixel_counter|Add2~6_combout\ & 
\pixel_counter|Add2~4_combout\ & \pixel_counter|Add2~2_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a9_PORTBADDR_bus\ <= (\clear_mux|LPM_MUX_component|auto_generated|result_node[11]~15_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[10]~14_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[9]~13_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[8]~12_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[7]~11_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[6]~10_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[5]~9_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[4]~8_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[3]~7_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[2]~6_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[1]~5_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[0]~4_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a9~portadataout\ <= \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a9_PORTADATAOUT_bus\(0);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a1_PORTADATAIN_bus\(0) <= vcc;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a1_PORTBDATAIN_bus\(0) <= \clear_mux|LPM_MUX_component|auto_generated|result_node[16]~16_combout\;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a1_PORTAADDR_bus\ <= (\from640x480to160x120|adr[11]~12_combout\ & \from640x480to160x120|adr[10]~10_combout\ & \from640x480to160x120|adr[9]~8_combout\ & \from640x480to160x120|adr[8]~6_combout\
& \from640x480to160x120|adr[7]~4_combout\ & \from640x480to160x120|adr[6]~2_combout\ & \from640x480to160x120|adr[5]~0_combout\ & \pixel_counter|Add2~10_combout\ & \pixel_counter|Add2~8_combout\ & \pixel_counter|Add2~6_combout\ & 
\pixel_counter|Add2~4_combout\ & \pixel_counter|Add2~2_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a1_PORTBADDR_bus\ <= (\clear_mux|LPM_MUX_component|auto_generated|result_node[11]~15_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[10]~14_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[9]~13_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[8]~12_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[7]~11_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[6]~10_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[5]~9_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[4]~8_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[3]~7_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[2]~6_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[1]~5_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[0]~4_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a1~portadataout\ <= \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a1_PORTADATAOUT_bus\(0);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a13_PORTADATAIN_bus\(0) <= vcc;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a13_PORTBDATAIN_bus\(0) <= \clear_mux|LPM_MUX_component|auto_generated|result_node[16]~16_combout\;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a13_PORTAADDR_bus\ <= (\from640x480to160x120|adr[11]~12_combout\ & \from640x480to160x120|adr[10]~10_combout\ & \from640x480to160x120|adr[9]~8_combout\ & 
\from640x480to160x120|adr[8]~6_combout\ & \from640x480to160x120|adr[7]~4_combout\ & \from640x480to160x120|adr[6]~2_combout\ & \from640x480to160x120|adr[5]~0_combout\ & \pixel_counter|Add2~10_combout\ & \pixel_counter|Add2~8_combout\ & 
\pixel_counter|Add2~6_combout\ & \pixel_counter|Add2~4_combout\ & \pixel_counter|Add2~2_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a13_PORTBADDR_bus\ <= (\clear_mux|LPM_MUX_component|auto_generated|result_node[11]~15_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[10]~14_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[9]~13_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[8]~12_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[7]~11_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[6]~10_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[5]~9_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[4]~8_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[3]~7_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[2]~6_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[1]~5_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[0]~4_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a13~portadataout\ <= \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a13_PORTADATAOUT_bus\(0);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a18_PORTADATAIN_bus\(0) <= vcc;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a18_PORTBDATAIN_bus\(0) <= \clear_mux|LPM_MUX_component|auto_generated|result_node[17]~17_combout\;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a18_PORTAADDR_bus\ <= (\from640x480to160x120|adr[11]~12_combout\ & \from640x480to160x120|adr[10]~10_combout\ & \from640x480to160x120|adr[9]~8_combout\ & 
\from640x480to160x120|adr[8]~6_combout\ & \from640x480to160x120|adr[7]~4_combout\ & \from640x480to160x120|adr[6]~2_combout\ & \from640x480to160x120|adr[5]~0_combout\ & \pixel_counter|Add2~10_combout\ & \pixel_counter|Add2~8_combout\ & 
\pixel_counter|Add2~6_combout\ & \pixel_counter|Add2~4_combout\ & \pixel_counter|Add2~2_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a18_PORTBADDR_bus\ <= (\clear_mux|LPM_MUX_component|auto_generated|result_node[11]~15_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[10]~14_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[9]~13_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[8]~12_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[7]~11_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[6]~10_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[5]~9_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[4]~8_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[3]~7_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[2]~6_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[1]~5_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[0]~4_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a18~portadataout\ <= \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a18_PORTADATAOUT_bus\(0);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a10_PORTADATAIN_bus\(0) <= vcc;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a10_PORTBDATAIN_bus\(0) <= \clear_mux|LPM_MUX_component|auto_generated|result_node[17]~17_combout\;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a10_PORTAADDR_bus\ <= (\from640x480to160x120|adr[11]~12_combout\ & \from640x480to160x120|adr[10]~10_combout\ & \from640x480to160x120|adr[9]~8_combout\ & 
\from640x480to160x120|adr[8]~6_combout\ & \from640x480to160x120|adr[7]~4_combout\ & \from640x480to160x120|adr[6]~2_combout\ & \from640x480to160x120|adr[5]~0_combout\ & \pixel_counter|Add2~10_combout\ & \pixel_counter|Add2~8_combout\ & 
\pixel_counter|Add2~6_combout\ & \pixel_counter|Add2~4_combout\ & \pixel_counter|Add2~2_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a10_PORTBADDR_bus\ <= (\clear_mux|LPM_MUX_component|auto_generated|result_node[11]~15_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[10]~14_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[9]~13_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[8]~12_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[7]~11_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[6]~10_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[5]~9_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[4]~8_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[3]~7_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[2]~6_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[1]~5_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[0]~4_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a10~portadataout\ <= \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a10_PORTADATAOUT_bus\(0);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a6_PORTADATAIN_bus\(0) <= vcc;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a6_PORTBDATAIN_bus\(0) <= \clear_mux|LPM_MUX_component|auto_generated|result_node[17]~17_combout\;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a6_PORTAADDR_bus\ <= (\from640x480to160x120|adr[11]~12_combout\ & \from640x480to160x120|adr[10]~10_combout\ & \from640x480to160x120|adr[9]~8_combout\ & \from640x480to160x120|adr[8]~6_combout\
& \from640x480to160x120|adr[7]~4_combout\ & \from640x480to160x120|adr[6]~2_combout\ & \from640x480to160x120|adr[5]~0_combout\ & \pixel_counter|Add2~10_combout\ & \pixel_counter|Add2~8_combout\ & \pixel_counter|Add2~6_combout\ & 
\pixel_counter|Add2~4_combout\ & \pixel_counter|Add2~2_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a6_PORTBADDR_bus\ <= (\clear_mux|LPM_MUX_component|auto_generated|result_node[11]~15_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[10]~14_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[9]~13_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[8]~12_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[7]~11_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[6]~10_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[5]~9_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[4]~8_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[3]~7_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[2]~6_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[1]~5_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[0]~4_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a6~portadataout\ <= \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a6_PORTADATAOUT_bus\(0);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a2_PORTADATAIN_bus\(0) <= vcc;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a2_PORTBDATAIN_bus\(0) <= \clear_mux|LPM_MUX_component|auto_generated|result_node[17]~17_combout\;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a2_PORTAADDR_bus\ <= (\from640x480to160x120|adr[11]~12_combout\ & \from640x480to160x120|adr[10]~10_combout\ & \from640x480to160x120|adr[9]~8_combout\ & \from640x480to160x120|adr[8]~6_combout\
& \from640x480to160x120|adr[7]~4_combout\ & \from640x480to160x120|adr[6]~2_combout\ & \from640x480to160x120|adr[5]~0_combout\ & \pixel_counter|Add2~10_combout\ & \pixel_counter|Add2~8_combout\ & \pixel_counter|Add2~6_combout\ & 
\pixel_counter|Add2~4_combout\ & \pixel_counter|Add2~2_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a2_PORTBADDR_bus\ <= (\clear_mux|LPM_MUX_component|auto_generated|result_node[11]~15_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[10]~14_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[9]~13_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[8]~12_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[7]~11_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[6]~10_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[5]~9_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[4]~8_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[3]~7_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[2]~6_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[1]~5_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[0]~4_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a2~portadataout\ <= \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a2_PORTADATAOUT_bus\(0);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a14_PORTADATAIN_bus\(0) <= vcc;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a14_PORTBDATAIN_bus\(0) <= \clear_mux|LPM_MUX_component|auto_generated|result_node[17]~17_combout\;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a14_PORTAADDR_bus\ <= (\from640x480to160x120|adr[11]~12_combout\ & \from640x480to160x120|adr[10]~10_combout\ & \from640x480to160x120|adr[9]~8_combout\ & 
\from640x480to160x120|adr[8]~6_combout\ & \from640x480to160x120|adr[7]~4_combout\ & \from640x480to160x120|adr[6]~2_combout\ & \from640x480to160x120|adr[5]~0_combout\ & \pixel_counter|Add2~10_combout\ & \pixel_counter|Add2~8_combout\ & 
\pixel_counter|Add2~6_combout\ & \pixel_counter|Add2~4_combout\ & \pixel_counter|Add2~2_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a14_PORTBADDR_bus\ <= (\clear_mux|LPM_MUX_component|auto_generated|result_node[11]~15_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[10]~14_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[9]~13_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[8]~12_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[7]~11_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[6]~10_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[5]~9_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[4]~8_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[3]~7_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[2]~6_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[1]~5_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[0]~4_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a14~portadataout\ <= \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a14_PORTADATAOUT_bus\(0);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a19_PORTADATAIN_bus\(0) <= vcc;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a19_PORTBDATAIN_bus\(0) <= \clear_mux|LPM_MUX_component|auto_generated|result_node[18]~18_combout\;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a19_PORTAADDR_bus\ <= (\from640x480to160x120|adr[11]~12_combout\ & \from640x480to160x120|adr[10]~10_combout\ & \from640x480to160x120|adr[9]~8_combout\ & 
\from640x480to160x120|adr[8]~6_combout\ & \from640x480to160x120|adr[7]~4_combout\ & \from640x480to160x120|adr[6]~2_combout\ & \from640x480to160x120|adr[5]~0_combout\ & \pixel_counter|Add2~10_combout\ & \pixel_counter|Add2~8_combout\ & 
\pixel_counter|Add2~6_combout\ & \pixel_counter|Add2~4_combout\ & \pixel_counter|Add2~2_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a19_PORTBADDR_bus\ <= (\clear_mux|LPM_MUX_component|auto_generated|result_node[11]~15_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[10]~14_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[9]~13_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[8]~12_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[7]~11_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[6]~10_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[5]~9_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[4]~8_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[3]~7_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[2]~6_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[1]~5_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[0]~4_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a19~portadataout\ <= \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a19_PORTADATAOUT_bus\(0);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a7_PORTADATAIN_bus\(0) <= vcc;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a7_PORTBDATAIN_bus\(0) <= \clear_mux|LPM_MUX_component|auto_generated|result_node[18]~18_combout\;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a7_PORTAADDR_bus\ <= (\from640x480to160x120|adr[11]~12_combout\ & \from640x480to160x120|adr[10]~10_combout\ & \from640x480to160x120|adr[9]~8_combout\ & \from640x480to160x120|adr[8]~6_combout\
& \from640x480to160x120|adr[7]~4_combout\ & \from640x480to160x120|adr[6]~2_combout\ & \from640x480to160x120|adr[5]~0_combout\ & \pixel_counter|Add2~10_combout\ & \pixel_counter|Add2~8_combout\ & \pixel_counter|Add2~6_combout\ & 
\pixel_counter|Add2~4_combout\ & \pixel_counter|Add2~2_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a7_PORTBADDR_bus\ <= (\clear_mux|LPM_MUX_component|auto_generated|result_node[11]~15_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[10]~14_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[9]~13_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[8]~12_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[7]~11_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[6]~10_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[5]~9_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[4]~8_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[3]~7_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[2]~6_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[1]~5_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[0]~4_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a7~portadataout\ <= \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a7_PORTADATAOUT_bus\(0);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a11_PORTADATAIN_bus\(0) <= vcc;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a11_PORTBDATAIN_bus\(0) <= \clear_mux|LPM_MUX_component|auto_generated|result_node[18]~18_combout\;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a11_PORTAADDR_bus\ <= (\from640x480to160x120|adr[11]~12_combout\ & \from640x480to160x120|adr[10]~10_combout\ & \from640x480to160x120|adr[9]~8_combout\ & 
\from640x480to160x120|adr[8]~6_combout\ & \from640x480to160x120|adr[7]~4_combout\ & \from640x480to160x120|adr[6]~2_combout\ & \from640x480to160x120|adr[5]~0_combout\ & \pixel_counter|Add2~10_combout\ & \pixel_counter|Add2~8_combout\ & 
\pixel_counter|Add2~6_combout\ & \pixel_counter|Add2~4_combout\ & \pixel_counter|Add2~2_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a11_PORTBADDR_bus\ <= (\clear_mux|LPM_MUX_component|auto_generated|result_node[11]~15_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[10]~14_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[9]~13_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[8]~12_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[7]~11_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[6]~10_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[5]~9_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[4]~8_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[3]~7_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[2]~6_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[1]~5_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[0]~4_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a11~portadataout\ <= \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a11_PORTADATAOUT_bus\(0);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a3_PORTADATAIN_bus\(0) <= vcc;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a3_PORTBDATAIN_bus\(0) <= \clear_mux|LPM_MUX_component|auto_generated|result_node[18]~18_combout\;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a3_PORTAADDR_bus\ <= (\from640x480to160x120|adr[11]~12_combout\ & \from640x480to160x120|adr[10]~10_combout\ & \from640x480to160x120|adr[9]~8_combout\ & \from640x480to160x120|adr[8]~6_combout\
& \from640x480to160x120|adr[7]~4_combout\ & \from640x480to160x120|adr[6]~2_combout\ & \from640x480to160x120|adr[5]~0_combout\ & \pixel_counter|Add2~10_combout\ & \pixel_counter|Add2~8_combout\ & \pixel_counter|Add2~6_combout\ & 
\pixel_counter|Add2~4_combout\ & \pixel_counter|Add2~2_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a3_PORTBADDR_bus\ <= (\clear_mux|LPM_MUX_component|auto_generated|result_node[11]~15_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[10]~14_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[9]~13_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[8]~12_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[7]~11_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[6]~10_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[5]~9_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[4]~8_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[3]~7_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[2]~6_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[1]~5_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[0]~4_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a3~portadataout\ <= \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a3_PORTADATAOUT_bus\(0);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a15_PORTADATAIN_bus\(0) <= vcc;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a15_PORTBDATAIN_bus\(0) <= \clear_mux|LPM_MUX_component|auto_generated|result_node[18]~18_combout\;

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a15_PORTAADDR_bus\ <= (\from640x480to160x120|adr[11]~12_combout\ & \from640x480to160x120|adr[10]~10_combout\ & \from640x480to160x120|adr[9]~8_combout\ & 
\from640x480to160x120|adr[8]~6_combout\ & \from640x480to160x120|adr[7]~4_combout\ & \from640x480to160x120|adr[6]~2_combout\ & \from640x480to160x120|adr[5]~0_combout\ & \pixel_counter|Add2~10_combout\ & \pixel_counter|Add2~8_combout\ & 
\pixel_counter|Add2~6_combout\ & \pixel_counter|Add2~4_combout\ & \pixel_counter|Add2~2_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a15_PORTBADDR_bus\ <= (\clear_mux|LPM_MUX_component|auto_generated|result_node[11]~15_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[10]~14_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[9]~13_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[8]~12_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[7]~11_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[6]~10_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[5]~9_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[4]~8_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[3]~7_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[2]~6_combout\ & \clear_mux|LPM_MUX_component|auto_generated|result_node[1]~5_combout\ & 
\clear_mux|LPM_MUX_component|auto_generated|result_node[0]~4_combout\);

\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a15~portadataout\ <= \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a15_PORTADATAOUT_bus\(0);

\main_pll|altpll_component|_clk0~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \main_pll|altpll_component|_clk0\);

\clear_mux|LPM_MUX_component|auto_generated|result_node[19]~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \clear_mux|LPM_MUX_component|auto_generated|result_node\(19));

\main_pll|altpll_component|_clk1~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \main_pll|altpll_component|_clk1\);
\main_pll|altpll_component|ALT_INV__clk1~clkctrl_outclk\ <= NOT \main_pll|altpll_component|_clk1~clkctrl_outclk\;
ALT_INV_v_sync_pipe(2) <= NOT v_sync_pipe(2);
ALT_INV_h_sync_pipe(2) <= NOT h_sync_pipe(2);

-- Location: M4K_X11_Y11
\color_rom|palette_rom|altsyncram_component|auto_generated|ram_block1a0\ : cycloneii_ram_block
-- pragma translate_off
GENERIC MAP (
	mem_init0 => X"000F0F0FFFF000F0F0F00FFF000F0F0FFFF000F0F0F00FFF",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	init_file => "color_table_rom_data.hex",
	init_file_layout => "port_a",
	logical_ram_name => "color_palette:color_rom|color_table:palette_rom|altsyncram:altsyncram_component|altsyncram_h791:auto_generated|ALTSYNCRAM",
	operation_mode => "rom",
	port_a_address_clear => "none",
	port_a_address_width => 4,
	port_a_byte_enable_clear => "none",
	port_a_byte_enable_clock => "none",
	port_a_data_in_clear => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 12,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 15,
	port_a_logical_ram_depth => 16,
	port_a_logical_ram_width => 12,
	port_a_write_enable_clear => "none",
	port_a_write_enable_clock => "none",
	port_b_address_width => 4,
	port_b_data_width => 12,
	ram_block_type => "M4K",
	safe_write => "err_on_2clk")
-- pragma translate_on
PORT MAP (
	clk0 => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	portaaddr => \color_rom|palette_rom|altsyncram_component|auto_generated|ram_block1a0_PORTAADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portadataout => \color_rom|palette_rom|altsyncram_component|auto_generated|ram_block1a0_PORTADATAOUT_bus\);

-- Location: M4K_X23_Y10
\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a8\ : cycloneii_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vram160x120x4:vram|altsyncram:altsyncram_component|altsyncram_r5o1:auto_generated|altsyncram_m6m1:altsyncram1|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "bidir_dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clear => "none",
	port_a_byte_enable_clock => "none",
	port_a_data_in_clear => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 4,
	port_a_write_enable_clear => "none",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_byte_enable_clear => "none",
	port_b_data_in_clear => "none",
	port_b_data_in_clock => "clock1",
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 4,
	port_b_read_enable_write_enable_clear => "none",
	port_b_read_enable_write_enable_clock => "clock1",
	ram_block_type => "M4K",
	safe_write => "err_on_2clk")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbrewe => \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~0_combout\,
	clk0 => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	clk1 => \clear_mux|LPM_MUX_component|auto_generated|result_node[19]~clkctrl_outclk\,
	ena0 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode324w\(3),
	ena1 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~0_combout\,
	portadatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a8_PORTADATAIN_bus\,
	portbdatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a8_PORTBDATAIN_bus\,
	portaaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a8_PORTAADDR_bus\,
	portbaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a8_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portadataout => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a8_PORTADATAOUT_bus\);

-- Location: M4K_X11_Y10
\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a4\ : cycloneii_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vram160x120x4:vram|altsyncram:altsyncram_component|altsyncram_r5o1:auto_generated|altsyncram_m6m1:altsyncram1|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "bidir_dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clear => "none",
	port_a_byte_enable_clock => "none",
	port_a_data_in_clear => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 4,
	port_a_write_enable_clear => "none",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_byte_enable_clear => "none",
	port_b_data_in_clear => "none",
	port_b_data_in_clock => "clock1",
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 4,
	port_b_read_enable_write_enable_clear => "none",
	port_b_read_enable_write_enable_clock => "clock1",
	ram_block_type => "M4K",
	safe_write => "err_on_2clk")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbrewe => \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~1_combout\,
	clk0 => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	clk1 => \clear_mux|LPM_MUX_component|auto_generated|result_node[19]~clkctrl_outclk\,
	ena0 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode314w\(3),
	ena1 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~1_combout\,
	portadatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a4_PORTADATAIN_bus\,
	portbdatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a4_PORTBDATAIN_bus\,
	portaaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a4_PORTAADDR_bus\,
	portbaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a4_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portadataout => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a4_PORTADATAOUT_bus\);

-- Location: M4K_X23_Y3
\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a1\ : cycloneii_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vram160x120x4:vram|altsyncram:altsyncram_component|altsyncram_r5o1:auto_generated|altsyncram_m6m1:altsyncram1|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "bidir_dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clear => "none",
	port_a_byte_enable_clock => "none",
	port_a_data_in_clear => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 1,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 4,
	port_a_write_enable_clear => "none",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_byte_enable_clear => "none",
	port_b_data_in_clear => "none",
	port_b_data_in_clock => "clock1",
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 1,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 4,
	port_b_read_enable_write_enable_clear => "none",
	port_b_read_enable_write_enable_clock => "clock1",
	ram_block_type => "M4K",
	safe_write => "err_on_2clk")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbrewe => \vram|altsyncram_component|auto_generated|altsyncram1|decode4|w_anode297w\(3),
	clk0 => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	clk1 => \clear_mux|LPM_MUX_component|auto_generated|result_node[19]~clkctrl_outclk\,
	ena0 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode297w\(3),
	ena1 => \vram|altsyncram_component|auto_generated|altsyncram1|decode4|w_anode297w\(3),
	portadatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a1_PORTADATAIN_bus\,
	portbdatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a1_PORTBDATAIN_bus\,
	portaaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a1_PORTAADDR_bus\,
	portbaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a1_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portadataout => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a1_PORTADATAOUT_bus\);

-- Location: M4K_X23_Y11
\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a13\ : cycloneii_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vram160x120x4:vram|altsyncram:altsyncram_component|altsyncram_r5o1:auto_generated|altsyncram_m6m1:altsyncram1|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "bidir_dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clear => "none",
	port_a_byte_enable_clock => "none",
	port_a_data_in_clear => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 1,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 4,
	port_a_write_enable_clear => "none",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_byte_enable_clear => "none",
	port_b_data_in_clear => "none",
	port_b_data_in_clock => "clock1",
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 1,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 4,
	port_b_read_enable_write_enable_clear => "none",
	port_b_read_enable_write_enable_clock => "clock1",
	ram_block_type => "M4K",
	safe_write => "err_on_2clk")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbrewe => \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~2_combout\,
	clk0 => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	clk1 => \clear_mux|LPM_MUX_component|auto_generated|result_node[19]~clkctrl_outclk\,
	ena0 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode334w\(3),
	ena1 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~2_combout\,
	portadatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a13_PORTADATAIN_bus\,
	portbdatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a13_PORTBDATAIN_bus\,
	portaaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a13_PORTAADDR_bus\,
	portbaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a13_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portadataout => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a13_PORTADATAOUT_bus\);

-- Location: M4K_X23_Y6
\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a10\ : cycloneii_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vram160x120x4:vram|altsyncram:altsyncram_component|altsyncram_r5o1:auto_generated|altsyncram_m6m1:altsyncram1|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "bidir_dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clear => "none",
	port_a_byte_enable_clock => "none",
	port_a_data_in_clear => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 2,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 4,
	port_a_write_enable_clear => "none",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_byte_enable_clear => "none",
	port_b_data_in_clear => "none",
	port_b_data_in_clock => "clock1",
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 2,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 4,
	port_b_read_enable_write_enable_clear => "none",
	port_b_read_enable_write_enable_clock => "clock1",
	ram_block_type => "M4K",
	safe_write => "err_on_2clk")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbrewe => \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~0_combout\,
	clk0 => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	clk1 => \clear_mux|LPM_MUX_component|auto_generated|result_node[19]~clkctrl_outclk\,
	ena0 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode324w\(3),
	ena1 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~0_combout\,
	portadatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a10_PORTADATAIN_bus\,
	portbdatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a10_PORTBDATAIN_bus\,
	portaaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a10_PORTAADDR_bus\,
	portbaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a10_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portadataout => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a10_PORTADATAOUT_bus\);

-- Location: M4K_X11_Y13
\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a6\ : cycloneii_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vram160x120x4:vram|altsyncram:altsyncram_component|altsyncram_r5o1:auto_generated|altsyncram_m6m1:altsyncram1|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "bidir_dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clear => "none",
	port_a_byte_enable_clock => "none",
	port_a_data_in_clear => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 2,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 4,
	port_a_write_enable_clear => "none",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_byte_enable_clear => "none",
	port_b_data_in_clear => "none",
	port_b_data_in_clock => "clock1",
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 2,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 4,
	port_b_read_enable_write_enable_clear => "none",
	port_b_read_enable_write_enable_clock => "clock1",
	ram_block_type => "M4K",
	safe_write => "err_on_2clk")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbrewe => \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~1_combout\,
	clk0 => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	clk1 => \clear_mux|LPM_MUX_component|auto_generated|result_node[19]~clkctrl_outclk\,
	ena0 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode314w\(3),
	ena1 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~1_combout\,
	portadatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a6_PORTADATAIN_bus\,
	portbdatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a6_PORTBDATAIN_bus\,
	portaaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a6_PORTAADDR_bus\,
	portbaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a6_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portadataout => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a6_PORTADATAOUT_bus\);

-- Location: M4K_X23_Y8
\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a7\ : cycloneii_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vram160x120x4:vram|altsyncram:altsyncram_component|altsyncram_r5o1:auto_generated|altsyncram_m6m1:altsyncram1|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "bidir_dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clear => "none",
	port_a_byte_enable_clock => "none",
	port_a_data_in_clear => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 3,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 4,
	port_a_write_enable_clear => "none",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_byte_enable_clear => "none",
	port_b_data_in_clear => "none",
	port_b_data_in_clock => "clock1",
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 3,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 4,
	port_b_read_enable_write_enable_clear => "none",
	port_b_read_enable_write_enable_clock => "clock1",
	ram_block_type => "M4K",
	safe_write => "err_on_2clk")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbrewe => \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~1_combout\,
	clk0 => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	clk1 => \clear_mux|LPM_MUX_component|auto_generated|result_node[19]~clkctrl_outclk\,
	ena0 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode314w\(3),
	ena1 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~1_combout\,
	portadatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a7_PORTADATAIN_bus\,
	portbdatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a7_PORTBDATAIN_bus\,
	portaaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a7_PORTAADDR_bus\,
	portbaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a7_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portadataout => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a7_PORTADATAOUT_bus\);

-- Location: M4K_X23_Y4
\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a11\ : cycloneii_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vram160x120x4:vram|altsyncram:altsyncram_component|altsyncram_r5o1:auto_generated|altsyncram_m6m1:altsyncram1|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "bidir_dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clear => "none",
	port_a_byte_enable_clock => "none",
	port_a_data_in_clear => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 3,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 4,
	port_a_write_enable_clear => "none",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_byte_enable_clear => "none",
	port_b_data_in_clear => "none",
	port_b_data_in_clock => "clock1",
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 3,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 4,
	port_b_read_enable_write_enable_clear => "none",
	port_b_read_enable_write_enable_clock => "clock1",
	ram_block_type => "M4K",
	safe_write => "err_on_2clk")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbrewe => \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~0_combout\,
	clk0 => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	clk1 => \clear_mux|LPM_MUX_component|auto_generated|result_node[19]~clkctrl_outclk\,
	ena0 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode324w\(3),
	ena1 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~0_combout\,
	portadatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a11_PORTADATAIN_bus\,
	portbdatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a11_PORTBDATAIN_bus\,
	portaaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a11_PORTAADDR_bus\,
	portbaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a11_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portadataout => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a11_PORTADATAOUT_bus\);

-- Location: LCCOMB_X14_Y10_N0
\from640x480to160x120|Add0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \from640x480to160x120|Add0~0_combout\ = (\pixel_counter|y_reg\(2) & (\pixel_counter|y_reg\(4) $ (VCC))) # (!\pixel_counter|y_reg\(2) & (\pixel_counter|y_reg\(4) & VCC))
-- \from640x480to160x120|Add0~1\ = CARRY((\pixel_counter|y_reg\(2) & \pixel_counter|y_reg\(4)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|y_reg\(2),
	datab => \pixel_counter|y_reg\(4),
	datad => VCC,
	combout => \from640x480to160x120|Add0~0_combout\,
	cout => \from640x480to160x120|Add0~1\);

-- Location: LCCOMB_X14_Y10_N2
\from640x480to160x120|Add0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \from640x480to160x120|Add0~2_combout\ = (\pixel_counter|y_reg\(5) & ((\pixel_counter|y_reg\(3) & (\from640x480to160x120|Add0~1\ & VCC)) # (!\pixel_counter|y_reg\(3) & (!\from640x480to160x120|Add0~1\)))) # (!\pixel_counter|y_reg\(5) & 
-- ((\pixel_counter|y_reg\(3) & (!\from640x480to160x120|Add0~1\)) # (!\pixel_counter|y_reg\(3) & ((\from640x480to160x120|Add0~1\) # (GND)))))
-- \from640x480to160x120|Add0~3\ = CARRY((\pixel_counter|y_reg\(5) & (!\pixel_counter|y_reg\(3) & !\from640x480to160x120|Add0~1\)) # (!\pixel_counter|y_reg\(5) & ((!\from640x480to160x120|Add0~1\) # (!\pixel_counter|y_reg\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|y_reg\(5),
	datab => \pixel_counter|y_reg\(3),
	datad => VCC,
	cin => \from640x480to160x120|Add0~1\,
	combout => \from640x480to160x120|Add0~2_combout\,
	cout => \from640x480to160x120|Add0~3\);

-- Location: LCCOMB_X14_Y10_N8
\from640x480to160x120|Add0~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \from640x480to160x120|Add0~8_combout\ = ((\pixel_counter|y_reg\(6) $ (\pixel_counter|y_reg\(8) $ (!\from640x480to160x120|Add0~7\)))) # (GND)
-- \from640x480to160x120|Add0~9\ = CARRY((\pixel_counter|y_reg\(6) & ((\pixel_counter|y_reg\(8)) # (!\from640x480to160x120|Add0~7\))) # (!\pixel_counter|y_reg\(6) & (\pixel_counter|y_reg\(8) & !\from640x480to160x120|Add0~7\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|y_reg\(6),
	datab => \pixel_counter|y_reg\(8),
	datad => VCC,
	cin => \from640x480to160x120|Add0~7\,
	combout => \from640x480to160x120|Add0~8_combout\,
	cout => \from640x480to160x120|Add0~9\);

-- Location: LCCOMB_X14_Y10_N10
\from640x480to160x120|Add0~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \from640x480to160x120|Add0~10_combout\ = (\pixel_counter|y_reg\(7) & (!\from640x480to160x120|Add0~9\)) # (!\pixel_counter|y_reg\(7) & ((\from640x480to160x120|Add0~9\) # (GND)))
-- \from640x480to160x120|Add0~11\ = CARRY((!\from640x480to160x120|Add0~9\) # (!\pixel_counter|y_reg\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \pixel_counter|y_reg\(7),
	datad => VCC,
	cin => \from640x480to160x120|Add0~9\,
	combout => \from640x480to160x120|Add0~10_combout\,
	cout => \from640x480to160x120|Add0~11\);

-- Location: LCCOMB_X14_Y10_N12
\from640x480to160x120|Add0~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \from640x480to160x120|Add0~12_combout\ = (\pixel_counter|y_reg\(8) & (\from640x480to160x120|Add0~11\ $ (GND))) # (!\pixel_counter|y_reg\(8) & (!\from640x480to160x120|Add0~11\ & VCC))
-- \from640x480to160x120|Add0~13\ = CARRY((\pixel_counter|y_reg\(8) & !\from640x480to160x120|Add0~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \pixel_counter|y_reg\(8),
	datad => VCC,
	cin => \from640x480to160x120|Add0~11\,
	combout => \from640x480to160x120|Add0~12_combout\,
	cout => \from640x480to160x120|Add0~13\);

-- Location: LCFF_X13_Y10_N13
\pixel_counter|y_reg[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \pixel_counter|y_reg[1]~12_combout\,
	sclr => \pixel_counter|LessThan1~2_combout\,
	ena => \pixel_counter|Equal0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pixel_counter|y_reg\(1));

-- Location: LCFF_X13_Y10_N11
\pixel_counter|y_reg[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \pixel_counter|y_reg[0]~10_combout\,
	sclr => \pixel_counter|LessThan1~2_combout\,
	ena => \pixel_counter|Equal0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pixel_counter|y_reg\(0));

-- Location: LCCOMB_X13_Y10_N10
\pixel_counter|y_reg[0]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|y_reg[0]~10_combout\ = \pixel_counter|y_reg\(0) $ (VCC)
-- \pixel_counter|y_reg[0]~11\ = CARRY(\pixel_counter|y_reg\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|y_reg\(0),
	datad => VCC,
	combout => \pixel_counter|y_reg[0]~10_combout\,
	cout => \pixel_counter|y_reg[0]~11\);

-- Location: LCCOMB_X13_Y10_N12
\pixel_counter|y_reg[1]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|y_reg[1]~12_combout\ = (\pixel_counter|y_reg\(1) & (!\pixel_counter|y_reg[0]~11\)) # (!\pixel_counter|y_reg\(1) & ((\pixel_counter|y_reg[0]~11\) # (GND)))
-- \pixel_counter|y_reg[1]~13\ = CARRY((!\pixel_counter|y_reg[0]~11\) # (!\pixel_counter|y_reg\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|y_reg\(1),
	datad => VCC,
	cin => \pixel_counter|y_reg[0]~11\,
	combout => \pixel_counter|y_reg[1]~12_combout\,
	cout => \pixel_counter|y_reg[1]~13\);

-- Location: LCCOMB_X20_Y10_N24
\pixel_counter|LessThan0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|LessThan0~0_combout\ = (\pixel_counter|x_reg\(3) & (\pixel_counter|x_reg\(2) & ((\pixel_counter|x_reg\(0)) # (\pixel_counter|x_reg\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|x_reg\(3),
	datab => \pixel_counter|x_reg\(0),
	datac => \pixel_counter|x_reg\(1),
	datad => \pixel_counter|x_reg\(2),
	combout => \pixel_counter|LessThan0~0_combout\);

-- Location: LCCOMB_X18_Y10_N28
\pixel_counter|process_2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|process_2~0_combout\ = (((!\pixel_counter|Add0~2_combout\) # (!\pixel_counter|Add0~6_combout\)) # (!\pixel_counter|Add0~0_combout\)) # (!\pixel_counter|Add0~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|Add0~4_combout\,
	datab => \pixel_counter|Add0~0_combout\,
	datac => \pixel_counter|Add0~6_combout\,
	datad => \pixel_counter|Add0~2_combout\,
	combout => \pixel_counter|process_2~0_combout\);

-- Location: LCCOMB_X18_Y10_N26
\pixel_counter|process_2~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|process_2~1_combout\ = (!\pixel_counter|Add0~8_combout\ & (!\pixel_counter|Add0~10_combout\ & (!\pixel_counter|Add0~12_combout\ & \pixel_counter|process_2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|Add0~8_combout\,
	datab => \pixel_counter|Add0~10_combout\,
	datac => \pixel_counter|Add0~12_combout\,
	datad => \pixel_counter|process_2~0_combout\,
	combout => \pixel_counter|process_2~1_combout\);

-- Location: LCCOMB_X18_Y10_N20
\pixel_counter|process_2~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|process_2~2_combout\ = (!\pixel_counter|Add0~16_combout\ & (!\pixel_counter|Add0~18_combout\ & ((\pixel_counter|process_2~1_combout\) # (!\pixel_counter|Add0~14_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|process_2~1_combout\,
	datab => \pixel_counter|Add0~16_combout\,
	datac => \pixel_counter|Add0~14_combout\,
	datad => \pixel_counter|Add0~18_combout\,
	combout => \pixel_counter|process_2~2_combout\);

-- Location: LCCOMB_X20_Y10_N12
\pixel_counter|process_2~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|process_2~3_combout\ = (\pixel_counter|x_reg\(1) & (\pixel_counter|x_reg\(2) & \pixel_counter|x_reg\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|x_reg\(1),
	datab => \pixel_counter|x_reg\(2),
	datad => \pixel_counter|x_reg\(3),
	combout => \pixel_counter|process_2~3_combout\);

-- Location: LCCOMB_X18_Y10_N24
\pixel_counter|LessThan5~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|LessThan5~1_combout\ = (\pixel_counter|Add0~4_combout\ & (\pixel_counter|Add0~6_combout\ & (\pixel_counter|Add0~8_combout\ & \pixel_counter|LessThan0~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|Add0~4_combout\,
	datab => \pixel_counter|Add0~6_combout\,
	datac => \pixel_counter|Add0~8_combout\,
	datad => \pixel_counter|LessThan0~1_combout\,
	combout => \pixel_counter|LessThan5~1_combout\);

-- Location: LCCOMB_X18_Y10_N18
\pixel_counter|LessThan5~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|LessThan5~2_combout\ = (\pixel_counter|x_next[5]~2_combout\) # ((\pixel_counter|LessThan5~1_combout\ & (\pixel_counter|Add0~0_combout\ & \pixel_counter|Add0~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|LessThan5~1_combout\,
	datab => \pixel_counter|x_next[5]~2_combout\,
	datac => \pixel_counter|Add0~0_combout\,
	datad => \pixel_counter|Add0~2_combout\,
	combout => \pixel_counter|LessThan5~2_combout\);

-- Location: PIN_104,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
\adr[14]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_adr(14),
	combout => \adr~combout\(14));

-- Location: PIN_103,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
\adr[13]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_adr(13),
	combout => \adr~combout\(13));

-- Location: PIN_93,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
\adr[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_adr(6),
	combout => \adr~combout\(6));

-- Location: PIN_80,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
\color[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_color(1),
	combout => \color~combout\(1));

-- Location: PIN_81,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
\color[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_color(2),
	combout => \color~combout\(2));

-- Location: PIN_17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\clk~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_clk,
	combout => \clk~combout\);

-- Location: PLL_1
\main_pll|altpll_component|pll\ : cycloneii_pll
-- pragma translate_off
GENERIC MAP (
	bandwidth => 0,
	bandwidth_type => "low",
	c0_high => 16,
	c0_initial => 1,
	c0_low => 16,
	c0_mode => "even",
	c0_ph => 0,
	c1_high => 2,
	c1_initial => 1,
	c1_low => 2,
	c1_mode => "even",
	c1_ph => 0,
	c2_mode => "bypass",
	c2_ph => 0,
	charge_pump_current => 80,
	clk0_counter => "c0",
	clk0_divide_by => 2,
	clk0_duty_cycle => 50,
	clk0_multiply_by => 1,
	clk0_phase_shift => "0",
	clk1_counter => "c1",
	clk1_divide_by => 1,
	clk1_duty_cycle => 50,
	clk1_multiply_by => 4,
	clk1_phase_shift => "0",
	clk2_duty_cycle => 50,
	clk2_phase_shift => "0",
	compensate_clock => "clk0",
	gate_lock_counter => 0,
	gate_lock_signal => "no",
	inclk0_input_frequency => 20000,
	inclk1_input_frequency => 20000,
	invalid_lock_multiplier => 5,
	loop_filter_c => 3,
	loop_filter_r => " 2.500000",
	m => 16,
	m_initial => 1,
	m_ph => 0,
	n => 1,
	operation_mode => "normal",
	pfd_max => 100000,
	pfd_min => 2484,
	pll_compensation_delay => 3433,
	self_reset_on_gated_loss_lock => "off",
	sim_gate_lock_device_behavior => "off",
	simulation_type => "timing",
	valid_lock_multiplier => 1,
	vco_center => 1333,
	vco_max => 2000,
	vco_min => 1000)
-- pragma translate_on
PORT MAP (
	inclk => \main_pll|altpll_component|pll_INCLK_bus\,
	clk => \main_pll|altpll_component|pll_CLK_bus\);

-- Location: CLKCTRL_G3
\main_pll|altpll_component|_clk0~clkctrl\ : cycloneii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \main_pll|altpll_component|_clk0~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \main_pll|altpll_component|_clk0~clkctrl_outclk\);

-- Location: PIN_101,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
\adr[12]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_adr(12),
	combout => \adr~combout\(12));

-- Location: PIN_74,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
\clear~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_clear,
	combout => \clear~combout\);

-- Location: LCCOMB_X18_Y9_N0
\clear_screen|adr[0]~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_screen|adr[0]~15_combout\ = \clear_screen|adr\(0) $ (VCC)
-- \clear_screen|adr[0]~16\ = CARRY(\clear_screen|adr\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \clear_screen|adr\(0),
	datad => VCC,
	combout => \clear_screen|adr[0]~15_combout\,
	cout => \clear_screen|adr[0]~16\);

-- Location: LCCOMB_X18_Y9_N24
\clear_screen|adr[12]~39\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_screen|adr[12]~39_combout\ = (\clear_screen|adr\(12) & (\clear_screen|adr[11]~38\ $ (GND))) # (!\clear_screen|adr\(12) & (!\clear_screen|adr[11]~38\ & VCC))
-- \clear_screen|adr[12]~40\ = CARRY((\clear_screen|adr\(12) & !\clear_screen|adr[11]~38\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \clear_screen|adr\(12),
	datad => VCC,
	cin => \clear_screen|adr[11]~38\,
	combout => \clear_screen|adr[12]~39_combout\,
	cout => \clear_screen|adr[12]~40\);

-- Location: LCCOMB_X18_Y9_N26
\clear_screen|adr[13]~41\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_screen|adr[13]~41_combout\ = (\clear_screen|adr\(13) & (!\clear_screen|adr[12]~40\)) # (!\clear_screen|adr\(13) & ((\clear_screen|adr[12]~40\) # (GND)))
-- \clear_screen|adr[13]~42\ = CARRY((!\clear_screen|adr[12]~40\) # (!\clear_screen|adr\(13)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \clear_screen|adr\(13),
	datad => VCC,
	cin => \clear_screen|adr[12]~40\,
	combout => \clear_screen|adr[13]~41_combout\,
	cout => \clear_screen|adr[13]~42\);

-- Location: LCFF_X18_Y9_N27
\clear_screen|adr[13]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|ALT_INV__clk1~clkctrl_outclk\,
	datain => \clear_screen|adr[13]~41_combout\,
	sclr => \clear_screen|LessThan0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \clear_screen|adr\(13));

-- Location: LCCOMB_X18_Y9_N28
\clear_screen|adr[14]~43\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_screen|adr[14]~43_combout\ = \clear_screen|adr\(14) $ (!\clear_screen|adr[13]~42\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001111000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \clear_screen|adr\(14),
	cin => \clear_screen|adr[13]~42\,
	combout => \clear_screen|adr[14]~43_combout\);

-- Location: LCFF_X19_Y9_N1
\clear_screen|adr[14]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|ALT_INV__clk1~clkctrl_outclk\,
	sdata => \clear_screen|adr[14]~43_combout\,
	sclr => \clear_screen|LessThan0~4_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \clear_screen|adr\(14));

-- Location: LCCOMB_X18_Y9_N20
\clear_screen|adr[10]~35\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_screen|adr[10]~35_combout\ = (\clear_screen|adr\(10) & (\clear_screen|adr[9]~34\ $ (GND))) # (!\clear_screen|adr\(10) & (!\clear_screen|adr[9]~34\ & VCC))
-- \clear_screen|adr[10]~36\ = CARRY((\clear_screen|adr\(10) & !\clear_screen|adr[9]~34\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \clear_screen|adr\(10),
	datad => VCC,
	cin => \clear_screen|adr[9]~34\,
	combout => \clear_screen|adr[10]~35_combout\,
	cout => \clear_screen|adr[10]~36\);

-- Location: LCFF_X18_Y9_N21
\clear_screen|adr[10]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|ALT_INV__clk1~clkctrl_outclk\,
	datain => \clear_screen|adr[10]~35_combout\,
	sclr => \clear_screen|LessThan0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \clear_screen|adr\(10));

-- Location: LCCOMB_X18_Y9_N12
\clear_screen|adr[6]~27\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_screen|adr[6]~27_combout\ = (\clear_screen|adr\(6) & (\clear_screen|adr[5]~26\ $ (GND))) # (!\clear_screen|adr\(6) & (!\clear_screen|adr[5]~26\ & VCC))
-- \clear_screen|adr[6]~28\ = CARRY((\clear_screen|adr\(6) & !\clear_screen|adr[5]~26\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \clear_screen|adr\(6),
	datad => VCC,
	cin => \clear_screen|adr[5]~26\,
	combout => \clear_screen|adr[6]~27_combout\,
	cout => \clear_screen|adr[6]~28\);

-- Location: LCFF_X18_Y9_N13
\clear_screen|adr[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|ALT_INV__clk1~clkctrl_outclk\,
	datain => \clear_screen|adr[6]~27_combout\,
	sclr => \clear_screen|LessThan0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \clear_screen|adr\(6));

-- Location: LCCOMB_X17_Y9_N20
\clear_screen|LessThan0~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_screen|LessThan0~1_combout\ = (((!\clear_screen|adr\(6)) # (!\clear_screen|adr\(4))) # (!\clear_screen|adr\(7))) # (!\clear_screen|adr\(5))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \clear_screen|adr\(5),
	datab => \clear_screen|adr\(7),
	datac => \clear_screen|adr\(4),
	datad => \clear_screen|adr\(6),
	combout => \clear_screen|LessThan0~1_combout\);

-- Location: LCCOMB_X18_Y9_N30
\clear_screen|LessThan0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_screen|LessThan0~0_combout\ = (((!\clear_screen|adr\(0)) # (!\clear_screen|adr\(2))) # (!\clear_screen|adr\(1))) # (!\clear_screen|adr\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \clear_screen|adr\(3),
	datab => \clear_screen|adr\(1),
	datac => \clear_screen|adr\(2),
	datad => \clear_screen|adr\(0),
	combout => \clear_screen|LessThan0~0_combout\);

-- Location: LCCOMB_X17_Y9_N26
\clear_screen|LessThan0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_screen|LessThan0~2_combout\ = ((!\clear_screen|adr\(8) & ((\clear_screen|LessThan0~1_combout\) # (\clear_screen|LessThan0~0_combout\)))) # (!\clear_screen|adr\(9))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011101110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \clear_screen|adr\(8),
	datab => \clear_screen|adr\(9),
	datac => \clear_screen|LessThan0~1_combout\,
	datad => \clear_screen|LessThan0~0_combout\,
	combout => \clear_screen|LessThan0~2_combout\);

-- Location: LCCOMB_X17_Y9_N12
\clear_screen|LessThan0~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_screen|LessThan0~3_combout\ = ((!\clear_screen|adr\(10) & \clear_screen|LessThan0~2_combout\)) # (!\clear_screen|adr\(11))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \clear_screen|adr\(10),
	datac => \clear_screen|adr\(11),
	datad => \clear_screen|LessThan0~2_combout\,
	combout => \clear_screen|LessThan0~3_combout\);

-- Location: LCCOMB_X17_Y9_N6
\clear_screen|LessThan0~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_screen|LessThan0~4_combout\ = (\clear_screen|adr\(14) & ((\clear_screen|adr\(13)) # ((\clear_screen|adr\(12)) # (!\clear_screen|LessThan0~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \clear_screen|adr\(13),
	datab => \clear_screen|adr\(12),
	datac => \clear_screen|adr\(14),
	datad => \clear_screen|LessThan0~3_combout\,
	combout => \clear_screen|LessThan0~4_combout\);

-- Location: LCFF_X18_Y9_N1
\clear_screen|adr[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|ALT_INV__clk1~clkctrl_outclk\,
	datain => \clear_screen|adr[0]~15_combout\,
	sclr => \clear_screen|LessThan0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \clear_screen|adr\(0));

-- Location: LCCOMB_X18_Y9_N2
\clear_screen|adr[1]~17\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_screen|adr[1]~17_combout\ = (\clear_screen|adr\(1) & (!\clear_screen|adr[0]~16\)) # (!\clear_screen|adr\(1) & ((\clear_screen|adr[0]~16\) # (GND)))
-- \clear_screen|adr[1]~18\ = CARRY((!\clear_screen|adr[0]~16\) # (!\clear_screen|adr\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \clear_screen|adr\(1),
	datad => VCC,
	cin => \clear_screen|adr[0]~16\,
	combout => \clear_screen|adr[1]~17_combout\,
	cout => \clear_screen|adr[1]~18\);

-- Location: LCFF_X18_Y9_N3
\clear_screen|adr[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|ALT_INV__clk1~clkctrl_outclk\,
	datain => \clear_screen|adr[1]~17_combout\,
	sclr => \clear_screen|LessThan0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \clear_screen|adr\(1));

-- Location: LCCOMB_X18_Y9_N4
\clear_screen|adr[2]~19\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_screen|adr[2]~19_combout\ = (\clear_screen|adr\(2) & (\clear_screen|adr[1]~18\ $ (GND))) # (!\clear_screen|adr\(2) & (!\clear_screen|adr[1]~18\ & VCC))
-- \clear_screen|adr[2]~20\ = CARRY((\clear_screen|adr\(2) & !\clear_screen|adr[1]~18\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \clear_screen|adr\(2),
	datad => VCC,
	cin => \clear_screen|adr[1]~18\,
	combout => \clear_screen|adr[2]~19_combout\,
	cout => \clear_screen|adr[2]~20\);

-- Location: LCFF_X18_Y9_N5
\clear_screen|adr[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|ALT_INV__clk1~clkctrl_outclk\,
	datain => \clear_screen|adr[2]~19_combout\,
	sclr => \clear_screen|LessThan0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \clear_screen|adr\(2));

-- Location: LCCOMB_X18_Y9_N6
\clear_screen|adr[3]~21\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_screen|adr[3]~21_combout\ = (\clear_screen|adr\(3) & (!\clear_screen|adr[2]~20\)) # (!\clear_screen|adr\(3) & ((\clear_screen|adr[2]~20\) # (GND)))
-- \clear_screen|adr[3]~22\ = CARRY((!\clear_screen|adr[2]~20\) # (!\clear_screen|adr\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \clear_screen|adr\(3),
	datad => VCC,
	cin => \clear_screen|adr[2]~20\,
	combout => \clear_screen|adr[3]~21_combout\,
	cout => \clear_screen|adr[3]~22\);

-- Location: LCCOMB_X18_Y9_N8
\clear_screen|adr[4]~23\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_screen|adr[4]~23_combout\ = (\clear_screen|adr\(4) & (\clear_screen|adr[3]~22\ $ (GND))) # (!\clear_screen|adr\(4) & (!\clear_screen|adr[3]~22\ & VCC))
-- \clear_screen|adr[4]~24\ = CARRY((\clear_screen|adr\(4) & !\clear_screen|adr[3]~22\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \clear_screen|adr\(4),
	datad => VCC,
	cin => \clear_screen|adr[3]~22\,
	combout => \clear_screen|adr[4]~23_combout\,
	cout => \clear_screen|adr[4]~24\);

-- Location: LCFF_X18_Y9_N9
\clear_screen|adr[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|ALT_INV__clk1~clkctrl_outclk\,
	datain => \clear_screen|adr[4]~23_combout\,
	sclr => \clear_screen|LessThan0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \clear_screen|adr\(4));

-- Location: LCCOMB_X18_Y9_N10
\clear_screen|adr[5]~25\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_screen|adr[5]~25_combout\ = (\clear_screen|adr\(5) & (!\clear_screen|adr[4]~24\)) # (!\clear_screen|adr\(5) & ((\clear_screen|adr[4]~24\) # (GND)))
-- \clear_screen|adr[5]~26\ = CARRY((!\clear_screen|adr[4]~24\) # (!\clear_screen|adr\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \clear_screen|adr\(5),
	datad => VCC,
	cin => \clear_screen|adr[4]~24\,
	combout => \clear_screen|adr[5]~25_combout\,
	cout => \clear_screen|adr[5]~26\);

-- Location: LCCOMB_X18_Y9_N14
\clear_screen|adr[7]~29\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_screen|adr[7]~29_combout\ = (\clear_screen|adr\(7) & (!\clear_screen|adr[6]~28\)) # (!\clear_screen|adr\(7) & ((\clear_screen|adr[6]~28\) # (GND)))
-- \clear_screen|adr[7]~30\ = CARRY((!\clear_screen|adr[6]~28\) # (!\clear_screen|adr\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \clear_screen|adr\(7),
	datad => VCC,
	cin => \clear_screen|adr[6]~28\,
	combout => \clear_screen|adr[7]~29_combout\,
	cout => \clear_screen|adr[7]~30\);

-- Location: LCFF_X18_Y9_N15
\clear_screen|adr[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|ALT_INV__clk1~clkctrl_outclk\,
	datain => \clear_screen|adr[7]~29_combout\,
	sclr => \clear_screen|LessThan0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \clear_screen|adr\(7));

-- Location: LCCOMB_X18_Y9_N16
\clear_screen|adr[8]~31\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_screen|adr[8]~31_combout\ = (\clear_screen|adr\(8) & (\clear_screen|adr[7]~30\ $ (GND))) # (!\clear_screen|adr\(8) & (!\clear_screen|adr[7]~30\ & VCC))
-- \clear_screen|adr[8]~32\ = CARRY((\clear_screen|adr\(8) & !\clear_screen|adr[7]~30\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \clear_screen|adr\(8),
	datad => VCC,
	cin => \clear_screen|adr[7]~30\,
	combout => \clear_screen|adr[8]~31_combout\,
	cout => \clear_screen|adr[8]~32\);

-- Location: LCCOMB_X18_Y9_N18
\clear_screen|adr[9]~33\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_screen|adr[9]~33_combout\ = (\clear_screen|adr\(9) & (!\clear_screen|adr[8]~32\)) # (!\clear_screen|adr\(9) & ((\clear_screen|adr[8]~32\) # (GND)))
-- \clear_screen|adr[9]~34\ = CARRY((!\clear_screen|adr[8]~32\) # (!\clear_screen|adr\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \clear_screen|adr\(9),
	datad => VCC,
	cin => \clear_screen|adr[8]~32\,
	combout => \clear_screen|adr[9]~33_combout\,
	cout => \clear_screen|adr[9]~34\);

-- Location: LCFF_X18_Y9_N19
\clear_screen|adr[9]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|ALT_INV__clk1~clkctrl_outclk\,
	datain => \clear_screen|adr[9]~33_combout\,
	sclr => \clear_screen|LessThan0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \clear_screen|adr\(9));

-- Location: LCCOMB_X18_Y9_N22
\clear_screen|adr[11]~37\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_screen|adr[11]~37_combout\ = (\clear_screen|adr\(11) & (!\clear_screen|adr[10]~36\)) # (!\clear_screen|adr\(11) & ((\clear_screen|adr[10]~36\) # (GND)))
-- \clear_screen|adr[11]~38\ = CARRY((!\clear_screen|adr[10]~36\) # (!\clear_screen|adr\(11)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \clear_screen|adr\(11),
	datad => VCC,
	cin => \clear_screen|adr[10]~36\,
	combout => \clear_screen|adr[11]~37_combout\,
	cout => \clear_screen|adr[11]~38\);

-- Location: LCFF_X18_Y9_N23
\clear_screen|adr[11]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|ALT_INV__clk1~clkctrl_outclk\,
	datain => \clear_screen|adr[11]~37_combout\,
	sclr => \clear_screen|LessThan0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \clear_screen|adr\(11));

-- Location: LCFF_X18_Y9_N25
\clear_screen|adr[12]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|ALT_INV__clk1~clkctrl_outclk\,
	datain => \clear_screen|adr[12]~39_combout\,
	sclr => \clear_screen|LessThan0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \clear_screen|adr\(12));

-- Location: LCCOMB_X19_Y9_N22
\clear_mux|LPM_MUX_component|auto_generated|result_node[12]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_mux|LPM_MUX_component|auto_generated|result_node[12]~2_combout\ = (\clear~combout\ & ((\clear_screen|adr\(12)))) # (!\clear~combout\ & (\adr~combout\(12)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \adr~combout\(12),
	datac => \clear~combout\,
	datad => \clear_screen|adr\(12),
	combout => \clear_mux|LPM_MUX_component|auto_generated|result_node[12]~2_combout\);

-- Location: LCCOMB_X19_Y9_N14
\clear_mux|LPM_MUX_component|auto_generated|result_node[14]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_mux|LPM_MUX_component|auto_generated|result_node[14]~0_combout\ = (\clear~combout\ & ((\clear_screen|adr\(14)))) # (!\clear~combout\ & (\adr~combout\(14)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \adr~combout\(14),
	datac => \clear~combout\,
	datad => \clear_screen|adr\(14),
	combout => \clear_mux|LPM_MUX_component|auto_generated|result_node[14]~0_combout\);

-- Location: LCCOMB_X19_Y9_N12
\clear_mux|LPM_MUX_component|auto_generated|result_node[13]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_mux|LPM_MUX_component|auto_generated|result_node[13]~1_combout\ = (\clear~combout\ & ((\clear_screen|adr\(13)))) # (!\clear~combout\ & (\adr~combout\(13)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \adr~combout\(13),
	datac => \clear~combout\,
	datad => \clear_screen|adr\(13),
	combout => \clear_mux|LPM_MUX_component|auto_generated|result_node[13]~1_combout\);

-- Location: LCCOMB_X19_Y9_N28
\vram|altsyncram_component|auto_generated|altsyncram1|decode4|w_anode297w[3]\ : cycloneii_lcell_comb
-- Equation(s):
-- \vram|altsyncram_component|auto_generated|altsyncram1|decode4|w_anode297w\(3) = (!\clear_mux|LPM_MUX_component|auto_generated|result_node[12]~2_combout\ & (!\clear_mux|LPM_MUX_component|auto_generated|result_node[14]~0_combout\ & 
-- !\clear_mux|LPM_MUX_component|auto_generated|result_node[13]~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \clear_mux|LPM_MUX_component|auto_generated|result_node[12]~2_combout\,
	datac => \clear_mux|LPM_MUX_component|auto_generated|result_node[14]~0_combout\,
	datad => \clear_mux|LPM_MUX_component|auto_generated|result_node[13]~1_combout\,
	combout => \vram|altsyncram_component|auto_generated|altsyncram1|decode4|w_anode297w\(3));

-- Location: PIN_73,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
\wr~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_wr,
	combout => \wr~combout\);

-- Location: CLKCTRL_G2
\main_pll|altpll_component|_clk1~clkctrl\ : cycloneii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \main_pll|altpll_component|_clk1~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \main_pll|altpll_component|_clk1~clkctrl_outclk\);

-- Location: LCCOMB_X27_Y7_N6
\clear_mux|LPM_MUX_component|auto_generated|result_node[19]\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_mux|LPM_MUX_component|auto_generated|result_node\(19) = LCELL((\clear~combout\ & ((GLOBAL(\main_pll|altpll_component|_clk1~clkctrl_outclk\)))) # (!\clear~combout\ & (\wr~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \wr~combout\,
	datac => \clear~combout\,
	datad => \main_pll|altpll_component|_clk1~clkctrl_outclk\,
	combout => \clear_mux|LPM_MUX_component|auto_generated|result_node\(19));

-- Location: CLKCTRL_G4
\clear_mux|LPM_MUX_component|auto_generated|result_node[19]~clkctrl\ : cycloneii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \clear_mux|LPM_MUX_component|auto_generated|result_node[19]~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clear_mux|LPM_MUX_component|auto_generated|result_node[19]~clkctrl_outclk\);

-- Location: LCCOMB_X13_Y10_N14
\pixel_counter|y_reg[2]~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|y_reg[2]~14_combout\ = (\pixel_counter|y_reg\(2) & (\pixel_counter|y_reg[1]~13\ $ (GND))) # (!\pixel_counter|y_reg\(2) & (!\pixel_counter|y_reg[1]~13\ & VCC))
-- \pixel_counter|y_reg[2]~15\ = CARRY((\pixel_counter|y_reg\(2) & !\pixel_counter|y_reg[1]~13\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \pixel_counter|y_reg\(2),
	datad => VCC,
	cin => \pixel_counter|y_reg[1]~13\,
	combout => \pixel_counter|y_reg[2]~14_combout\,
	cout => \pixel_counter|y_reg[2]~15\);

-- Location: LCCOMB_X13_Y10_N16
\pixel_counter|y_reg[3]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|y_reg[3]~16_combout\ = (\pixel_counter|y_reg\(3) & (!\pixel_counter|y_reg[2]~15\)) # (!\pixel_counter|y_reg\(3) & ((\pixel_counter|y_reg[2]~15\) # (GND)))
-- \pixel_counter|y_reg[3]~17\ = CARRY((!\pixel_counter|y_reg[2]~15\) # (!\pixel_counter|y_reg\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|y_reg\(3),
	datad => VCC,
	cin => \pixel_counter|y_reg[2]~15\,
	combout => \pixel_counter|y_reg[3]~16_combout\,
	cout => \pixel_counter|y_reg[3]~17\);

-- Location: LCCOMB_X20_Y10_N10
\pixel_counter|x_next[3]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|x_next[3]~8_combout\ = (\pixel_counter|Add0~6_combout\ & \pixel_counter|LessThan0~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|Add0~6_combout\,
	datad => \pixel_counter|LessThan0~1_combout\,
	combout => \pixel_counter|x_next[3]~8_combout\);

-- Location: LCFF_X20_Y10_N11
\pixel_counter|x_reg[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \pixel_counter|x_next[3]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pixel_counter|x_reg\(3));

-- Location: LCCOMB_X19_Y10_N2
\pixel_counter|Add0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|Add0~0_combout\ = \pixel_counter|x_reg\(0) $ (VCC)
-- \pixel_counter|Add0~1\ = CARRY(\pixel_counter|x_reg\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \pixel_counter|x_reg\(0),
	datad => VCC,
	combout => \pixel_counter|Add0~0_combout\,
	cout => \pixel_counter|Add0~1\);

-- Location: LCCOMB_X19_Y10_N26
\pixel_counter|x_next[0]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|x_next[0]~1_combout\ = (\pixel_counter|Add0~0_combout\ & \pixel_counter|LessThan0~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \pixel_counter|Add0~0_combout\,
	datad => \pixel_counter|LessThan0~1_combout\,
	combout => \pixel_counter|x_next[0]~1_combout\);

-- Location: LCFF_X19_Y10_N27
\pixel_counter|x_reg[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \pixel_counter|x_next[0]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pixel_counter|x_reg\(0));

-- Location: LCCOMB_X19_Y10_N4
\pixel_counter|Add0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|Add0~2_combout\ = (\pixel_counter|x_reg\(1) & (!\pixel_counter|Add0~1\)) # (!\pixel_counter|x_reg\(1) & ((\pixel_counter|Add0~1\) # (GND)))
-- \pixel_counter|Add0~3\ = CARRY((!\pixel_counter|Add0~1\) # (!\pixel_counter|x_reg\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|x_reg\(1),
	datad => VCC,
	cin => \pixel_counter|Add0~1\,
	combout => \pixel_counter|Add0~2_combout\,
	cout => \pixel_counter|Add0~3\);

-- Location: LCCOMB_X19_Y10_N6
\pixel_counter|Add0~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|Add0~4_combout\ = (\pixel_counter|x_reg\(2) & (\pixel_counter|Add0~3\ $ (GND))) # (!\pixel_counter|x_reg\(2) & (!\pixel_counter|Add0~3\ & VCC))
-- \pixel_counter|Add0~5\ = CARRY((\pixel_counter|x_reg\(2) & !\pixel_counter|Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \pixel_counter|x_reg\(2),
	datad => VCC,
	cin => \pixel_counter|Add0~3\,
	combout => \pixel_counter|Add0~4_combout\,
	cout => \pixel_counter|Add0~5\);

-- Location: LCCOMB_X19_Y10_N28
\pixel_counter|x_next[2]~9\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|x_next[2]~9_combout\ = (\pixel_counter|Add0~4_combout\ & \pixel_counter|LessThan0~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \pixel_counter|Add0~4_combout\,
	datad => \pixel_counter|LessThan0~1_combout\,
	combout => \pixel_counter|x_next[2]~9_combout\);

-- Location: LCFF_X19_Y10_N29
\pixel_counter|x_reg[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \pixel_counter|x_next[2]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pixel_counter|x_reg\(2));

-- Location: LCCOMB_X19_Y10_N8
\pixel_counter|Add0~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|Add0~6_combout\ = (\pixel_counter|x_reg\(3) & (!\pixel_counter|Add0~5\)) # (!\pixel_counter|x_reg\(3) & ((\pixel_counter|Add0~5\) # (GND)))
-- \pixel_counter|Add0~7\ = CARRY((!\pixel_counter|Add0~5\) # (!\pixel_counter|x_reg\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \pixel_counter|x_reg\(3),
	datad => VCC,
	cin => \pixel_counter|Add0~5\,
	combout => \pixel_counter|Add0~6_combout\,
	cout => \pixel_counter|Add0~7\);

-- Location: LCCOMB_X19_Y10_N10
\pixel_counter|Add0~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|Add0~8_combout\ = (\pixel_counter|x_reg\(4) & (\pixel_counter|Add0~7\ $ (GND))) # (!\pixel_counter|x_reg\(4) & (!\pixel_counter|Add0~7\ & VCC))
-- \pixel_counter|Add0~9\ = CARRY((\pixel_counter|x_reg\(4) & !\pixel_counter|Add0~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \pixel_counter|x_reg\(4),
	datad => VCC,
	cin => \pixel_counter|Add0~7\,
	combout => \pixel_counter|Add0~8_combout\,
	cout => \pixel_counter|Add0~9\);

-- Location: LCCOMB_X19_Y10_N22
\pixel_counter|x_next[4]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|x_next[4]~7_combout\ = (\pixel_counter|Add0~8_combout\ & \pixel_counter|LessThan0~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \pixel_counter|Add0~8_combout\,
	datad => \pixel_counter|LessThan0~1_combout\,
	combout => \pixel_counter|x_next[4]~7_combout\);

-- Location: LCFF_X19_Y10_N23
\pixel_counter|x_reg[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \pixel_counter|x_next[4]~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pixel_counter|x_reg\(4));

-- Location: LCCOMB_X20_Y10_N20
\pixel_counter|Equal0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|Equal0~0_combout\ = (!\pixel_counter|x_reg\(5) & (!\pixel_counter|x_reg\(6) & !\pixel_counter|x_reg\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|x_reg\(5),
	datac => \pixel_counter|x_reg\(6),
	datad => \pixel_counter|x_reg\(7),
	combout => \pixel_counter|Equal0~0_combout\);

-- Location: LCCOMB_X20_Y10_N30
\pixel_counter|LessThan0~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|LessThan0~1_combout\ = ((\pixel_counter|Equal0~0_combout\ & ((!\pixel_counter|x_reg\(4)) # (!\pixel_counter|LessThan0~0_combout\)))) # (!\pixel_counter|Equal0~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|LessThan0~0_combout\,
	datab => \pixel_counter|x_reg\(4),
	datac => \pixel_counter|Equal0~0_combout\,
	datad => \pixel_counter|Equal0~1_combout\,
	combout => \pixel_counter|LessThan0~1_combout\);

-- Location: LCCOMB_X19_Y10_N0
\pixel_counter|x_next[6]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|x_next[6]~6_combout\ = (\pixel_counter|Add0~12_combout\ & \pixel_counter|LessThan0~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|Add0~12_combout\,
	datad => \pixel_counter|LessThan0~1_combout\,
	combout => \pixel_counter|x_next[6]~6_combout\);

-- Location: LCFF_X19_Y10_N1
\pixel_counter|x_reg[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \pixel_counter|x_next[6]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pixel_counter|x_reg\(6));

-- Location: LCCOMB_X19_Y10_N30
\pixel_counter|x_next[5]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|x_next[5]~2_combout\ = (\pixel_counter|Add0~10_combout\ & \pixel_counter|LessThan0~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|Add0~10_combout\,
	datad => \pixel_counter|LessThan0~1_combout\,
	combout => \pixel_counter|x_next[5]~2_combout\);

-- Location: LCFF_X19_Y10_N31
\pixel_counter|x_reg[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \pixel_counter|x_next[5]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pixel_counter|x_reg\(5));

-- Location: LCCOMB_X19_Y10_N12
\pixel_counter|Add0~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|Add0~10_combout\ = (\pixel_counter|x_reg\(5) & (!\pixel_counter|Add0~9\)) # (!\pixel_counter|x_reg\(5) & ((\pixel_counter|Add0~9\) # (GND)))
-- \pixel_counter|Add0~11\ = CARRY((!\pixel_counter|Add0~9\) # (!\pixel_counter|x_reg\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \pixel_counter|x_reg\(5),
	datad => VCC,
	cin => \pixel_counter|Add0~9\,
	combout => \pixel_counter|Add0~10_combout\,
	cout => \pixel_counter|Add0~11\);

-- Location: LCCOMB_X19_Y10_N14
\pixel_counter|Add0~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|Add0~12_combout\ = (\pixel_counter|x_reg\(6) & (\pixel_counter|Add0~11\ $ (GND))) # (!\pixel_counter|x_reg\(6) & (!\pixel_counter|Add0~11\ & VCC))
-- \pixel_counter|Add0~13\ = CARRY((\pixel_counter|x_reg\(6) & !\pixel_counter|Add0~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \pixel_counter|x_reg\(6),
	datad => VCC,
	cin => \pixel_counter|Add0~11\,
	combout => \pixel_counter|Add0~12_combout\,
	cout => \pixel_counter|Add0~13\);

-- Location: LCCOMB_X19_Y10_N16
\pixel_counter|Add0~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|Add0~14_combout\ = (\pixel_counter|x_reg\(7) & (!\pixel_counter|Add0~13\)) # (!\pixel_counter|x_reg\(7) & ((\pixel_counter|Add0~13\) # (GND)))
-- \pixel_counter|Add0~15\ = CARRY((!\pixel_counter|Add0~13\) # (!\pixel_counter|x_reg\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \pixel_counter|x_reg\(7),
	datad => VCC,
	cin => \pixel_counter|Add0~13\,
	combout => \pixel_counter|Add0~14_combout\,
	cout => \pixel_counter|Add0~15\);

-- Location: LCCOMB_X20_Y10_N28
\pixel_counter|x_next[7]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|x_next[7]~5_combout\ = (\pixel_counter|Add0~14_combout\ & \pixel_counter|LessThan0~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \pixel_counter|Add0~14_combout\,
	datad => \pixel_counter|LessThan0~1_combout\,
	combout => \pixel_counter|x_next[7]~5_combout\);

-- Location: LCFF_X20_Y10_N29
\pixel_counter|x_reg[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \pixel_counter|x_next[7]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pixel_counter|x_reg\(7));

-- Location: LCCOMB_X19_Y10_N18
\pixel_counter|Add0~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|Add0~16_combout\ = (\pixel_counter|x_reg\(8) & (\pixel_counter|Add0~15\ $ (GND))) # (!\pixel_counter|x_reg\(8) & (!\pixel_counter|Add0~15\ & VCC))
-- \pixel_counter|Add0~17\ = CARRY((\pixel_counter|x_reg\(8) & !\pixel_counter|Add0~15\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \pixel_counter|x_reg\(8),
	datad => VCC,
	cin => \pixel_counter|Add0~15\,
	combout => \pixel_counter|Add0~16_combout\,
	cout => \pixel_counter|Add0~17\);

-- Location: LCCOMB_X20_Y10_N18
\pixel_counter|x_next[8]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|x_next[8]~4_combout\ = (\pixel_counter|Add0~16_combout\ & \pixel_counter|LessThan0~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \pixel_counter|Add0~16_combout\,
	datad => \pixel_counter|LessThan0~1_combout\,
	combout => \pixel_counter|x_next[8]~4_combout\);

-- Location: LCFF_X20_Y10_N19
\pixel_counter|x_reg[8]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \pixel_counter|x_next[8]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pixel_counter|x_reg\(8));

-- Location: LCCOMB_X19_Y10_N20
\pixel_counter|Add0~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|Add0~18_combout\ = \pixel_counter|x_reg\(9) $ (\pixel_counter|Add0~17\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \pixel_counter|x_reg\(9),
	cin => \pixel_counter|Add0~17\,
	combout => \pixel_counter|Add0~18_combout\);

-- Location: LCCOMB_X20_Y10_N0
\pixel_counter|x_next[9]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|x_next[9]~3_combout\ = (\pixel_counter|Add0~18_combout\ & \pixel_counter|LessThan0~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \pixel_counter|Add0~18_combout\,
	datad => \pixel_counter|LessThan0~1_combout\,
	combout => \pixel_counter|x_next[9]~3_combout\);

-- Location: LCFF_X20_Y10_N1
\pixel_counter|x_reg[9]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \pixel_counter|x_next[9]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pixel_counter|x_reg\(9));

-- Location: LCCOMB_X20_Y10_N26
\pixel_counter|Equal0~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|Equal0~1_combout\ = (\pixel_counter|x_reg\(9) & \pixel_counter|x_reg\(8))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \pixel_counter|x_reg\(9),
	datad => \pixel_counter|x_reg\(8),
	combout => \pixel_counter|Equal0~1_combout\);

-- Location: LCCOMB_X19_Y10_N24
\pixel_counter|x_next[1]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|x_next[1]~0_combout\ = (\pixel_counter|Add0~2_combout\ & \pixel_counter|LessThan0~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \pixel_counter|Add0~2_combout\,
	datad => \pixel_counter|LessThan0~1_combout\,
	combout => \pixel_counter|x_next[1]~0_combout\);

-- Location: LCFF_X19_Y10_N13
\pixel_counter|x_reg[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	sdata => \pixel_counter|x_next[1]~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pixel_counter|x_reg\(1));

-- Location: LCCOMB_X20_Y10_N16
\pixel_counter|Equal0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|Equal0~2_combout\ = (\pixel_counter|x_reg\(3) & (\pixel_counter|x_reg\(2) & (\pixel_counter|x_reg\(4) & !\pixel_counter|x_reg\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|x_reg\(3),
	datab => \pixel_counter|x_reg\(2),
	datac => \pixel_counter|x_reg\(4),
	datad => \pixel_counter|x_reg\(1),
	combout => \pixel_counter|Equal0~2_combout\);

-- Location: LCCOMB_X20_Y10_N22
\pixel_counter|Equal0~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|Equal0~3_combout\ = (\pixel_counter|Equal0~0_combout\ & (\pixel_counter|Equal0~1_combout\ & (\pixel_counter|Equal0~2_combout\ & \pixel_counter|x_reg\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|Equal0~0_combout\,
	datab => \pixel_counter|Equal0~1_combout\,
	datac => \pixel_counter|Equal0~2_combout\,
	datad => \pixel_counter|x_reg\(0),
	combout => \pixel_counter|Equal0~3_combout\);

-- Location: LCFF_X13_Y10_N17
\pixel_counter|y_reg[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \pixel_counter|y_reg[3]~16_combout\,
	sclr => \pixel_counter|LessThan1~2_combout\,
	ena => \pixel_counter|Equal0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pixel_counter|y_reg\(3));

-- Location: LCCOMB_X13_Y10_N30
\pixel_counter|LessThan1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|LessThan1~0_combout\ = (!\pixel_counter|y_reg\(5) & (!\pixel_counter|y_reg\(6) & ((!\pixel_counter|y_reg\(3)) # (!\pixel_counter|y_reg\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|y_reg\(5),
	datab => \pixel_counter|y_reg\(2),
	datac => \pixel_counter|y_reg\(3),
	datad => \pixel_counter|y_reg\(6),
	combout => \pixel_counter|LessThan1~0_combout\);

-- Location: LCCOMB_X13_Y10_N24
\pixel_counter|y_reg[7]~24\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|y_reg[7]~24_combout\ = (\pixel_counter|y_reg\(7) & (!\pixel_counter|y_reg[6]~23\)) # (!\pixel_counter|y_reg\(7) & ((\pixel_counter|y_reg[6]~23\) # (GND)))
-- \pixel_counter|y_reg[7]~25\ = CARRY((!\pixel_counter|y_reg[6]~23\) # (!\pixel_counter|y_reg\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|y_reg\(7),
	datad => VCC,
	cin => \pixel_counter|y_reg[6]~23\,
	combout => \pixel_counter|y_reg[7]~24_combout\,
	cout => \pixel_counter|y_reg[7]~25\);

-- Location: LCFF_X13_Y10_N25
\pixel_counter|y_reg[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \pixel_counter|y_reg[7]~24_combout\,
	sclr => \pixel_counter|LessThan1~2_combout\,
	ena => \pixel_counter|Equal0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pixel_counter|y_reg\(7));

-- Location: LCCOMB_X13_Y10_N8
\pixel_counter|LessThan1~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|LessThan1~1_combout\ = (!\pixel_counter|y_reg\(8) & (!\pixel_counter|y_reg\(7) & !\pixel_counter|y_reg\(4)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \pixel_counter|y_reg\(8),
	datac => \pixel_counter|y_reg\(7),
	datad => \pixel_counter|y_reg\(4),
	combout => \pixel_counter|LessThan1~1_combout\);

-- Location: LCCOMB_X13_Y10_N26
\pixel_counter|y_reg[8]~26\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|y_reg[8]~26_combout\ = (\pixel_counter|y_reg\(8) & (\pixel_counter|y_reg[7]~25\ $ (GND))) # (!\pixel_counter|y_reg\(8) & (!\pixel_counter|y_reg[7]~25\ & VCC))
-- \pixel_counter|y_reg[8]~27\ = CARRY((\pixel_counter|y_reg\(8) & !\pixel_counter|y_reg[7]~25\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \pixel_counter|y_reg\(8),
	datad => VCC,
	cin => \pixel_counter|y_reg[7]~25\,
	combout => \pixel_counter|y_reg[8]~26_combout\,
	cout => \pixel_counter|y_reg[8]~27\);

-- Location: LCCOMB_X13_Y10_N28
\pixel_counter|y_reg[9]~28\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|y_reg[9]~28_combout\ = \pixel_counter|y_reg[8]~27\ $ (\pixel_counter|y_reg\(9))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \pixel_counter|y_reg\(9),
	cin => \pixel_counter|y_reg[8]~27\,
	combout => \pixel_counter|y_reg[9]~28_combout\);

-- Location: LCFF_X13_Y10_N29
\pixel_counter|y_reg[9]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \pixel_counter|y_reg[9]~28_combout\,
	sclr => \pixel_counter|LessThan1~2_combout\,
	ena => \pixel_counter|Equal0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pixel_counter|y_reg\(9));

-- Location: LCCOMB_X13_Y10_N2
\pixel_counter|LessThan1~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|LessThan1~2_combout\ = (\pixel_counter|y_reg\(9) & ((!\pixel_counter|LessThan1~1_combout\) # (!\pixel_counter|LessThan1~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \pixel_counter|LessThan1~0_combout\,
	datac => \pixel_counter|LessThan1~1_combout\,
	datad => \pixel_counter|y_reg\(9),
	combout => \pixel_counter|LessThan1~2_combout\);

-- Location: LCFF_X13_Y10_N15
\pixel_counter|y_reg[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \pixel_counter|y_reg[2]~14_combout\,
	sclr => \pixel_counter|LessThan1~2_combout\,
	ena => \pixel_counter|Equal0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pixel_counter|y_reg\(2));

-- Location: LCCOMB_X13_Y10_N18
\pixel_counter|y_reg[4]~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|y_reg[4]~18_combout\ = (\pixel_counter|y_reg\(4) & (\pixel_counter|y_reg[3]~17\ $ (GND))) # (!\pixel_counter|y_reg\(4) & (!\pixel_counter|y_reg[3]~17\ & VCC))
-- \pixel_counter|y_reg[4]~19\ = CARRY((\pixel_counter|y_reg\(4) & !\pixel_counter|y_reg[3]~17\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \pixel_counter|y_reg\(4),
	datad => VCC,
	cin => \pixel_counter|y_reg[3]~17\,
	combout => \pixel_counter|y_reg[4]~18_combout\,
	cout => \pixel_counter|y_reg[4]~19\);

-- Location: LCFF_X13_Y10_N19
\pixel_counter|y_reg[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \pixel_counter|y_reg[4]~18_combout\,
	sclr => \pixel_counter|LessThan1~2_combout\,
	ena => \pixel_counter|Equal0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pixel_counter|y_reg\(4));

-- Location: LCCOMB_X13_Y10_N20
\pixel_counter|y_reg[5]~20\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|y_reg[5]~20_combout\ = (\pixel_counter|y_reg\(5) & (!\pixel_counter|y_reg[4]~19\)) # (!\pixel_counter|y_reg\(5) & ((\pixel_counter|y_reg[4]~19\) # (GND)))
-- \pixel_counter|y_reg[5]~21\ = CARRY((!\pixel_counter|y_reg[4]~19\) # (!\pixel_counter|y_reg\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|y_reg\(5),
	datad => VCC,
	cin => \pixel_counter|y_reg[4]~19\,
	combout => \pixel_counter|y_reg[5]~20_combout\,
	cout => \pixel_counter|y_reg[5]~21\);

-- Location: LCCOMB_X13_Y10_N22
\pixel_counter|y_reg[6]~22\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|y_reg[6]~22_combout\ = (\pixel_counter|y_reg\(6) & (\pixel_counter|y_reg[5]~21\ $ (GND))) # (!\pixel_counter|y_reg\(6) & (!\pixel_counter|y_reg[5]~21\ & VCC))
-- \pixel_counter|y_reg[6]~23\ = CARRY((\pixel_counter|y_reg\(6) & !\pixel_counter|y_reg[5]~21\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \pixel_counter|y_reg\(6),
	datad => VCC,
	cin => \pixel_counter|y_reg[5]~21\,
	combout => \pixel_counter|y_reg[6]~22_combout\,
	cout => \pixel_counter|y_reg[6]~23\);

-- Location: LCFF_X13_Y10_N23
\pixel_counter|y_reg[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \pixel_counter|y_reg[6]~22_combout\,
	sclr => \pixel_counter|LessThan1~2_combout\,
	ena => \pixel_counter|Equal0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pixel_counter|y_reg\(6));

-- Location: LCFF_X13_Y10_N27
\pixel_counter|y_reg[8]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \pixel_counter|y_reg[8]~26_combout\,
	sclr => \pixel_counter|LessThan1~2_combout\,
	ena => \pixel_counter|Equal0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pixel_counter|y_reg\(8));

-- Location: LCCOMB_X14_Y10_N4
\from640x480to160x120|Add0~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \from640x480to160x120|Add0~4_combout\ = ((\pixel_counter|y_reg\(6) $ (\pixel_counter|y_reg\(4) $ (!\from640x480to160x120|Add0~3\)))) # (GND)
-- \from640x480to160x120|Add0~5\ = CARRY((\pixel_counter|y_reg\(6) & ((\pixel_counter|y_reg\(4)) # (!\from640x480to160x120|Add0~3\))) # (!\pixel_counter|y_reg\(6) & (\pixel_counter|y_reg\(4) & !\from640x480to160x120|Add0~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|y_reg\(6),
	datab => \pixel_counter|y_reg\(4),
	datad => VCC,
	cin => \from640x480to160x120|Add0~3\,
	combout => \from640x480to160x120|Add0~4_combout\,
	cout => \from640x480to160x120|Add0~5\);

-- Location: LCCOMB_X14_Y10_N6
\from640x480to160x120|Add0~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \from640x480to160x120|Add0~6_combout\ = (\pixel_counter|y_reg\(5) & ((\pixel_counter|y_reg\(7) & (\from640x480to160x120|Add0~5\ & VCC)) # (!\pixel_counter|y_reg\(7) & (!\from640x480to160x120|Add0~5\)))) # (!\pixel_counter|y_reg\(5) & 
-- ((\pixel_counter|y_reg\(7) & (!\from640x480to160x120|Add0~5\)) # (!\pixel_counter|y_reg\(7) & ((\from640x480to160x120|Add0~5\) # (GND)))))
-- \from640x480to160x120|Add0~7\ = CARRY((\pixel_counter|y_reg\(5) & (!\pixel_counter|y_reg\(7) & !\from640x480to160x120|Add0~5\)) # (!\pixel_counter|y_reg\(5) & ((!\from640x480to160x120|Add0~5\) # (!\pixel_counter|y_reg\(7)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|y_reg\(5),
	datab => \pixel_counter|y_reg\(7),
	datad => VCC,
	cin => \from640x480to160x120|Add0~5\,
	combout => \from640x480to160x120|Add0~6_combout\,
	cout => \from640x480to160x120|Add0~7\);

-- Location: LCCOMB_X14_Y10_N14
\from640x480to160x120|Add0~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \from640x480to160x120|Add0~14_combout\ = \from640x480to160x120|Add0~13\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \from640x480to160x120|Add0~13\,
	combout => \from640x480to160x120|Add0~14_combout\);

-- Location: LCCOMB_X18_Y10_N0
\pixel_counter|Add2~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|Add2~1_cout\ = CARRY((\pixel_counter|x_next[0]~1_combout\ & \pixel_counter|x_next[1]~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|x_next[0]~1_combout\,
	datab => \pixel_counter|x_next[1]~0_combout\,
	datad => VCC,
	cout => \pixel_counter|Add2~1_cout\);

-- Location: LCCOMB_X18_Y10_N2
\pixel_counter|Add2~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|Add2~2_combout\ = (\pixel_counter|Add2~1_cout\ & (((!\pixel_counter|LessThan0~1_combout\)) # (!\pixel_counter|Add0~4_combout\))) # (!\pixel_counter|Add2~1_cout\ & (((\pixel_counter|Add0~4_combout\ & \pixel_counter|LessThan0~1_combout\)) # 
-- (GND)))
-- \pixel_counter|Add2~3\ = CARRY(((!\pixel_counter|Add2~1_cout\) # (!\pixel_counter|LessThan0~1_combout\)) # (!\pixel_counter|Add0~4_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100001111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|Add0~4_combout\,
	datab => \pixel_counter|LessThan0~1_combout\,
	datad => VCC,
	cin => \pixel_counter|Add2~1_cout\,
	combout => \pixel_counter|Add2~2_combout\,
	cout => \pixel_counter|Add2~3\);

-- Location: LCCOMB_X18_Y10_N4
\pixel_counter|Add2~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|Add2~4_combout\ = (\pixel_counter|Add2~3\ & (\pixel_counter|LessThan0~1_combout\ & (\pixel_counter|Add0~6_combout\ & VCC))) # (!\pixel_counter|Add2~3\ & ((((\pixel_counter|LessThan0~1_combout\ & \pixel_counter|Add0~6_combout\)))))
-- \pixel_counter|Add2~5\ = CARRY((\pixel_counter|LessThan0~1_combout\ & (\pixel_counter|Add0~6_combout\ & !\pixel_counter|Add2~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000011100001000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|LessThan0~1_combout\,
	datab => \pixel_counter|Add0~6_combout\,
	datad => VCC,
	cin => \pixel_counter|Add2~3\,
	combout => \pixel_counter|Add2~4_combout\,
	cout => \pixel_counter|Add2~5\);

-- Location: LCCOMB_X18_Y10_N6
\pixel_counter|Add2~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|Add2~6_combout\ = (\pixel_counter|LessThan0~1_combout\ & ((\pixel_counter|Add0~8_combout\ & (\pixel_counter|Add2~5\ & VCC)) # (!\pixel_counter|Add0~8_combout\ & (!\pixel_counter|Add2~5\)))) # (!\pixel_counter|LessThan0~1_combout\ & 
-- (((!\pixel_counter|Add2~5\))))
-- \pixel_counter|Add2~7\ = CARRY((!\pixel_counter|Add2~5\ & ((!\pixel_counter|Add0~8_combout\) # (!\pixel_counter|LessThan0~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000011100000111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|LessThan0~1_combout\,
	datab => \pixel_counter|Add0~8_combout\,
	datad => VCC,
	cin => \pixel_counter|Add2~5\,
	combout => \pixel_counter|Add2~6_combout\,
	cout => \pixel_counter|Add2~7\);

-- Location: LCCOMB_X18_Y10_N8
\pixel_counter|Add2~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|Add2~8_combout\ = (\pixel_counter|Add2~7\ & ((((\pixel_counter|LessThan0~1_combout\ & \pixel_counter|Add0~10_combout\))))) # (!\pixel_counter|Add2~7\ & (((\pixel_counter|LessThan0~1_combout\ & \pixel_counter|Add0~10_combout\)) # (GND)))
-- \pixel_counter|Add2~9\ = CARRY(((\pixel_counter|LessThan0~1_combout\ & \pixel_counter|Add0~10_combout\)) # (!\pixel_counter|Add2~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100010001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|LessThan0~1_combout\,
	datab => \pixel_counter|Add0~10_combout\,
	datad => VCC,
	cin => \pixel_counter|Add2~7\,
	combout => \pixel_counter|Add2~8_combout\,
	cout => \pixel_counter|Add2~9\);

-- Location: LCCOMB_X18_Y10_N10
\pixel_counter|Add2~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|Add2~10_combout\ = (\pixel_counter|LessThan0~1_combout\ & ((\pixel_counter|Add0~12_combout\ & (\pixel_counter|Add2~9\ & VCC)) # (!\pixel_counter|Add0~12_combout\ & (!\pixel_counter|Add2~9\)))) # (!\pixel_counter|LessThan0~1_combout\ & 
-- (((!\pixel_counter|Add2~9\))))
-- \pixel_counter|Add2~11\ = CARRY((!\pixel_counter|Add2~9\ & ((!\pixel_counter|Add0~12_combout\) # (!\pixel_counter|LessThan0~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000011100000111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|LessThan0~1_combout\,
	datab => \pixel_counter|Add0~12_combout\,
	datad => VCC,
	cin => \pixel_counter|Add2~9\,
	combout => \pixel_counter|Add2~10_combout\,
	cout => \pixel_counter|Add2~11\);

-- Location: LCCOMB_X18_Y10_N12
\pixel_counter|Add2~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|Add2~12_combout\ = (\pixel_counter|Add2~11\ & (\pixel_counter|Add0~14_combout\ & (\pixel_counter|LessThan0~1_combout\ & VCC))) # (!\pixel_counter|Add2~11\ & ((((\pixel_counter|Add0~14_combout\ & \pixel_counter|LessThan0~1_combout\)))))
-- \pixel_counter|Add2~13\ = CARRY((\pixel_counter|Add0~14_combout\ & (\pixel_counter|LessThan0~1_combout\ & !\pixel_counter|Add2~11\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000011100001000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|Add0~14_combout\,
	datab => \pixel_counter|LessThan0~1_combout\,
	datad => VCC,
	cin => \pixel_counter|Add2~11\,
	combout => \pixel_counter|Add2~12_combout\,
	cout => \pixel_counter|Add2~13\);

-- Location: LCCOMB_X18_Y10_N14
\pixel_counter|Add2~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|Add2~14_combout\ = (\pixel_counter|LessThan0~1_combout\ & ((\pixel_counter|Add0~16_combout\ & (\pixel_counter|Add2~13\ & VCC)) # (!\pixel_counter|Add0~16_combout\ & (!\pixel_counter|Add2~13\)))) # (!\pixel_counter|LessThan0~1_combout\ & 
-- (((!\pixel_counter|Add2~13\))))
-- \pixel_counter|Add2~15\ = CARRY((!\pixel_counter|Add2~13\ & ((!\pixel_counter|Add0~16_combout\) # (!\pixel_counter|LessThan0~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000011100000111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|LessThan0~1_combout\,
	datab => \pixel_counter|Add0~16_combout\,
	datad => VCC,
	cin => \pixel_counter|Add2~13\,
	combout => \pixel_counter|Add2~14_combout\,
	cout => \pixel_counter|Add2~15\);

-- Location: LCCOMB_X18_Y10_N16
\pixel_counter|Add2~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|Add2~16_combout\ = \pixel_counter|Add2~15\ $ (((\pixel_counter|Add0~18_combout\ & \pixel_counter|LessThan0~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \pixel_counter|Add0~18_combout\,
	datad => \pixel_counter|LessThan0~1_combout\,
	cin => \pixel_counter|Add2~15\,
	combout => \pixel_counter|Add2~16_combout\);

-- Location: LCCOMB_X15_Y10_N4
\from640x480to160x120|adr[5]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \from640x480to160x120|adr[5]~0_combout\ = (\pixel_counter|y_reg\(2) & (\pixel_counter|Add2~12_combout\ $ (VCC))) # (!\pixel_counter|y_reg\(2) & (\pixel_counter|Add2~12_combout\ & VCC))
-- \from640x480to160x120|adr[5]~1\ = CARRY((\pixel_counter|y_reg\(2) & \pixel_counter|Add2~12_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|y_reg\(2),
	datab => \pixel_counter|Add2~12_combout\,
	datad => VCC,
	combout => \from640x480to160x120|adr[5]~0_combout\,
	cout => \from640x480to160x120|adr[5]~1\);

-- Location: LCCOMB_X15_Y10_N6
\from640x480to160x120|adr[6]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \from640x480to160x120|adr[6]~2_combout\ = (\pixel_counter|y_reg\(3) & ((\pixel_counter|Add2~14_combout\ & (\from640x480to160x120|adr[5]~1\ & VCC)) # (!\pixel_counter|Add2~14_combout\ & (!\from640x480to160x120|adr[5]~1\)))) # (!\pixel_counter|y_reg\(3) & 
-- ((\pixel_counter|Add2~14_combout\ & (!\from640x480to160x120|adr[5]~1\)) # (!\pixel_counter|Add2~14_combout\ & ((\from640x480to160x120|adr[5]~1\) # (GND)))))
-- \from640x480to160x120|adr[6]~3\ = CARRY((\pixel_counter|y_reg\(3) & (!\pixel_counter|Add2~14_combout\ & !\from640x480to160x120|adr[5]~1\)) # (!\pixel_counter|y_reg\(3) & ((!\from640x480to160x120|adr[5]~1\) # (!\pixel_counter|Add2~14_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000010111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|y_reg\(3),
	datab => \pixel_counter|Add2~14_combout\,
	datad => VCC,
	cin => \from640x480to160x120|adr[5]~1\,
	combout => \from640x480to160x120|adr[6]~2_combout\,
	cout => \from640x480to160x120|adr[6]~3\);

-- Location: LCCOMB_X15_Y10_N8
\from640x480to160x120|adr[7]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \from640x480to160x120|adr[7]~4_combout\ = ((\from640x480to160x120|Add0~0_combout\ $ (\pixel_counter|Add2~16_combout\ $ (!\from640x480to160x120|adr[6]~3\)))) # (GND)
-- \from640x480to160x120|adr[7]~5\ = CARRY((\from640x480to160x120|Add0~0_combout\ & ((\pixel_counter|Add2~16_combout\) # (!\from640x480to160x120|adr[6]~3\))) # (!\from640x480to160x120|Add0~0_combout\ & (\pixel_counter|Add2~16_combout\ & 
-- !\from640x480to160x120|adr[6]~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \from640x480to160x120|Add0~0_combout\,
	datab => \pixel_counter|Add2~16_combout\,
	datad => VCC,
	cin => \from640x480to160x120|adr[6]~3\,
	combout => \from640x480to160x120|adr[7]~4_combout\,
	cout => \from640x480to160x120|adr[7]~5\);

-- Location: LCCOMB_X15_Y10_N10
\from640x480to160x120|adr[8]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \from640x480to160x120|adr[8]~6_combout\ = (\from640x480to160x120|Add0~2_combout\ & (!\from640x480to160x120|adr[7]~5\)) # (!\from640x480to160x120|Add0~2_combout\ & ((\from640x480to160x120|adr[7]~5\) # (GND)))
-- \from640x480to160x120|adr[8]~7\ = CARRY((!\from640x480to160x120|adr[7]~5\) # (!\from640x480to160x120|Add0~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \from640x480to160x120|Add0~2_combout\,
	datad => VCC,
	cin => \from640x480to160x120|adr[7]~5\,
	combout => \from640x480to160x120|adr[8]~6_combout\,
	cout => \from640x480to160x120|adr[8]~7\);

-- Location: LCCOMB_X15_Y10_N12
\from640x480to160x120|adr[9]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \from640x480to160x120|adr[9]~8_combout\ = (\from640x480to160x120|Add0~4_combout\ & (\from640x480to160x120|adr[8]~7\ $ (GND))) # (!\from640x480to160x120|Add0~4_combout\ & (!\from640x480to160x120|adr[8]~7\ & VCC))
-- \from640x480to160x120|adr[9]~9\ = CARRY((\from640x480to160x120|Add0~4_combout\ & !\from640x480to160x120|adr[8]~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \from640x480to160x120|Add0~4_combout\,
	datad => VCC,
	cin => \from640x480to160x120|adr[8]~7\,
	combout => \from640x480to160x120|adr[9]~8_combout\,
	cout => \from640x480to160x120|adr[9]~9\);

-- Location: LCCOMB_X15_Y10_N14
\from640x480to160x120|adr[10]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \from640x480to160x120|adr[10]~10_combout\ = (\from640x480to160x120|Add0~6_combout\ & (!\from640x480to160x120|adr[9]~9\)) # (!\from640x480to160x120|Add0~6_combout\ & ((\from640x480to160x120|adr[9]~9\) # (GND)))
-- \from640x480to160x120|adr[10]~11\ = CARRY((!\from640x480to160x120|adr[9]~9\) # (!\from640x480to160x120|Add0~6_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \from640x480to160x120|Add0~6_combout\,
	datad => VCC,
	cin => \from640x480to160x120|adr[9]~9\,
	combout => \from640x480to160x120|adr[10]~10_combout\,
	cout => \from640x480to160x120|adr[10]~11\);

-- Location: LCCOMB_X15_Y10_N16
\from640x480to160x120|adr[11]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \from640x480to160x120|adr[11]~12_combout\ = (\from640x480to160x120|Add0~8_combout\ & (\from640x480to160x120|adr[10]~11\ $ (GND))) # (!\from640x480to160x120|Add0~8_combout\ & (!\from640x480to160x120|adr[10]~11\ & VCC))
-- \from640x480to160x120|adr[11]~13\ = CARRY((\from640x480to160x120|Add0~8_combout\ & !\from640x480to160x120|adr[10]~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \from640x480to160x120|Add0~8_combout\,
	datad => VCC,
	cin => \from640x480to160x120|adr[10]~11\,
	combout => \from640x480to160x120|adr[11]~12_combout\,
	cout => \from640x480to160x120|adr[11]~13\);

-- Location: LCCOMB_X15_Y10_N18
\from640x480to160x120|adr[12]~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \from640x480to160x120|adr[12]~14_combout\ = (\from640x480to160x120|Add0~10_combout\ & (!\from640x480to160x120|adr[11]~13\)) # (!\from640x480to160x120|Add0~10_combout\ & ((\from640x480to160x120|adr[11]~13\) # (GND)))
-- \from640x480to160x120|adr[12]~15\ = CARRY((!\from640x480to160x120|adr[11]~13\) # (!\from640x480to160x120|Add0~10_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \from640x480to160x120|Add0~10_combout\,
	datad => VCC,
	cin => \from640x480to160x120|adr[11]~13\,
	combout => \from640x480to160x120|adr[12]~14_combout\,
	cout => \from640x480to160x120|adr[12]~15\);

-- Location: LCCOMB_X15_Y10_N20
\from640x480to160x120|adr[13]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \from640x480to160x120|adr[13]~16_combout\ = (\from640x480to160x120|Add0~12_combout\ & (\from640x480to160x120|adr[12]~15\ $ (GND))) # (!\from640x480to160x120|Add0~12_combout\ & (!\from640x480to160x120|adr[12]~15\ & VCC))
-- \from640x480to160x120|adr[13]~17\ = CARRY((\from640x480to160x120|Add0~12_combout\ & !\from640x480to160x120|adr[12]~15\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \from640x480to160x120|Add0~12_combout\,
	datad => VCC,
	cin => \from640x480to160x120|adr[12]~15\,
	combout => \from640x480to160x120|adr[13]~16_combout\,
	cout => \from640x480to160x120|adr[13]~17\);

-- Location: LCCOMB_X15_Y10_N22
\from640x480to160x120|adr[14]~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \from640x480to160x120|adr[14]~18_combout\ = \from640x480to160x120|adr[13]~17\ $ (\from640x480to160x120|Add0~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \from640x480to160x120|Add0~14_combout\,
	cin => \from640x480to160x120|adr[13]~17\,
	combout => \from640x480to160x120|adr[14]~18_combout\);

-- Location: LCCOMB_X15_Y10_N28
\vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode297w[3]\ : cycloneii_lcell_comb
-- Equation(s):
-- \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode297w\(3) = (!\from640x480to160x120|adr[14]~18_combout\ & (!\from640x480to160x120|adr[13]~16_combout\ & !\from640x480to160x120|adr[12]~14_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \from640x480to160x120|adr[14]~18_combout\,
	datac => \from640x480to160x120|adr[13]~16_combout\,
	datad => \from640x480to160x120|adr[12]~14_combout\,
	combout => \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode297w\(3));

-- Location: PIN_79,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
\color[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_color(0),
	combout => \color~combout\(0));

-- Location: LCCOMB_X17_Y9_N4
\clear_mux|LPM_MUX_component|auto_generated|result_node[15]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_mux|LPM_MUX_component|auto_generated|result_node[15]~3_combout\ = (\clear~combout\ & (\clear_screen|adr\(0))) # (!\clear~combout\ & ((\color~combout\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \clear~combout\,
	datac => \clear_screen|adr\(0),
	datad => \color~combout\(0),
	combout => \clear_mux|LPM_MUX_component|auto_generated|result_node[15]~3_combout\);

-- Location: PIN_87,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
\adr[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_adr(0),
	combout => \adr~combout\(0));

-- Location: LCCOMB_X19_Y9_N10
\clear_mux|LPM_MUX_component|auto_generated|result_node[0]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_mux|LPM_MUX_component|auto_generated|result_node[0]~4_combout\ = (\clear~combout\ & (\clear_screen|adr\(0))) # (!\clear~combout\ & ((\adr~combout\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \clear~combout\,
	datac => \clear_screen|adr\(0),
	datad => \adr~combout\(0),
	combout => \clear_mux|LPM_MUX_component|auto_generated|result_node[0]~4_combout\);

-- Location: PIN_88,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
\adr[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_adr(1),
	combout => \adr~combout\(1));

-- Location: LCCOMB_X19_Y9_N16
\clear_mux|LPM_MUX_component|auto_generated|result_node[1]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_mux|LPM_MUX_component|auto_generated|result_node[1]~5_combout\ = (\clear~combout\ & ((\clear_screen|adr\(1)))) # (!\clear~combout\ & (\adr~combout\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \clear~combout\,
	datac => \adr~combout\(1),
	datad => \clear_screen|adr\(1),
	combout => \clear_mux|LPM_MUX_component|auto_generated|result_node[1]~5_combout\);

-- Location: PIN_89,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
\adr[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_adr(2),
	combout => \adr~combout\(2));

-- Location: LCCOMB_X19_Y9_N18
\clear_mux|LPM_MUX_component|auto_generated|result_node[2]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_mux|LPM_MUX_component|auto_generated|result_node[2]~6_combout\ = (\clear~combout\ & (\clear_screen|adr\(2))) # (!\clear~combout\ & ((\adr~combout\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \clear~combout\,
	datac => \clear_screen|adr\(2),
	datad => \adr~combout\(2),
	combout => \clear_mux|LPM_MUX_component|auto_generated|result_node[2]~6_combout\);

-- Location: LCFF_X18_Y9_N7
\clear_screen|adr[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|ALT_INV__clk1~clkctrl_outclk\,
	datain => \clear_screen|adr[3]~21_combout\,
	sclr => \clear_screen|LessThan0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \clear_screen|adr\(3));

-- Location: PIN_90,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
\adr[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_adr(3),
	combout => \adr~combout\(3));

-- Location: LCCOMB_X19_Y9_N8
\clear_mux|LPM_MUX_component|auto_generated|result_node[3]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_mux|LPM_MUX_component|auto_generated|result_node[3]~7_combout\ = (\clear~combout\ & (\clear_screen|adr\(3))) # (!\clear~combout\ & ((\adr~combout\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \clear~combout\,
	datac => \clear_screen|adr\(3),
	datad => \adr~combout\(3),
	combout => \clear_mux|LPM_MUX_component|auto_generated|result_node[3]~7_combout\);

-- Location: PIN_91,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
\adr[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_adr(4),
	combout => \adr~combout\(4));

-- Location: LCCOMB_X17_Y9_N10
\clear_mux|LPM_MUX_component|auto_generated|result_node[4]~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_mux|LPM_MUX_component|auto_generated|result_node[4]~8_combout\ = (\clear~combout\ & (\clear_screen|adr\(4))) # (!\clear~combout\ & ((\adr~combout\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \clear~combout\,
	datac => \clear_screen|adr\(4),
	datad => \adr~combout\(4),
	combout => \clear_mux|LPM_MUX_component|auto_generated|result_node[4]~8_combout\);

-- Location: LCFF_X18_Y9_N11
\clear_screen|adr[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|ALT_INV__clk1~clkctrl_outclk\,
	datain => \clear_screen|adr[5]~25_combout\,
	sclr => \clear_screen|LessThan0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \clear_screen|adr\(5));

-- Location: PIN_92,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
\adr[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_adr(5),
	combout => \adr~combout\(5));

-- Location: LCCOMB_X17_Y9_N0
\clear_mux|LPM_MUX_component|auto_generated|result_node[5]~9\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_mux|LPM_MUX_component|auto_generated|result_node[5]~9_combout\ = (\clear~combout\ & (\clear_screen|adr\(5))) # (!\clear~combout\ & ((\adr~combout\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \clear~combout\,
	datac => \clear_screen|adr\(5),
	datad => \adr~combout\(5),
	combout => \clear_mux|LPM_MUX_component|auto_generated|result_node[5]~9_combout\);

-- Location: LCCOMB_X19_Y9_N30
\clear_mux|LPM_MUX_component|auto_generated|result_node[6]~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_mux|LPM_MUX_component|auto_generated|result_node[6]~10_combout\ = (\clear~combout\ & ((\clear_screen|adr\(6)))) # (!\clear~combout\ & (\adr~combout\(6)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \adr~combout\(6),
	datac => \clear~combout\,
	datad => \clear_screen|adr\(6),
	combout => \clear_mux|LPM_MUX_component|auto_generated|result_node[6]~10_combout\);

-- Location: PIN_94,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
\adr[7]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_adr(7),
	combout => \adr~combout\(7));

-- Location: LCCOMB_X17_Y9_N14
\clear_mux|LPM_MUX_component|auto_generated|result_node[7]~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_mux|LPM_MUX_component|auto_generated|result_node[7]~11_combout\ = (\clear~combout\ & (\clear_screen|adr\(7))) # (!\clear~combout\ & ((\adr~combout\(7))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \clear~combout\,
	datac => \clear_screen|adr\(7),
	datad => \adr~combout\(7),
	combout => \clear_mux|LPM_MUX_component|auto_generated|result_node[7]~11_combout\);

-- Location: LCFF_X18_Y9_N17
\clear_screen|adr[8]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|ALT_INV__clk1~clkctrl_outclk\,
	datain => \clear_screen|adr[8]~31_combout\,
	sclr => \clear_screen|LessThan0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \clear_screen|adr\(8));

-- Location: PIN_96,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
\adr[8]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_adr(8),
	combout => \adr~combout\(8));

-- Location: LCCOMB_X17_Y9_N24
\clear_mux|LPM_MUX_component|auto_generated|result_node[8]~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_mux|LPM_MUX_component|auto_generated|result_node[8]~12_combout\ = (\clear~combout\ & (\clear_screen|adr\(8))) # (!\clear~combout\ & ((\adr~combout\(8))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \clear~combout\,
	datac => \clear_screen|adr\(8),
	datad => \adr~combout\(8),
	combout => \clear_mux|LPM_MUX_component|auto_generated|result_node[8]~12_combout\);

-- Location: PIN_97,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
\adr[9]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_adr(9),
	combout => \adr~combout\(9));

-- Location: LCCOMB_X17_Y9_N18
\clear_mux|LPM_MUX_component|auto_generated|result_node[9]~13\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_mux|LPM_MUX_component|auto_generated|result_node[9]~13_combout\ = (\clear~combout\ & ((\clear_screen|adr\(9)))) # (!\clear~combout\ & (\adr~combout\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \clear~combout\,
	datac => \adr~combout\(9),
	datad => \clear_screen|adr\(9),
	combout => \clear_mux|LPM_MUX_component|auto_generated|result_node[9]~13_combout\);

-- Location: PIN_99,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
\adr[10]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_adr(10),
	combout => \adr~combout\(10));

-- Location: LCCOMB_X17_Y9_N8
\clear_mux|LPM_MUX_component|auto_generated|result_node[10]~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_mux|LPM_MUX_component|auto_generated|result_node[10]~14_combout\ = (\clear~combout\ & ((\clear_screen|adr\(10)))) # (!\clear~combout\ & (\adr~combout\(10)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \clear~combout\,
	datac => \adr~combout\(10),
	datad => \clear_screen|adr\(10),
	combout => \clear_mux|LPM_MUX_component|auto_generated|result_node[10]~14_combout\);

-- Location: PIN_100,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
\adr[11]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_adr(11),
	combout => \adr~combout\(11));

-- Location: LCCOMB_X17_Y9_N22
\clear_mux|LPM_MUX_component|auto_generated|result_node[11]~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_mux|LPM_MUX_component|auto_generated|result_node[11]~15_combout\ = (\clear~combout\ & ((\clear_screen|adr\(11)))) # (!\clear~combout\ & (\adr~combout\(11)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \clear~combout\,
	datab => \adr~combout\(11),
	datac => \clear_screen|adr\(11),
	combout => \clear_mux|LPM_MUX_component|auto_generated|result_node[11]~15_combout\);

-- Location: M4K_X11_Y5
\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a0\ : cycloneii_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vram160x120x4:vram|altsyncram:altsyncram_component|altsyncram_r5o1:auto_generated|altsyncram_m6m1:altsyncram1|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "bidir_dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clear => "none",
	port_a_byte_enable_clock => "none",
	port_a_data_in_clear => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 4,
	port_a_write_enable_clear => "none",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_byte_enable_clear => "none",
	port_b_data_in_clear => "none",
	port_b_data_in_clock => "clock1",
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 4,
	port_b_read_enable_write_enable_clear => "none",
	port_b_read_enable_write_enable_clock => "clock1",
	ram_block_type => "M4K",
	safe_write => "err_on_2clk")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbrewe => \vram|altsyncram_component|auto_generated|altsyncram1|decode4|w_anode297w\(3),
	clk0 => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	clk1 => \clear_mux|LPM_MUX_component|auto_generated|result_node[19]~clkctrl_outclk\,
	ena0 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode297w\(3),
	ena1 => \vram|altsyncram_component|auto_generated|altsyncram1|decode4|w_anode297w\(3),
	portadatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a0_PORTADATAIN_bus\,
	portbdatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a0_PORTBDATAIN_bus\,
	portaaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a0_PORTAADDR_bus\,
	portbaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a0_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portadataout => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a0_PORTADATAOUT_bus\);

-- Location: LCCOMB_X15_Y10_N0
\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a[0]~feeder\ : cycloneii_lcell_comb
-- Equation(s):
-- \vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a[0]~feeder_combout\ = \from640x480to160x120|adr[12]~14_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \from640x480to160x120|adr[12]~14_combout\,
	combout => \vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a[0]~feeder_combout\);

-- Location: LCFF_X15_Y10_N1
\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a[0]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(0));

-- Location: LCFF_X15_Y10_N21
\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \from640x480to160x120|adr[13]~16_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(1));

-- Location: LCCOMB_X15_Y9_N4
\vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs407w[0]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs407w[0]~0_combout\ = (\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(0) & ((\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a4~portadataout\) # 
-- ((\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(1))))) # (!\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(0) & (((\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a0~portadataout\ & 
-- !\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a4~portadataout\,
	datab => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a0~portadataout\,
	datac => \vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(0),
	datad => \vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(1),
	combout => \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs407w[0]~0_combout\);

-- Location: LCCOMB_X19_Y9_N26
\vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~2_combout\ = (\clear_mux|LPM_MUX_component|auto_generated|result_node[12]~2_combout\ & (!\clear_mux|LPM_MUX_component|auto_generated|result_node[14]~0_combout\ & 
-- \clear_mux|LPM_MUX_component|auto_generated|result_node[13]~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \clear_mux|LPM_MUX_component|auto_generated|result_node[12]~2_combout\,
	datac => \clear_mux|LPM_MUX_component|auto_generated|result_node[14]~0_combout\,
	datad => \clear_mux|LPM_MUX_component|auto_generated|result_node[13]~1_combout\,
	combout => \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~2_combout\);

-- Location: LCCOMB_X15_Y10_N2
\vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode334w[3]\ : cycloneii_lcell_comb
-- Equation(s):
-- \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode334w\(3) = (!\from640x480to160x120|adr[14]~18_combout\ & (\from640x480to160x120|adr[13]~16_combout\ & \from640x480to160x120|adr[12]~14_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \from640x480to160x120|adr[14]~18_combout\,
	datac => \from640x480to160x120|adr[13]~16_combout\,
	datad => \from640x480to160x120|adr[12]~14_combout\,
	combout => \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode334w\(3));

-- Location: M4K_X11_Y12
\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a12\ : cycloneii_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vram160x120x4:vram|altsyncram:altsyncram_component|altsyncram_r5o1:auto_generated|altsyncram_m6m1:altsyncram1|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "bidir_dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clear => "none",
	port_a_byte_enable_clock => "none",
	port_a_data_in_clear => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 4,
	port_a_write_enable_clear => "none",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_byte_enable_clear => "none",
	port_b_data_in_clear => "none",
	port_b_data_in_clock => "clock1",
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 4,
	port_b_read_enable_write_enable_clear => "none",
	port_b_read_enable_write_enable_clock => "clock1",
	ram_block_type => "M4K",
	safe_write => "err_on_2clk")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbrewe => \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~2_combout\,
	clk0 => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	clk1 => \clear_mux|LPM_MUX_component|auto_generated|result_node[19]~clkctrl_outclk\,
	ena0 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode334w\(3),
	ena1 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~2_combout\,
	portadatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a12_PORTADATAIN_bus\,
	portbdatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a12_PORTBDATAIN_bus\,
	portaaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a12_PORTAADDR_bus\,
	portbaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a12_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portadataout => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a12_PORTADATAOUT_bus\);

-- Location: LCCOMB_X15_Y9_N18
\vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs407w[0]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs407w[0]~1_combout\ = (\vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs407w[0]~0_combout\ & 
-- (((\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a12~portadataout\) # (!\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(1))))) # 
-- (!\vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs407w[0]~0_combout\ & (\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a8~portadataout\ & 
-- ((\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a8~portadataout\,
	datab => \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs407w[0]~0_combout\,
	datac => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a12~portadataout\,
	datad => \vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(1),
	combout => \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs407w[0]~1_combout\);

-- Location: LCFF_X15_Y10_N23
\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \from640x480to160x120|adr[14]~18_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(2));

-- Location: LCCOMB_X19_Y9_N20
\vram|altsyncram_component|auto_generated|altsyncram1|decode4|w_anode344w[3]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \vram|altsyncram_component|auto_generated|altsyncram1|decode4|w_anode344w[3]~0_combout\ = (!\clear_mux|LPM_MUX_component|auto_generated|result_node[12]~2_combout\ & (\clear_mux|LPM_MUX_component|auto_generated|result_node[14]~0_combout\ & 
-- !\clear_mux|LPM_MUX_component|auto_generated|result_node[13]~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \clear_mux|LPM_MUX_component|auto_generated|result_node[12]~2_combout\,
	datac => \clear_mux|LPM_MUX_component|auto_generated|result_node[14]~0_combout\,
	datad => \clear_mux|LPM_MUX_component|auto_generated|result_node[13]~1_combout\,
	combout => \vram|altsyncram_component|auto_generated|altsyncram1|decode4|w_anode344w[3]~0_combout\);

-- Location: LCCOMB_X15_Y10_N26
\vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode344w[3]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode344w[3]~0_combout\ = (\from640x480to160x120|adr[14]~18_combout\ & (!\from640x480to160x120|adr[13]~16_combout\ & !\from640x480to160x120|adr[12]~14_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \from640x480to160x120|adr[14]~18_combout\,
	datac => \from640x480to160x120|adr[13]~16_combout\,
	datad => \from640x480to160x120|adr[12]~14_combout\,
	combout => \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode344w[3]~0_combout\);

-- Location: M4K_X11_Y3
\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a16\ : cycloneii_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vram160x120x4:vram|altsyncram:altsyncram_component|altsyncram_r5o1:auto_generated|altsyncram_m6m1:altsyncram1|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "bidir_dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clear => "none",
	port_a_byte_enable_clock => "none",
	port_a_data_in_clear => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 4,
	port_a_write_enable_clear => "none",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_byte_enable_clear => "none",
	port_b_data_in_clear => "none",
	port_b_data_in_clock => "clock1",
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 4,
	port_b_read_enable_write_enable_clear => "none",
	port_b_read_enable_write_enable_clock => "clock1",
	ram_block_type => "M4K",
	safe_write => "err_on_2clk")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbrewe => \vram|altsyncram_component|auto_generated|altsyncram1|decode4|w_anode344w[3]~0_combout\,
	clk0 => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	clk1 => \clear_mux|LPM_MUX_component|auto_generated|result_node[19]~clkctrl_outclk\,
	ena0 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode344w[3]~0_combout\,
	ena1 => \vram|altsyncram_component|auto_generated|altsyncram1|decode4|w_anode344w[3]~0_combout\,
	portadatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a16_PORTADATAIN_bus\,
	portbdatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a16_PORTBDATAIN_bus\,
	portaaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a16_PORTAADDR_bus\,
	portbaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a16_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portadataout => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a16_PORTADATAOUT_bus\);

-- Location: LCCOMB_X15_Y9_N0
\vram|altsyncram_component|auto_generated|altsyncram1|mux5|muxlut_result0w~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \vram|altsyncram_component|auto_generated|altsyncram1|mux5|muxlut_result0w~0_combout\ = (\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(2) & ((\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a16~portadataout\))) # 
-- (!\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(2) & (\vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs407w[0]~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs407w[0]~1_combout\,
	datac => \vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(2),
	datad => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a16~portadataout\,
	combout => \vram|altsyncram_component|auto_generated|altsyncram1|mux5|muxlut_result0w~0_combout\);

-- Location: LCCOMB_X19_Y9_N6
\vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~1_combout\ = (\clear_mux|LPM_MUX_component|auto_generated|result_node[12]~2_combout\ & (!\clear_mux|LPM_MUX_component|auto_generated|result_node[14]~0_combout\ & 
-- !\clear_mux|LPM_MUX_component|auto_generated|result_node[13]~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \clear_mux|LPM_MUX_component|auto_generated|result_node[12]~2_combout\,
	datac => \clear_mux|LPM_MUX_component|auto_generated|result_node[14]~0_combout\,
	datad => \clear_mux|LPM_MUX_component|auto_generated|result_node[13]~1_combout\,
	combout => \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~1_combout\);

-- Location: LCCOMB_X15_Y10_N30
\vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode314w[3]\ : cycloneii_lcell_comb
-- Equation(s):
-- \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode314w\(3) = (!\from640x480to160x120|adr[14]~18_combout\ & (!\from640x480to160x120|adr[13]~16_combout\ & \from640x480to160x120|adr[12]~14_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \from640x480to160x120|adr[14]~18_combout\,
	datac => \from640x480to160x120|adr[13]~16_combout\,
	datad => \from640x480to160x120|adr[12]~14_combout\,
	combout => \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode314w\(3));

-- Location: LCCOMB_X19_Y9_N0
\clear_mux|LPM_MUX_component|auto_generated|result_node[16]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_mux|LPM_MUX_component|auto_generated|result_node[16]~16_combout\ = (\clear~combout\ & ((\clear_screen|adr\(1)))) # (!\clear~combout\ & (\color~combout\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \color~combout\(1),
	datab => \clear~combout\,
	datad => \clear_screen|adr\(1),
	combout => \clear_mux|LPM_MUX_component|auto_generated|result_node[16]~16_combout\);

-- Location: M4K_X11_Y8
\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a5\ : cycloneii_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vram160x120x4:vram|altsyncram:altsyncram_component|altsyncram_r5o1:auto_generated|altsyncram_m6m1:altsyncram1|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "bidir_dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clear => "none",
	port_a_byte_enable_clock => "none",
	port_a_data_in_clear => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 1,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 4,
	port_a_write_enable_clear => "none",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_byte_enable_clear => "none",
	port_b_data_in_clear => "none",
	port_b_data_in_clock => "clock1",
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 1,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 4,
	port_b_read_enable_write_enable_clear => "none",
	port_b_read_enable_write_enable_clock => "clock1",
	ram_block_type => "M4K",
	safe_write => "err_on_2clk")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbrewe => \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~1_combout\,
	clk0 => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	clk1 => \clear_mux|LPM_MUX_component|auto_generated|result_node[19]~clkctrl_outclk\,
	ena0 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode314w\(3),
	ena1 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~1_combout\,
	portadatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a5_PORTADATAIN_bus\,
	portbdatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a5_PORTBDATAIN_bus\,
	portaaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a5_PORTAADDR_bus\,
	portbaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a5_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portadataout => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a5_PORTADATAOUT_bus\);

-- Location: LCCOMB_X19_Y9_N24
\vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~0_combout\ = (!\clear_mux|LPM_MUX_component|auto_generated|result_node[12]~2_combout\ & (!\clear_mux|LPM_MUX_component|auto_generated|result_node[14]~0_combout\ & 
-- \clear_mux|LPM_MUX_component|auto_generated|result_node[13]~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \clear_mux|LPM_MUX_component|auto_generated|result_node[12]~2_combout\,
	datac => \clear_mux|LPM_MUX_component|auto_generated|result_node[14]~0_combout\,
	datad => \clear_mux|LPM_MUX_component|auto_generated|result_node[13]~1_combout\,
	combout => \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~0_combout\);

-- Location: LCCOMB_X15_Y10_N24
\vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode324w[3]\ : cycloneii_lcell_comb
-- Equation(s):
-- \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode324w\(3) = (!\from640x480to160x120|adr[14]~18_combout\ & (\from640x480to160x120|adr[13]~16_combout\ & !\from640x480to160x120|adr[12]~14_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \from640x480to160x120|adr[14]~18_combout\,
	datac => \from640x480to160x120|adr[13]~16_combout\,
	datad => \from640x480to160x120|adr[12]~14_combout\,
	combout => \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode324w\(3));

-- Location: M4K_X11_Y9
\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a9\ : cycloneii_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vram160x120x4:vram|altsyncram:altsyncram_component|altsyncram_r5o1:auto_generated|altsyncram_m6m1:altsyncram1|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "bidir_dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clear => "none",
	port_a_byte_enable_clock => "none",
	port_a_data_in_clear => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 1,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 4,
	port_a_write_enable_clear => "none",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_byte_enable_clear => "none",
	port_b_data_in_clear => "none",
	port_b_data_in_clock => "clock1",
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 1,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 4,
	port_b_read_enable_write_enable_clear => "none",
	port_b_read_enable_write_enable_clock => "clock1",
	ram_block_type => "M4K",
	safe_write => "err_on_2clk")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbrewe => \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~0_combout\,
	clk0 => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	clk1 => \clear_mux|LPM_MUX_component|auto_generated|result_node[19]~clkctrl_outclk\,
	ena0 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode324w\(3),
	ena1 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~0_combout\,
	portadatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a9_PORTADATAIN_bus\,
	portbdatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a9_PORTBDATAIN_bus\,
	portaaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a9_PORTAADDR_bus\,
	portbaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a9_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portadataout => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a9_PORTADATAOUT_bus\);

-- Location: LCCOMB_X15_Y9_N30
\vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs455w[0]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs455w[0]~0_combout\ = (\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(0) & (((\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(1))))) # 
-- (!\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(0) & ((\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(1) & ((\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a9~portadataout\))) # 
-- (!\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(1) & (\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a1~portadataout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a1~portadataout\,
	datab => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a9~portadataout\,
	datac => \vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(0),
	datad => \vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(1),
	combout => \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs455w[0]~0_combout\);

-- Location: LCCOMB_X15_Y9_N28
\vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs455w[0]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs455w[0]~1_combout\ = (\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(0) & 
-- ((\vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs455w[0]~0_combout\ & (\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a13~portadataout\)) # 
-- (!\vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs455w[0]~0_combout\ & ((\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a5~portadataout\))))) # 
-- (!\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(0) & (((\vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs455w[0]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a13~portadataout\,
	datab => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a5~portadataout\,
	datac => \vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(0),
	datad => \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs455w[0]~0_combout\,
	combout => \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs455w[0]~1_combout\);

-- Location: M4K_X11_Y7
\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a17\ : cycloneii_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vram160x120x4:vram|altsyncram:altsyncram_component|altsyncram_r5o1:auto_generated|altsyncram_m6m1:altsyncram1|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "bidir_dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clear => "none",
	port_a_byte_enable_clock => "none",
	port_a_data_in_clear => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 1,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 4,
	port_a_write_enable_clear => "none",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_byte_enable_clear => "none",
	port_b_data_in_clear => "none",
	port_b_data_in_clock => "clock1",
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 1,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 4,
	port_b_read_enable_write_enable_clear => "none",
	port_b_read_enable_write_enable_clock => "clock1",
	ram_block_type => "M4K",
	safe_write => "err_on_2clk")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbrewe => \vram|altsyncram_component|auto_generated|altsyncram1|decode4|w_anode344w[3]~0_combout\,
	clk0 => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	clk1 => \clear_mux|LPM_MUX_component|auto_generated|result_node[19]~clkctrl_outclk\,
	ena0 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode344w[3]~0_combout\,
	ena1 => \vram|altsyncram_component|auto_generated|altsyncram1|decode4|w_anode344w[3]~0_combout\,
	portadatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a17_PORTADATAIN_bus\,
	portbdatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a17_PORTBDATAIN_bus\,
	portaaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a17_PORTAADDR_bus\,
	portbaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a17_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portadataout => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a17_PORTADATAOUT_bus\);

-- Location: LCCOMB_X15_Y9_N10
\vram|altsyncram_component|auto_generated|altsyncram1|mux5|muxlut_result1w~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \vram|altsyncram_component|auto_generated|altsyncram1|mux5|muxlut_result1w~0_combout\ = (\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(2) & ((\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a17~portadataout\))) # 
-- (!\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(2) & (\vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs455w[0]~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs455w[0]~1_combout\,
	datac => \vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(2),
	datad => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a17~portadataout\,
	combout => \vram|altsyncram_component|auto_generated|altsyncram1|mux5|muxlut_result1w~0_combout\);

-- Location: LCCOMB_X19_Y9_N4
\clear_mux|LPM_MUX_component|auto_generated|result_node[17]~17\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_mux|LPM_MUX_component|auto_generated|result_node[17]~17_combout\ = (\clear~combout\ & ((\clear_screen|adr\(2)))) # (!\clear~combout\ & (\color~combout\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \color~combout\(2),
	datab => \clear~combout\,
	datac => \clear_screen|adr\(2),
	combout => \clear_mux|LPM_MUX_component|auto_generated|result_node[17]~17_combout\);

-- Location: M4K_X11_Y6
\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a2\ : cycloneii_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vram160x120x4:vram|altsyncram:altsyncram_component|altsyncram_r5o1:auto_generated|altsyncram_m6m1:altsyncram1|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "bidir_dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clear => "none",
	port_a_byte_enable_clock => "none",
	port_a_data_in_clear => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 2,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 4,
	port_a_write_enable_clear => "none",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_byte_enable_clear => "none",
	port_b_data_in_clear => "none",
	port_b_data_in_clock => "clock1",
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 2,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 4,
	port_b_read_enable_write_enable_clear => "none",
	port_b_read_enable_write_enable_clock => "clock1",
	ram_block_type => "M4K",
	safe_write => "err_on_2clk")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbrewe => \vram|altsyncram_component|auto_generated|altsyncram1|decode4|w_anode297w\(3),
	clk0 => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	clk1 => \clear_mux|LPM_MUX_component|auto_generated|result_node[19]~clkctrl_outclk\,
	ena0 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode297w\(3),
	ena1 => \vram|altsyncram_component|auto_generated|altsyncram1|decode4|w_anode297w\(3),
	portadatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a2_PORTADATAIN_bus\,
	portbdatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a2_PORTBDATAIN_bus\,
	portaaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a2_PORTAADDR_bus\,
	portbaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a2_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portadataout => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a2_PORTADATAOUT_bus\);

-- Location: LCCOMB_X15_Y9_N24
\vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs503w[0]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs503w[0]~0_combout\ = (\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(0) & ((\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a6~portadataout\) # 
-- ((\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(1))))) # (!\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(0) & (((\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a2~portadataout\ & 
-- !\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a6~portadataout\,
	datab => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a2~portadataout\,
	datac => \vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(0),
	datad => \vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(1),
	combout => \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs503w[0]~0_combout\);

-- Location: M4K_X23_Y13
\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a14\ : cycloneii_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vram160x120x4:vram|altsyncram:altsyncram_component|altsyncram_r5o1:auto_generated|altsyncram_m6m1:altsyncram1|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "bidir_dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clear => "none",
	port_a_byte_enable_clock => "none",
	port_a_data_in_clear => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 2,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 4,
	port_a_write_enable_clear => "none",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_byte_enable_clear => "none",
	port_b_data_in_clear => "none",
	port_b_data_in_clock => "clock1",
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 2,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 4,
	port_b_read_enable_write_enable_clear => "none",
	port_b_read_enable_write_enable_clock => "clock1",
	ram_block_type => "M4K",
	safe_write => "err_on_2clk")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbrewe => \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~2_combout\,
	clk0 => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	clk1 => \clear_mux|LPM_MUX_component|auto_generated|result_node[19]~clkctrl_outclk\,
	ena0 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode334w\(3),
	ena1 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~2_combout\,
	portadatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a14_PORTADATAIN_bus\,
	portbdatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a14_PORTBDATAIN_bus\,
	portaaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a14_PORTAADDR_bus\,
	portbaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a14_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portadataout => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a14_PORTADATAOUT_bus\);

-- Location: LCCOMB_X15_Y9_N14
\vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs503w[0]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs503w[0]~1_combout\ = (\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(1) & 
-- ((\vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs503w[0]~0_combout\ & ((\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a14~portadataout\))) # 
-- (!\vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs503w[0]~0_combout\ & (\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a10~portadataout\)))) # 
-- (!\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(1) & (((\vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs503w[0]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100000111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a10~portadataout\,
	datab => \vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(1),
	datac => \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs503w[0]~0_combout\,
	datad => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a14~portadataout\,
	combout => \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs503w[0]~1_combout\);

-- Location: M4K_X23_Y7
\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a18\ : cycloneii_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vram160x120x4:vram|altsyncram:altsyncram_component|altsyncram_r5o1:auto_generated|altsyncram_m6m1:altsyncram1|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "bidir_dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clear => "none",
	port_a_byte_enable_clock => "none",
	port_a_data_in_clear => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 2,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 4,
	port_a_write_enable_clear => "none",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_byte_enable_clear => "none",
	port_b_data_in_clear => "none",
	port_b_data_in_clock => "clock1",
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 2,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 4,
	port_b_read_enable_write_enable_clear => "none",
	port_b_read_enable_write_enable_clock => "clock1",
	ram_block_type => "M4K",
	safe_write => "err_on_2clk")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbrewe => \vram|altsyncram_component|auto_generated|altsyncram1|decode4|w_anode344w[3]~0_combout\,
	clk0 => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	clk1 => \clear_mux|LPM_MUX_component|auto_generated|result_node[19]~clkctrl_outclk\,
	ena0 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode344w[3]~0_combout\,
	ena1 => \vram|altsyncram_component|auto_generated|altsyncram1|decode4|w_anode344w[3]~0_combout\,
	portadatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a18_PORTADATAIN_bus\,
	portbdatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a18_PORTBDATAIN_bus\,
	portaaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a18_PORTAADDR_bus\,
	portbaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a18_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portadataout => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a18_PORTADATAOUT_bus\);

-- Location: LCCOMB_X15_Y9_N12
\vram|altsyncram_component|auto_generated|altsyncram1|mux5|muxlut_result2w~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \vram|altsyncram_component|auto_generated|altsyncram1|mux5|muxlut_result2w~0_combout\ = (\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(2) & ((\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a18~portadataout\))) # 
-- (!\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(2) & (\vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs503w[0]~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(2),
	datac => \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs503w[0]~1_combout\,
	datad => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a18~portadataout\,
	combout => \vram|altsyncram_component|auto_generated|altsyncram1|mux5|muxlut_result2w~0_combout\);

-- Location: PIN_86,	 I/O Standard: 3.3-V PCI,	 Current Strength: Default
\color[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_color(3),
	combout => \color~combout\(3));

-- Location: LCCOMB_X19_Y9_N2
\clear_mux|LPM_MUX_component|auto_generated|result_node[18]~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \clear_mux|LPM_MUX_component|auto_generated|result_node[18]~18_combout\ = (\clear~combout\ & (\clear_screen|adr\(3))) # (!\clear~combout\ & ((\color~combout\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \clear~combout\,
	datac => \clear_screen|adr\(3),
	datad => \color~combout\(3),
	combout => \clear_mux|LPM_MUX_component|auto_generated|result_node[18]~18_combout\);

-- Location: M4K_X23_Y9
\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a3\ : cycloneii_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vram160x120x4:vram|altsyncram:altsyncram_component|altsyncram_r5o1:auto_generated|altsyncram_m6m1:altsyncram1|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "bidir_dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clear => "none",
	port_a_byte_enable_clock => "none",
	port_a_data_in_clear => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 3,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 4,
	port_a_write_enable_clear => "none",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_byte_enable_clear => "none",
	port_b_data_in_clear => "none",
	port_b_data_in_clock => "clock1",
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 3,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 4,
	port_b_read_enable_write_enable_clear => "none",
	port_b_read_enable_write_enable_clock => "clock1",
	ram_block_type => "M4K",
	safe_write => "err_on_2clk")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbrewe => \vram|altsyncram_component|auto_generated|altsyncram1|decode4|w_anode297w\(3),
	clk0 => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	clk1 => \clear_mux|LPM_MUX_component|auto_generated|result_node[19]~clkctrl_outclk\,
	ena0 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode297w\(3),
	ena1 => \vram|altsyncram_component|auto_generated|altsyncram1|decode4|w_anode297w\(3),
	portadatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a3_PORTADATAIN_bus\,
	portbdatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a3_PORTBDATAIN_bus\,
	portaaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a3_PORTAADDR_bus\,
	portbaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a3_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portadataout => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a3_PORTADATAOUT_bus\);

-- Location: LCCOMB_X15_Y9_N22
\vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs551w[0]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs551w[0]~0_combout\ = (\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(1) & ((\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a11~portadataout\) # 
-- ((\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(0))))) # (!\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(1) & (((!\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(0) & 
-- \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a3~portadataout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a11~portadataout\,
	datab => \vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(1),
	datac => \vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(0),
	datad => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a3~portadataout\,
	combout => \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs551w[0]~0_combout\);

-- Location: M4K_X23_Y12
\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a15\ : cycloneii_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vram160x120x4:vram|altsyncram:altsyncram_component|altsyncram_r5o1:auto_generated|altsyncram_m6m1:altsyncram1|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "bidir_dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clear => "none",
	port_a_byte_enable_clock => "none",
	port_a_data_in_clear => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 3,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 4,
	port_a_write_enable_clear => "none",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_byte_enable_clear => "none",
	port_b_data_in_clear => "none",
	port_b_data_in_clock => "clock1",
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 3,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 4,
	port_b_read_enable_write_enable_clear => "none",
	port_b_read_enable_write_enable_clock => "clock1",
	ram_block_type => "M4K",
	safe_write => "err_on_2clk")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbrewe => \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~2_combout\,
	clk0 => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	clk1 => \clear_mux|LPM_MUX_component|auto_generated|result_node[19]~clkctrl_outclk\,
	ena0 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode334w\(3),
	ena1 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_b|w_anode314w[2]~2_combout\,
	portadatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a15_PORTADATAIN_bus\,
	portbdatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a15_PORTBDATAIN_bus\,
	portaaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a15_PORTAADDR_bus\,
	portbaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a15_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portadataout => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a15_PORTADATAOUT_bus\);

-- Location: LCCOMB_X15_Y9_N20
\vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs551w[0]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs551w[0]~1_combout\ = (\vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs551w[0]~0_combout\ & 
-- (((\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a15~portadataout\) # (!\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(0))))) # 
-- (!\vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs551w[0]~0_combout\ & (\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a7~portadataout\ & (\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110000101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a7~portadataout\,
	datab => \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs551w[0]~0_combout\,
	datac => \vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(0),
	datad => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a15~portadataout\,
	combout => \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs551w[0]~1_combout\);

-- Location: M4K_X23_Y5
\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a19\ : cycloneii_ram_block
-- pragma translate_off
GENERIC MAP (
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "vram160x120x4:vram|altsyncram:altsyncram_component|altsyncram_r5o1:auto_generated|altsyncram_m6m1:altsyncram1|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "bidir_dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 12,
	port_a_byte_enable_clear => "none",
	port_a_byte_enable_clock => "none",
	port_a_data_in_clear => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 1,
	port_a_first_address => 0,
	port_a_first_bit_number => 3,
	port_a_last_address => 4095,
	port_a_logical_ram_depth => 19200,
	port_a_logical_ram_width => 4,
	port_a_write_enable_clear => "none",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 12,
	port_b_byte_enable_clear => "none",
	port_b_data_in_clear => "none",
	port_b_data_in_clock => "clock1",
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 1,
	port_b_first_address => 0,
	port_b_first_bit_number => 3,
	port_b_last_address => 4095,
	port_b_logical_ram_depth => 19200,
	port_b_logical_ram_width => 4,
	port_b_read_enable_write_enable_clear => "none",
	port_b_read_enable_write_enable_clock => "clock1",
	ram_block_type => "M4K",
	safe_write => "err_on_2clk")
-- pragma translate_on
PORT MAP (
	portawe => GND,
	portbrewe => \vram|altsyncram_component|auto_generated|altsyncram1|decode4|w_anode344w[3]~0_combout\,
	clk0 => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	clk1 => \clear_mux|LPM_MUX_component|auto_generated|result_node[19]~clkctrl_outclk\,
	ena0 => \vram|altsyncram_component|auto_generated|altsyncram1|decode_a|w_anode344w[3]~0_combout\,
	ena1 => \vram|altsyncram_component|auto_generated|altsyncram1|decode4|w_anode344w[3]~0_combout\,
	portadatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a19_PORTADATAIN_bus\,
	portbdatain => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a19_PORTBDATAIN_bus\,
	portaaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a19_PORTAADDR_bus\,
	portbaddr => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a19_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portadataout => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a19_PORTADATAOUT_bus\);

-- Location: LCCOMB_X15_Y9_N26
\vram|altsyncram_component|auto_generated|altsyncram1|mux5|muxlut_result3w~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \vram|altsyncram_component|auto_generated|altsyncram1|mux5|muxlut_result3w~0_combout\ = (\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(2) & ((\vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a19~portadataout\))) # 
-- (!\vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(2) & (\vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs551w[0]~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \vram|altsyncram_component|auto_generated|altsyncram1|address_reg_a\(2),
	datac => \vram|altsyncram_component|auto_generated|altsyncram1|mux5|w_mux_outputs551w[0]~1_combout\,
	datad => \vram|altsyncram_component|auto_generated|altsyncram1|ram_block2a19~portadataout\,
	combout => \vram|altsyncram_component|auto_generated|altsyncram1|mux5|muxlut_result3w~0_combout\);

-- Location: LCFF_X13_Y10_N21
\pixel_counter|y_reg[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \pixel_counter|y_reg[5]~20_combout\,
	sclr => \pixel_counter|LessThan1~2_combout\,
	ena => \pixel_counter|Equal0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pixel_counter|y_reg\(5));

-- Location: LCCOMB_X14_Y10_N24
\pixel_counter|Equal1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|Equal1~0_combout\ = (\pixel_counter|y_reg\(6) & (\pixel_counter|y_reg\(7) & (\pixel_counter|y_reg\(5) & \pixel_counter|y_reg\(8))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|y_reg\(6),
	datab => \pixel_counter|y_reg\(7),
	datac => \pixel_counter|y_reg\(5),
	datad => \pixel_counter|y_reg\(8),
	combout => \pixel_counter|Equal1~0_combout\);

-- Location: LCCOMB_X20_Y10_N2
\pixel_counter|process_2~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|process_2~4_combout\ = (\pixel_counter|Equal0~1_combout\ & ((\pixel_counter|process_2~3_combout\) # ((\pixel_counter|x_reg\(4)) # (!\pixel_counter|Equal0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|process_2~3_combout\,
	datab => \pixel_counter|x_reg\(4),
	datac => \pixel_counter|Equal0~0_combout\,
	datad => \pixel_counter|Equal0~1_combout\,
	combout => \pixel_counter|process_2~4_combout\);

-- Location: LCCOMB_X14_Y10_N20
\pixel_counter|process_2~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|process_2~5_combout\ = (\pixel_counter|process_2~2_combout\) # ((\pixel_counter|y_reg\(9)) # ((\pixel_counter|Equal1~0_combout\) # (\pixel_counter|process_2~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|process_2~2_combout\,
	datab => \pixel_counter|y_reg\(9),
	datac => \pixel_counter|Equal1~0_combout\,
	datad => \pixel_counter|process_2~4_combout\,
	combout => \pixel_counter|process_2~5_combout\);

-- Location: LCFF_X14_Y10_N21
\pixel_counter|blanking\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \pixel_counter|process_2~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pixel_counter|blanking~regout\);

-- Location: LCCOMB_X13_Y11_N24
\blanking_pipe[1]~feeder\ : cycloneii_lcell_comb
-- Equation(s):
-- \blanking_pipe[1]~feeder_combout\ = \pixel_counter|blanking~regout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \pixel_counter|blanking~regout\,
	combout => \blanking_pipe[1]~feeder_combout\);

-- Location: LCFF_X13_Y11_N25
\blanking_pipe[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \blanking_pipe[1]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => blanking_pipe(1));

-- Location: LCCOMB_X12_Y11_N24
\color_rom|r~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \color_rom|r~0_combout\ = (\color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(0) & !blanking_pipe(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(0),
	datad => blanking_pipe(1),
	combout => \color_rom|r~0_combout\);

-- Location: LCFF_X12_Y11_N25
\color_rom|r[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \color_rom|r~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \color_rom|r\(0));

-- Location: LCCOMB_X12_Y11_N2
\color_rom|r~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \color_rom|r~1_combout\ = (\color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(1) & !blanking_pipe(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(1),
	datad => blanking_pipe(1),
	combout => \color_rom|r~1_combout\);

-- Location: LCFF_X12_Y11_N3
\color_rom|r[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \color_rom|r~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \color_rom|r\(1));

-- Location: LCCOMB_X12_Y11_N20
\color_rom|r~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \color_rom|r~2_combout\ = (\color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(2) & !blanking_pipe(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(2),
	datad => blanking_pipe(1),
	combout => \color_rom|r~2_combout\);

-- Location: LCFF_X12_Y11_N21
\color_rom|r[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \color_rom|r~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \color_rom|r\(2));

-- Location: LCCOMB_X12_Y11_N22
\color_rom|r~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \color_rom|r~3_combout\ = (\color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(3) & !blanking_pipe(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(3),
	datad => blanking_pipe(1),
	combout => \color_rom|r~3_combout\);

-- Location: LCFF_X12_Y11_N23
\color_rom|r[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \color_rom|r~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \color_rom|r\(3));

-- Location: LCCOMB_X12_Y11_N0
\color_rom|g~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \color_rom|g~0_combout\ = (\color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(4) & !blanking_pipe(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(4),
	datad => blanking_pipe(1),
	combout => \color_rom|g~0_combout\);

-- Location: LCFF_X12_Y11_N1
\color_rom|g[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \color_rom|g~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \color_rom|g\(0));

-- Location: LCCOMB_X12_Y11_N18
\color_rom|g~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \color_rom|g~1_combout\ = (\color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(5) & !blanking_pipe(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(5),
	datad => blanking_pipe(1),
	combout => \color_rom|g~1_combout\);

-- Location: LCFF_X12_Y11_N19
\color_rom|g[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \color_rom|g~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \color_rom|g\(1));

-- Location: LCCOMB_X12_Y11_N16
\color_rom|g~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \color_rom|g~2_combout\ = (\color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(6) & !blanking_pipe(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(6),
	datad => blanking_pipe(1),
	combout => \color_rom|g~2_combout\);

-- Location: LCFF_X12_Y11_N17
\color_rom|g[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \color_rom|g~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \color_rom|g\(2));

-- Location: LCCOMB_X12_Y11_N26
\color_rom|g~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \color_rom|g~3_combout\ = (\color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(7) & !blanking_pipe(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(7),
	datad => blanking_pipe(1),
	combout => \color_rom|g~3_combout\);

-- Location: LCFF_X12_Y11_N27
\color_rom|g[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \color_rom|g~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \color_rom|g\(3));

-- Location: LCCOMB_X12_Y11_N28
\color_rom|b~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \color_rom|b~0_combout\ = (\color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(8) & !blanking_pipe(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(8),
	datad => blanking_pipe(1),
	combout => \color_rom|b~0_combout\);

-- Location: LCFF_X12_Y11_N29
\color_rom|b[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \color_rom|b~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \color_rom|b\(0));

-- Location: LCCOMB_X12_Y11_N6
\color_rom|b~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \color_rom|b~1_combout\ = (\color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(9) & !blanking_pipe(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(9),
	datad => blanking_pipe(1),
	combout => \color_rom|b~1_combout\);

-- Location: LCFF_X12_Y11_N7
\color_rom|b[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \color_rom|b~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \color_rom|b\(1));

-- Location: LCCOMB_X12_Y11_N8
\color_rom|b~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \color_rom|b~2_combout\ = (\color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(10) & !blanking_pipe(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(10),
	datad => blanking_pipe(1),
	combout => \color_rom|b~2_combout\);

-- Location: LCFF_X12_Y11_N9
\color_rom|b[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \color_rom|b~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \color_rom|b\(2));

-- Location: LCCOMB_X12_Y11_N10
\color_rom|b~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \color_rom|b~3_combout\ = (\color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(11) & !blanking_pipe(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \color_rom|palette_rom|altsyncram_component|auto_generated|q_a\(11),
	datad => blanking_pipe(1),
	combout => \color_rom|b~3_combout\);

-- Location: LCFF_X12_Y11_N11
\color_rom|b[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \color_rom|b~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \color_rom|b\(3));

-- Location: LCCOMB_X18_Y10_N22
\pixel_counter|LessThan5~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|LessThan5~0_combout\ = (\pixel_counter|Add0~16_combout\) # ((\pixel_counter|Add0~14_combout\) # (\pixel_counter|Add0~18_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \pixel_counter|Add0~16_combout\,
	datac => \pixel_counter|Add0~14_combout\,
	datad => \pixel_counter|Add0~18_combout\,
	combout => \pixel_counter|LessThan5~0_combout\);

-- Location: LCCOMB_X18_Y10_N30
\pixel_counter|LessThan5~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|LessThan5~3_combout\ = ((!\pixel_counter|LessThan5~0_combout\ & ((!\pixel_counter|Add0~12_combout\) # (!\pixel_counter|LessThan5~2_combout\)))) # (!\pixel_counter|LessThan0~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001001111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|LessThan5~2_combout\,
	datab => \pixel_counter|LessThan5~0_combout\,
	datac => \pixel_counter|Add0~12_combout\,
	datad => \pixel_counter|LessThan0~1_combout\,
	combout => \pixel_counter|LessThan5~3_combout\);

-- Location: LCFF_X18_Y10_N31
\pixel_counter|h_sync\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \pixel_counter|LessThan5~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pixel_counter|h_sync~regout\);

-- Location: LCFF_X19_Y10_N25
\h_sync_pipe[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	sdata => \pixel_counter|h_sync~regout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => h_sync_pipe(1));

-- Location: LCFF_X19_Y10_N19
\h_sync_pipe[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	sdata => h_sync_pipe(1),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => h_sync_pipe(2));

-- Location: LCCOMB_X13_Y10_N0
\pixel_counter|Equal1~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|Equal1~1_combout\ = (\pixel_counter|y_reg\(1) & (!\pixel_counter|y_reg\(2) & (\pixel_counter|y_reg\(3) & !\pixel_counter|y_reg\(9))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|y_reg\(1),
	datab => \pixel_counter|y_reg\(2),
	datac => \pixel_counter|y_reg\(3),
	datad => \pixel_counter|y_reg\(9),
	combout => \pixel_counter|Equal1~1_combout\);

-- Location: LCCOMB_X14_Y10_N26
\pixel_counter|Equal1~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \pixel_counter|Equal1~2_combout\ = (\pixel_counter|Equal1~0_combout\ & (!\pixel_counter|y_reg\(4) & \pixel_counter|Equal1~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pixel_counter|Equal1~0_combout\,
	datab => \pixel_counter|y_reg\(4),
	datac => \pixel_counter|Equal1~1_combout\,
	combout => \pixel_counter|Equal1~2_combout\);

-- Location: LCFF_X14_Y10_N27
\pixel_counter|v_sync\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \pixel_counter|Equal1~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pixel_counter|v_sync~regout\);

-- Location: LCCOMB_X20_Y10_N6
\v_sync_pipe[1]~feeder\ : cycloneii_lcell_comb
-- Equation(s):
-- \v_sync_pipe[1]~feeder_combout\ = \pixel_counter|v_sync~regout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \pixel_counter|v_sync~regout\,
	combout => \v_sync_pipe[1]~feeder_combout\);

-- Location: LCFF_X20_Y10_N7
\v_sync_pipe[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \v_sync_pipe[1]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => v_sync_pipe(1));

-- Location: LCCOMB_X20_Y10_N8
\v_sync_pipe[2]~feeder\ : cycloneii_lcell_comb
-- Equation(s):
-- \v_sync_pipe[2]~feeder_combout\ = v_sync_pipe(1)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => v_sync_pipe(1),
	combout => \v_sync_pipe[2]~feeder_combout\);

-- Location: LCFF_X20_Y10_N9
\v_sync_pipe[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \main_pll|altpll_component|_clk0~clkctrl_outclk\,
	datain => \v_sync_pipe[2]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => v_sync_pipe(2));

-- Location: PIN_114,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\r[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \color_rom|r\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_r(0));

-- Location: PIN_115,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\r[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \color_rom|r\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_r(1));

-- Location: PIN_118,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\r[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \color_rom|r\(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_r(2));

-- Location: PIN_119,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\r[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \color_rom|r\(3),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_r(3));

-- Location: PIN_120,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\g[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \color_rom|g\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_g(0));

-- Location: PIN_121,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\g[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \color_rom|g\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_g(1));

-- Location: PIN_122,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\g[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \color_rom|g\(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_g(2));

-- Location: PIN_125,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\g[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \color_rom|g\(3),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_g(3));

-- Location: PIN_126,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\b[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \color_rom|b\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_b(0));

-- Location: PIN_129,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\b[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \color_rom|b\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_b(1));

-- Location: PIN_132,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\b[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \color_rom|b\(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_b(2));

-- Location: PIN_133,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\b[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \color_rom|b\(3),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_b(3));

-- Location: PIN_113,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\h_sync~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => ALT_INV_h_sync_pipe(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_h_sync);

-- Location: PIN_112,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\v_sync~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => ALT_INV_v_sync_pipe(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_v_sync);
END structure;


