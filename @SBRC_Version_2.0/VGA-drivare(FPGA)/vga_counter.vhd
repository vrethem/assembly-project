library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity vga_counter is
	port( clk		: in std_logic;
			reset		: in std_logic;
			x			: out std_logic_vector(9 downto 0);
			y			: out std_logic_vector(8 downto 0);
			h_sync	: out std_logic;
			v_sync	: out std_logic;
			blanking	: out std_logic);
end vga_counter;

architecture vga_counter_beh of vga_counter is
	signal x_reg	: std_logic_vector(9 downto 0);
	signal y_reg	: std_logic_vector(9 downto 0);
	
	signal x_next	: std_logic_vector(9 downto 0);
	signal y_next	: std_logic_vector(9 downto 0);
	
	signal h_blank		: std_logic;
	signal v_blank		: std_logic;
begin
	-- Combinational logic
	x_next <= x_reg + '1' when x_reg < 797 else (others => '0');
	y_next <= y_reg + '1' when y_reg < 524 else (others => '0');
	
	h_blank <= '1' when x_next <= 142 OR x_reg > 781 else '0';
	v_blank <= '1' when y_reg >= 480 else '0';

	
	-- Actuate next x-counter value
	process(clk, reset)
	begin
		if reset = '1' then
		  x_reg <= (others => '0');
		elsif rising_edge(clk) then
			x_reg <= x_next;
		end if;
	end process;
	
	-- Actuate next y-counter value
	process(clk, reset)
	begin
		if reset = '1' then
		  y_reg <= (others => '0');
		elsif rising_edge(clk) AND x_reg = 797 then
			y_reg <= y_next;
		end if;
	end process;
	
	-- Actuate blanking signal
	process(clk, reset)
	begin
		if reset = '1' then
			blanking <= '0';
		elsif rising_edge(clk) then
			-- blanking <= h_blank OR v_blank; 
			if (h_blank = '1' OR v_blank = '1') then
				blanking <= '1';
			else
				blanking <= '0' ;
			end if;
		end if;
	end process;
	
	-- Actuate h_sync signal	
	process(clk, reset)
	begin
		if reset = '1' then
			h_sync <= '0';
		elsif rising_edge(clk) then
			-- h_sync <= x_next <= 94;
			if x_next <= 94 then
				h_sync <= '1';
			else
				h_sync <= '0';
			end if;
		end if;
	end process;
	
	
	-- Actuate v_sync signal
	process(clk, reset)
	begin
		if reset = '1' then
			v_sync <= '0';
		elsif rising_edge(clk) then
			-- v_sync <= y_next = 490 OR y_next = 491
			if (y_reg = 490 OR y_reg = 491) then
				v_sync <= '1';
			else
				v_sync <= '0' ;
			end if;
		end if;
	end process;
	
	-- Output buffered signals
	x <= x_next - 143;
	--x <= x_reg - 143;
	y <= y_reg(8 downto 0) ;
	
end vga_counter_beh;