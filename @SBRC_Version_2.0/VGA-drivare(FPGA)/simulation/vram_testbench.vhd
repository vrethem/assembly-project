library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity vram_testbench is
end vram_testbench;

architecture vram_testbench_beh of vram_testbench is
	component vram160x120x4
		port(
		data		: IN STD_LOGIC_VECTOR (3 DOWNTO 0);
		rdaddress		: IN STD_LOGIC_VECTOR (14 DOWNTO 0);
		rdclock		: IN STD_LOGIC ;
		rden		: IN STD_LOGIC  := '1';
		wraddress		: IN STD_LOGIC_VECTOR (14 DOWNTO 0);
		wrclock		: IN STD_LOGIC  := '1';
		wren		: IN STD_LOGIC  := '0';
		q		: OUT STD_LOGIC_VECTOR (3 DOWNTO 0)
	);
	end component;
	
	
	signal data			: STD_LOGIC_VECTOR (3 DOWNTO 0);
	signal rdaddress	: STD_LOGIC_VECTOR (14 DOWNTO 0);
	signal rdclock		: STD_LOGIC;
	signal rden			: STD_LOGIC;
	signal wraddress	: STD_LOGIC_VECTOR (14 DOWNTO 0);
	signal wrclock		: STD_LOGIC;
	signal wren			: STD_LOGIC;
	signal q				: STD_LOGIC_VECTOR (3 DOWNTO 0);
	
	constant clk_period : time := 1 ns;
begin

	uut: vram160x120x4 port map(
		data			=> data,
		rdaddress	=> rdaddress,
		rdclock		=> rdclock,
		rden			=> rden,
		wraddress	=> wraddress,
		wrclock		=> wrclock,
		wren			=> wren,
		q				=> q
	);
	
	test_process : process
	begin
		data			<= (others => '0');
		rdaddress	<= (others => '0');
		rdclock		<= '0';
		rden			<= '1';
		wraddress	<= (others => '0');
		wrclock		<= '0';
		wren			<= '1';
		
		wait for clk_period;
		
		for i in 0 to 5 loop
			data <= std_logic_vector(to_unsigned(i, data'length));
			wraddress <= std_logic_vector(to_unsigned(i, wraddress'length));
			wrclock <= '1';
			wait for clk_period;
			wrclock <= '0';
			wait for clk_period;
		end loop;
			
		for i in 0 to 5 loop
			rdaddress <= std_logic_vector(to_unsigned(i, rdaddress'length));
			wait for clk_period;
			rdclock <= '1';
			wait for clk_period;
			rdclock <= '0';
			wait for clk_period;
		end loop;
		
		
		
		wait for clk_period;
	end process;
	
end vram_testbench_beh;